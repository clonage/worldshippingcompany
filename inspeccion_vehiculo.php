<?php

include 'database/database.php';
session_start();

$id_servicio = base64_decode($_GET["service"]);

$id_cargo = 2; //$_SESSION["cargo"];

$consultar_conceptos_inspeccion = $conn->prepare("SELECT * FROM control_concepto_inspeccion");
$consultar_conceptos_inspeccion->execute();
$consultar_conceptos_inspeccion = $consultar_conceptos_inspeccion->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

<head>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="" />
    <!-- Page Title -->
    <title>World Shipping Company</title>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/flag-icon/flag-icon.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/ionicons/css/ionicons.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/footable/footable.core.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/chartist/chartist.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/apex-chart/apexcharts.css">
    <link type="text/css" rel="stylesheet" href="assets/css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/css/style.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.min.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-select/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/steps/jquery.steps.css">

    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

    <style>
    #responsive {
        overflow: hidden;
        white-space: initial;
    }
    </style>
    <style>
    #footers {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
    }

    #ver_inspeccion {
        padding-right: 0px !important;
    }
    </style>
</head>


<body>

    <div class="page-container">
        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <div class="preview"> <a href="http://worldshippingcompany.com.co/"><i
                                        class="icon-arrow-left"></i> Regresar </a> </div>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                            <br>
                            <h6>Formulario inspección vehículo World Shipping Company S.A.S</h6>
                        </center>

                    </h4>
                </div>
            </div>

            <div style="margin-bottom: 30px;">
                <form id="form_inspeccion_vehiculo" enctype="multipart/form-data" style="margin-bottom:80px;">
                    <div>
                        <center>Datos conductor</center>
                        <div class="row" style="margin-bottom: 30px;">
                            <div class="col-lg">
                                <label for="">Nombre conductor</label>
                                <input class="form-control form-control-sm" name="conductor" id="conductor"
                                    placeholder="Nombre conductor" type="text" readonly>
                            </div>
                            <!-- col -->
                            <div class="col-lg mg-t-10 mg-lg-t-0">
                                <label for="">Número documento</label>
                                <input class="form-control form-control-sm" name="identificacion_conductor"
                                    id="identificacion_conductor" placeholder="Identificación" type="text" readonly>
                            </div>
                            <!-- col -->
                            <div class="col-lg mg-t-10 mg-lg-t-0">
                                <label for="">Placa vehículo (campo modificable)</label>
                                <input class="form-control form-control-sm" name="placa_vehiculo" id="placa_vehiculo"
                                    placeholder="Placa vehículo" type="text">
                            </div>
                            <!-- col -->
                        </div>
                        <button type="button" style="display:none" id="btn_modal_validacion" class="btn btn-danger"
                            data-toggle="modal" data-target="#m_modal_5">Launch
                            Modal</button>

                    </div>
                    <div class="form-layout form-layout-2">
                        <div class="row no-gutters">

                            <?php
                            foreach ($consultar_conceptos_inspeccion as $value) {
                            ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-control-label active">¿<?php echo $value["concepto"] ?>?
                                        <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="hidden" name="concepto[]"
                                        value="<?php echo $value["concepto"] ?>">
                                    <select name="seleccion[]" class="form-control form-control-sm" id="">
                                        <option value="Si">Sí</option>
                                        <option value="No">No</option>
                                        <option value="Na">Na</option>
                                    </select>
                                </div>
                            </div>
                            <!-- col-4 -->
                            <div class="col-md-4 mg-t--1 mg-md-t-0">
                                <div class="form-group mg-md-l--1">
                                    <label class="form-control-label active">Observaciones: <span
                                            class="tx-danger">*</span></label>
                                    <textarea name="observacion[]" rows="2" class="form-control form-control-sm"
                                        placeholder=" Observaciones" value="Ninguna"></textarea>
                                </div>
                            </div>
                            <!-- col-4 -->
                            <div class="col-md-4 mg-t--1 mg-md-t-0">
                                <div class="form-group mg-md-l--1">
                                    <label for="">Subir evidencia (opcional)</label>
                                    <div class="input-group">
                                        <div class="custom-file"> <input type="file" name="evidencias[]"
                                                class="form-control form-control-sm" id="inputGroupFile04" multiple="">
                                            <label class="" for="inputGroupFile04"></label>
                                        </div>
                                        <br>
                                        <div class="input-group-append">
                                            <span class="input-group-btn">
                                                <button class="btn btn-custom-primary file-browser" type="button"><i
                                                        class="fa fa-upload"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            }
                            ?>


                        </div>


                    </div>
                </form>

                <!-- row -->
                <div class="form-layout-footer bd pd-20 bd-t-0" id="footers">
                    <!--<a href="#" class="btn btn-danger" onclick="borrar_forms()">Borrar todo</a>-->
                    <div id="respuesta_servidor"></div>
                    <a href="javascript:void(0)" onclick="tabla_inspeccion_vehiculo()" data-toggle="modal"
                        data-target="#ver_inspeccion" class="btn btn-success">Ver mis
                        inspecciones</a>
                    <a href="javascript:void(0)" onclick="guardar_inspeccion_vehiculo()"
                        class="btn btn-custom-primary">Guardar</a>

                </div>
                <!-- form-group -->
            </div>
        </div>

    </div>


    <div class="modal show" id="m_modal_5" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel_5">¡Importante!</h5>
                    <button type="button" style="display:none" id="btn_cerrar_modal" class="btn-close"
                        data-dismiss="modal" aria-label="Close"></button>

                </div>
                <div class="modal-body">
                    <form id="validar_conductor">
                        <div class="form-group">
                            <label for="recipient-name-2" class="form-control-label">Para poder ingresar por favor
                                ingrese su número de documento:</label>
                            <input type="text" class="form-control" name="documento" placeholder="Número documento"
                                id="recipient-name-2">
                    </form>
                </div>
                <div id="respuesta_validar_conductor"></div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" onclick="validar_conductor()" class="btn btn-primary">Ingresar</a>
                </div>
            </div>
        </div>
    </div>

    <footer class="page-footer" style="position: fixed; left: 0;  bottom: 0; width: 100%; text-align: center;">
        <p class="pd-y-10 mb-0">Copyright&copy; 2022 | All rights reserved.</p>
    </footer>
    <!--/ Page Footer End -->
    </div>
    <!--/ Page Content End -->
    </div>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="assets/plugins/popper/popper.js"></script>
    <script src="assets/plugins/feather-icon/feather.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/plugins/pace/pace.min.js"></script>
    <script src="assets/plugins/toastr/toastr.min.js"></script>
    <script src="assets/plugins/countup/counterup.min.js"></script>
    <script src="assets/plugins/waypoints/waypoints.min.js"></script>
    <script src="assets/plugins/chartjs/chartjs.js"></script>
    <script src="assets/plugins/apex-chart/apexcharts.min.js"></script>
    <script src="assets/plugins/apex-chart/irregular-data-series.js"></script>
    <script src="assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js"></script>
    <script src="assets/js/dashboard/sales-dashboard-init.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/highlight.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/custom.js"></script>

    <script src="assets/plugins/footable/footable.all.min.js"></script>
    <script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="assets/plugins/steps/jquery.steps.js"></script>
    <script src="assets/plugins/pace/pace.min.js"></script>

    <script src="assets/plugins/parsleyjs/parsley.js"></script>

    <script src="js/js-admin/main.js"></script>
    <script src="js/js-operador/main.js"></script>


    <div class="modal show" id="ver_inspeccion" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_2">Inspección completa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body collapse show" id="collapse3">
                    <div class="row">

                        </button>
                        <div class="mg-20 form-inline wd-100p">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Estado</label>
                                    <select id="foo-filter-status" class="form-control">
                                        <option value="">Mostrar todos</option>
                                        <option value="Sin revisar">Sin revisar</option>
                                        <option value="Revisado">Revisados</option>

                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group ft-right">
                                    <input id="foo-search" type="text" placeholder="Buscar inspección..."
                                        class="form-control" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tabla_inspeccion"></div>
                    <div id="estado_inspeccion"></div>
                </div>
            </div>
            <div id="respuesta_form_empleado"></div>
            <div class="modal-footer">
                <div id="respuesta_actulizar_inspeccion"></div>

                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</body>

</html>

<script>
function tabla_inspeccion_vehiculo() {
    var conductor = document.getElementById("identificacion_conductor").value;
    var url = "actions/actions_admin/inspeccion_vehiculos_conductor.php?id_conductor=" + conductor;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#tabla_inspeccion").html("Cargando...");
        },
        success: function(data) {
            $("#tabla_inspeccion").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

window.load = btn_modal_validacion();

////////// Inspeccion vehiculos //////////////

function editar_inspeccion(id) {
    var url = "../actions/actions_admin/editar_inspeccion_vehiculos.php?id_inspeccion=" + id;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#editar_inspeccion").html("Cargando...");
        },
        success: function(data) {
            $("#editar_inspeccion").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function btn_modal_validacion() {
    document.getElementById("btn_modal_validacion").click();
}

function borrar_forms() {
    document.getElementById('form_inspeccion_vehiculo').reset();
}

function guardar_inspeccion_vehiculo() {
    var url =
        "actions/actions_admin/guardar_inspeccion_vehiculo.php";

    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        $.ajax({
            cache: false,
            async: false,
            type: 'POST',
            data: new FormData($("#form_inspeccion_vehiculo")[0]),
            contentType: false,
            processData: false,
            url: url,
            beforeSend: function() {
                $("#respuesta_servidor").html(
                    '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Cargando, por favor espere...</button>'
                );
            },
            success: function(data) {
                setTimeout(function() {
                    $("#respuesta_servidor").html(data);
                }, 2000);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}

function validar_conductor() {
    var url = "../../actions/actions_admin/validar_conductor.php";

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: url,
        data: $("#validar_conductor").serialize(),
        beforeSend: function() {
            $("#respuesta_validar_conductor").html(
                '<button class="btn btn-primary" type="button" disabled> <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Cargando, por favor espere...</button>'
            );
        },
        success: function(data) {
            setTimeout(function() {
                $("#respuesta_validar_conductor").html(data);
            }, 3000);
            //document.getElementById("id_servicio_form2").click();
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function llenar_campos(documento, nombres, placa) {
    $("#identificacion_conductor").val(documento);
    $("#conductor").val(nombres);
    $("#placa_vehiculo").val(placa);
}
</script>