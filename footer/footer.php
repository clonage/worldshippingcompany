 <!--/ User Singin End -->
 <!--================================-->
 <!-- Footer Script -->
 <!--================================-->
 </div>
 </div>

 <footer class="page-footer" style=" position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
  
   text-align: center;">
     <div class="pd-t-4 pd-b-0 pd-x-20">
         <div class="tx-10 tx-uppercase">
             <p class="pd-y-10 mb-0">Copyright&copy; 2022 | All rights reserved.</p>
         </div>
     </div>
 </footer>
 <!--/ Page Footer End -->
 </div>
 <!--/ Page Content End -->
 </div>
 <script src="../assets/plugins/jquery/jquery.min.js"></script>
 <script src="../assets/plugins/jquery-ui/jquery-ui.js"></script>
 <script src="../assets/plugins/popper/popper.js"></script>
 <script src="../assets/plugins/feather-icon/feather.min.js"></script>
 <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
 <script src="../assets/plugins/pace/pace.min.js"></script>
 <script src="../assets/plugins/toastr/toastr.min.js"></script>
 <script src="../assets/plugins/countup/counterup.min.js"></script>
 <script src="../assets/plugins/waypoints/waypoints.min.js"></script>
 <script src="../assets/plugins/chartjs/chartjs.js"></script>
 <script src="../assets/plugins/apex-chart/apexcharts.min.js"></script>
 <script src="../assets/plugins/apex-chart/irregular-data-series.js"></script>
 <script src="../assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js"></script>
 <script src="../assets/js/dashboard/sales-dashboard-init.js"></script>
 <script src="../assets/js/jquery.slimscroll.min.js"></script>
 <script src="../assets/js/highlight.min.js"></script>
 <script src="../assets/js/app.js"></script>
 <script src="../assets/js/custom.js"></script>

 <script src="../assets/plugins/footable/footable.all.min.js"></script>
 <script src="../assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
 <script src="../assets/plugins/steps/jquery.steps.js"></script>
 <script src="../assets/plugins/pace/pace.min.js"></script>

 <script src="../assets/plugins/parsleyjs/parsley.js"></script>

 <script src="../js/js-admin/main.js"></script>
 <script src="../js/js-operador/main.js"></script>

 </body>

 <!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

 </html>