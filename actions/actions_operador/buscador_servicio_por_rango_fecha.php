<?php 


include '../../database/database.php';


$rango_fecha = $_GET["rango_fecha"];
$fecha_cortada = explode(" ",$rango_fecha, 3);


 

$fecha_restada = new DateTime($fecha_cortada[0]);
 $fecha_incio = $fecha_restada->format('Y-m-d');

$fecha_sumada = new DateTime($fecha_cortada[2]);
 $fecha_fin = $fecha_sumada->format('Y-m-d');



 $consultar_servicios = $conn->prepare("SELECT * FROM ordendeservicio WHERE fecha 
    BETWEEN '$fecha_incio' AND '$fecha_fin' ORDER BY oid DESC");

    $consultar_servicios->execute();
    $consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);
    foreach($consultar_servicios as $servicios){
       
    }

?>

<a href="javascript:void(0)" onclick="cargar_servicios()" class="btn btn-primary">Quitar filtro</a>
<table id="foo-filterings" class="table table-bordered table-hover toggle-circle" data-page-size="7">
    <thead>
        <tr>
            <th data-toggle="true">Id servicio</th>
            <th>Cliente</th>
            <th data-hide="phone, tablet">Instrucción</th>
            <th data-hide="phone, tablet">Fecha</th>
            <th data-hide="phone, tablet">Estado</th>
            <?php
            if($accion == 'ultimo10'){
                
            }else{
                echo '<th data-hide="phone, tablet">Acción</th>';
            }
            ?>

        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($consultar_servicios as $servicios){
            ?> <tr>
            <td><?php echo $servicios["oid"]?></td>
            <td><?php $nit_cliente = $servicios["cliente"];
            $consultar_cliente = $conn->prepare("SELECT razon_social FROM clientes WHERE nit = '$nit_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

            foreach($consultar_cliente as $cliente){
                echo $cliente["razon_social"];
            }
            ?></td>
            <td><?php echo $servicios["instruciones"]?></td>
            <td><?php echo $servicios["fecha"]?></td>
            <td>
                <?php

                if($servicios["estado"] == 0){
                    echo ' <span class="label label-table label-danger">No realizado</span>';
                }else if($servicios["estado"] == 1){
                    echo ' <span class="label label-table label-success">Realizado</span>';
                }
               ?>
            </td>

            <td>
                <button type="button" data-toggle="modal" href="#formulario1" role="button"
                    onclick="formulario1(<?php echo $servicios['oid']?>)"
                    class="btn btn-outline-primary btn-icon mg-r-5"><i data-feather="plus-circle" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" title=""
                        data-original-title="Ver/Añadir información sobre el control de vehículo en ruta"></i></button>
            </td>

        </tr>
        <?php
            }
            ?>

    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>


<script>
// Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filterings");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>

<script src="http://worldshippingcompany.com.co/assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="http://worldshippingcompany.com.co/assets/plugins/datepicker/js/datepicker.es.js"></script>

<script src="http://worldshippingcompany.com.co/js/js-operador/main.js"></script>