<?php

include '../../database/database.php';
date_default_timezone_set('America/Bogota');
$fecha_actual = date("Y-m-d");
$id_servicio = $_GET["id_servicio"];

$consultar_servicio= $conn->prepare("SELECT * FROM ordendeservicio WHERE oid = '$id_servicio' ");
$consultar_servicio->execute();
$consultar_servicio = $consultar_servicio->fetchAll(PDO::FETCH_ASSOC);
foreach($consultar_servicio as $servicios){
            $nit = $servicios["cliente"];
            $consultar_cliente= $conn->prepare("SELECT * FROM clientes WHERE nit = '$nit' ");
             $consultar_cliente->execute();
             $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
             foreach($consultar_cliente as $cliente){
                 
             }
}

$consultar_informacion_control = $conn->prepare("SELECT * FROM control_vehiculo_ruta WHERE id_servicio = '$id_servicio' ");
$consultar_informacion_control->execute();
$consultar_informacion_control = $consultar_informacion_control->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_servicio = $conn->prepare("SELECT * FROM servicios  ORDER BY servicio ASC");
$consultar_tipo_servicio->execute();
$consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_carga = $conn->prepare("SELECT * FROM carga_suelta WHERE oid= '$id_servicio' ");
$consultar_tipo_carga->execute();
$consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);


?>



<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel">ID SERVICIO: #<?php echo $id_servicio?> - Formulario (1)
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
    </div>
    <div class="modal-body">

        <form id="form-formulario1">

            <div class="row">
                <div class="col-12 col-sm-6">
                    <label for="">Fecha recepción documento</label>

                    <input id="datePicker1" type="text" name="fecha_recepcion_doc" class="form-control datepicker-here"
                        placeholder="Seleccionar una fecha">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Cliente</label>
                    <select class="form-control" name="cliente" id="cliente">
                        <option value="<?php echo $servicios["cliente"]?>"><?php echo $cliente["razon_social"]?>
                        </option>
                    </select>
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Tipo de servicio</label>
                    <select class="selectpicker form-control" id="basic2" data-container="body" data-live-search="true"
                        title="Select a number" data-hide-disabled="false">
                        <?php foreach($consultar_tipo_servicio as $tipo_servicio){?>
                        <option value="<?php echo $tipo_servicio['sid']?>"><?php echo $tipo_servicio['servicio']?>
                        </option>
                        <?php }?>
                    </select>

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Tipo de carga</label>
                    <select class="selectpicker form-control" id="basic3" data-container="body" data-live-search="true"
                        title="Select a number" data-hide-disabled="false">
                        <?php foreach($consultar_tipo_carga as $tipo_carga){?>
                        <option value="<?php echo $tipo_carga['oid']?>" selected>
                            <?php echo $tipo_carga['tipo_mercancia']?>
                        </option>
                        <?php }?>
                    </select>

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Contenedor</label>
                    <input type="text" name="contenedor" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Tamaño contenedor</label>
                    <input type="text" name="tamaño_contenedor" class="form-control">

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Linea naviera</label>
                    <input type="text" name="tamaño_contenedor" class="form-control">

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Medio recepcion documentos</label>
                    <input type="text" name="tamaño_contenedor" class="form-control">

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Origen</label>
                    <input type="text" name="tamaño_contenedor" class="form-control">

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Lugar de descargue</label>
                    <input type="text" name="tamaño_contenedor" class="form-control">

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Fecha vencimiento bodegaje puerto</label>
                    <input id="datePicker1" type="text" name="fecha_recepcion_doc" class="form-control datepicker-here"
                        placeholder="Seleccionar una fecha">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Fecha vencimiento traslado ZF</label>
                    <input id="datePicker1" type="text" name="fecha_recepcion_doc" class="form-control datepicker-here"
                        placeholder="Seleccionar una fecha">

                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Fecha y hora de retiro del producto (origen)</label>
                    <input id="datePicker1" type="text" name="fecha_recepcion_doc" class="form-control datepicker-here"
                        placeholder="Seleccionar una fecha">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Seleccionar imporetiro o expoingreso</label>
                    <select name="" class="form-control" id="">
                        <option value="">Imporetiro</option>
                        <option value="">Expoingreso</option>
                    </select>

                </div>
            </div>


        </form>


    </div>
    <div class="modal-footer">
        <button class="btn btn-dark" data-dismiss="modal" aria-label="Close">Cerrar</button>
        <button class="btn btn-primary" data-target="#exampleModalToggle2"
            onclick="formulario2(<?php echo $id_servicio?>)" data-toggle="modal">Continuar</button>
    </div>
</div>


<script>
$('#basic2').selectpicker({
    liveSearch: true,
    maxOptions: 1
});

$('#basic3').selectpicker({
    liveSearch: true,
    maxOptions: 1
});
</script>
<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>
<script src="../assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>