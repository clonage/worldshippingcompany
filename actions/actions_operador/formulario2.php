<?php 
$id_servicio = $_GET["id_servicio"];
?>

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel2">ID SERVICIO: #<?php echo $id_servicio?> - Formulario (2)
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
    </div>
    <div class="modal-body">
        <form action="">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <label for="">Número de placa del vehículo</label>
                    <input type="text" class="form-control" name="placa_vehiculo_c">
                </div>
                <div class="col-12 col-sm-12">
                    <label for="">Nombre del conductor del vehículo</label>
                    <input type="text" class="form-control" name="nombre_conductor">
                </div>
                <div class="col-12 col-sm-12">
                    <label for="">Identificación del conductor</label>
                    <input type="text" class="form-control" name="identificacion_conductor">
                </div>
                <div class="col-12 col-sm-12">
                    <label for="">Ruta</label>
                    <input type="text" class="form-control" name="ruta">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-dark" data-dismiss="modal" onclick="formulario1(<?php echo $id_servicio?>)"
            aria-label="Close">Regresar</button>
        <button class="btn btn-primary" data-target="#exampleModalToggle3"
            onclick="formulario3(<?php echo $id_servicio?>)" data-toggle="modal">Continuar</button>
    </div>
</div>
<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>