<?php 
$id_servicio = $_GET["id_servicio"];
?>

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalToggleLabel3">ID SERVICIO: #<?php echo $id_servicio?> - Formulario (3)
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
    </div>
    <div class="modal-body">
        <form action="">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <label for="">Fecha y hora de inicio descargue</label>
                    <input type="text" class="form-control datepicker-here" placeholder="Select Date &amp; Time"
                        data-timepicker="true" data-time-format="hh:ii aa">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Fecha y hora de terminación descargue</label>
                    <input type="text" class="form-control datepicker-here" placeholder="Select Date &amp; Time"
                        data-timepicker="true" data-time-format="hh:ii aa">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Cantidad bultos</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Cantidad pallets</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Aplica almacenamiento</label>
                    <select name="" class="form-control" id="">
                        <option value="">Sí</option>
                        <option value="">No</option>
                    </select>
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Número de día de almacenaje</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Días de almacenaje libre</label>
                    <input type="text" class="form-control">
                </div>
                <div class="col-12 col-sm-6">
                    <label for="">Cubicaje</label>
                    <input type="text" class="form-control">
                </div>

            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-dark" data-dismiss="modal" aria-label="Close">Regresar</button>
        <button class="btn btn-primary" data-target="#exampleModalToggle4"
            onclick="formulario4(<?php echo $id_servicio?>)" data-toggle="modal">Continuar</button>
    </div>
</div>
<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>