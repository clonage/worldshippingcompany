<div id="mostrar_formulario" style="display:none" data-scrollbar-shown="true" data-scrollable="true"
    style=" overflow: hidden; overflow-y: auto;">
    <a href="javascript:void(0)" onclick="boton_atras()">Atrás</a>
    <div class="card-body collapse show" id="collapse2">
        <div id="wizard5" class="wizard wizard-style-1 clearfix">

            <h3>Creación del servicio</h3>
            <section>
                <form id="form_1">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha recepción documento</label>

                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                                placeholder="Seleccionar una fecha" <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cliente</label>
                            <select class="form-control" style="width:100% !important;"
                                onchange="consultar_subcliente()" name="cliente" id="cliente"
                                <?php echo $disabled_transporte?>>
                                <option value="" selected>Seleccione una opción</option>
                                <?php 
                            foreach($consultar_cliente as $cliente){
                            ?>
                                <option value="<?php echo $cliente["id"]?>"><?php echo $cliente["razon_social"]?>
                                </option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Subcliente</label>
                            <div id="sub_clientes">
                                <select class="form-control" style="width:100% !important;" name="sub_cliente"
                                    id="sub_cliente" <?php echo $disabled_transporte?>>
                                    <option value="" selected>Seleccione un cliente</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Do/Ns</label>
                            <input type="tel" id="dons" name="dons" class="form-control"
                                <?php echo $disabled_transporte?>>
                        </div>

                        <div class="col-12 col-sm-6">
                            <label for="">Tipo de servicio</label>
                            <select class="form-control" style="width:100% !important;" name="tipo_servicio"
                                id="tipo_servicios" <?php echo $disabled_transporte?>>

                                <?php foreach($consultar_tipo_servicio as $tipo_servicio){?>
                                <option value="<?php echo $tipo_servicio['id']?>">
                                    <?php echo $tipo_servicio['nombre_servicio']?>
                                </option>
                                <?php }?>

                            </select>



                        </div>
                        <div class="col-12 col-sm-6" id="cantidad_pallets_despacho">
                            <label for="">Cantidad pallets despachado</label>
                            <input type="text" class="form-control" id="cantidad_pallets_despacho"
                                name="cantidad_pallets_despacho" <?php echo $disabled_carga ?>>

                        </div>
                        <!-- <div class="col-12 col-sm-6">
                            <label for="">Seleccionar imporetiro o expoingreso</label>
                            <select name="impoexpo" style="width:100% !important;" class="form-control" id="impoexpo"
                                <?php echo $disabled_transporte?>>
                                <option value="" selected>Seleccione una opción</option>
                                <option value="IMPORETIRO">IMPORETIRO</option>
                                <option value="EXPOINGRESO">EXPOINGRESO</option>
                            </select>

                        </div>-->
                        <div class="col-12 col-sm-6" id="tipo_carga">
                            <label for="">Tipo de carga</label>
                            <select class="form-control" name="tipo_carga" id="tipo_carga"
                                style="width:100% !important;" <?php echo $disabled_transporte?>>
                                <?php foreach($consultar_tipo_carga as $tipo_carga){?>
                                <option value="<?php echo $tipo_carga['id']?>" selected>
                                    <?php echo $tipo_carga['nombre_carga']?>
                                </option>
                                <?php }?>
                            </select>

                        </div>
                        <div class="col-12 col-sm-6" id="contenedor">
                            <label for="">Número de contenedor</label>
                            <input type="text" name="contenedor" id="contenedor" class="form-control"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6" id="tamaño_contenedor">
                            <label for="">Tamaño contenedor</label>
                            <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                                <?php echo $disabled_transporte?>>
                                <option value="">Seleccione una opción</option>
                                <option value="20">20</option>
                                <option value="40">40</option>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6" id="linea_naviera">
                            <label for="">Linea naviera</label>
                            <input type="text" name="linea_naviera" id="linea_naviera" class="form-control"
                                <?php echo $disabled_transporte?>>

                        </div>
                        <!-- <div class="col-12 col-sm-6">
                            <label for="">Medio recepcion documentos</label>
                            <input type="text" name="recepcion_doc" id="recepcion_doc" class="form-control"
                                <?php echo $disabled_transporte?>>

                        </div>-->
                        <div class="col-12 col-sm-6">
                            <label for="">Puerto de retiro del vacío</label>
                            <input type="text" name="origen" id="origen" class="form-control"
                                <?php echo $disabled_transporte?>>

                        </div>
                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                            <label for="">Fecha retiro del vacío</label>
                            <input type="datetime-local" class="form-control " name="fecha_hora_r_p" id="fecha_hora_r_p"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6" id="lugar_descargue">
                            <label for="">Lugar de entrega del vacío</label>
                            <input type="text" name="lugar_descargue" id="lugar_descargue" class="form-control"
                                <?php echo $disabled_transporte?>>

                        </div>

                        <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                            <label for="">Fecha de entrega del vacío</label>
                            <input type="date" class="form-control " name="fecha_devolucion_v" id="fecha_devolucion_v"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6" id="lugar_entrega_zona_f">
                            <label for="">Lugar de entrega zona franca</label>
                            <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f"
                                class="form-control" <?php echo $disabled_transporte?>>

                        </div>

                        <div class="col-12 col-sm-6" id="fecha_entrega_zona_f">
                            <label for="">Fecha de entrega en zona franca</label>
                            <input type="date" class="form-control " name="fecha_entrega_zona_f"
                                id="fecha_entrega_zona_f" <?php echo $disabled_transporte?>>
                        </div>


                        <!-- <div id="fechas_bodegaje_traslado_zf" class="col-12 col-sm-6"></div>-->
                        <!-- <div class=" col-12 col-sm-6">
                            <label for="">Fecha y hora de retiro del producto (origen)</label>
                            <input type="datetime-local" name="fecha_hora_r_p" id="fecha_hora_r_p" class="form-control "
                                placeholder="Seleccionar una fecha" <?php echo $disabled_transporte?>>
                        </div>-->

                        <!--<div class="col-12 col-sm-12">

                        <center><br><a href="javascript:void(0)" onclick="guardar_form_1()"
                                class="btn btn-primary">Guardar
                                datos</a></center>

                    </div>-->
                        <div class="col-12 col-sm-6">
                            <hr>
                            <a href="javascript:void(0)" onclick="guardar_form_1()" style="width: 60%;margin-top: -7px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Guardar datos</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>
                    </div>
                </form>
                <!-- form-group -->
            </section>

            <!--    <h3>Transporte</h3>
            <section>
                <form id="form_2">
                    <input type="hidden" name="id_servicio_form2" class="form-control" id="id_servicio_form2"
                        <?php echo $disabled_transporte?>>


                    <div class="col-12 col-sm-12">
                        <label for="">Número de placa del vehículo</label>
                        <input type="text" class="form-control" id="placa_vehiculo_c" name="placa_vehiculo_c"
                            <?php echo $disabled_transporte?>>
                    </div>
                    <div class="col-12 col-sm-12">
                        <label for="">Nombre del conductor del vehículo</label>
                        <input type="text" class="form-control" name="nombre_conductor" id="nombre_conductor"
                            <?php echo $disabled_transporte?>>
                    </div>
                    <div class="col-12 col-sm-12">
                        <label for="">Identificación del conductor</label>
                        <input type="text" class="form-control" name="identificacion_conductor"
                            id="identificacion_conductor" <?php echo $disabled_transporte?>>
                    </div>
                    <div class="col-12 col-sm-12">
                        <label for="">Ruta</label>
                        <input type="text" class="form-control" name="ruta" id="ruta" <?php echo $disabled_transporte?>>
                    </div>
                    <div class="col-12 col-sm-12">
                        <label for="">Fecha y hora de retiro del producto (origen)</label>
                        <input type="datetime-local" name="fecha_hora_r_p" id="fecha_hora_r_p" class="form-control "
                            placeholder="Seleccionar una fecha" <?php echo $disabled_transporte?>>
                    </div>
                    <div class="col-12 col-sm-12">
                        <hr>
                        <a href="javascript:void(0)" onclick="guardar_form_2()" style="width: 60%;margin-top: 17px;"
                            class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte?> ">
                            <div class="ht-40 justify-content-between">
                                <span class="pd-x-15">Guardar datos</span>
                                <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                            </div>
                        </a>

                    </div>

                </form>
              

            </section>
            <h3>Descargue mercancia</h3>
            <section>
                <form id="form_3" enctype="multipart/form-data">
                    <div class="row">
                        <input type="hidden" name="id_servicio_form3" class="form-control" id="id_servicio_form3">
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha y hora de inicio descargue</label>
                            <input type="datetime-local" class="form-control" id="fecha_hora_descargue"
                                name="fecha_hora_descargue" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha y hora de terminación descargue</label>
                            <input type="datetime-local" class="form-control" id="fecha_hora_t_descargue"
                                name="fecha_hora_t_descargue" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad bultos</label>
                            <input type="text" class="form-control" id="cantidad_bultos" name="cantidad_bultos"
                                <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cantidad pallets total</label>
                            <input type="text" class="form-control" id="cantidad_bpallets" name="cantidad_bpallets"
                                <?php echo $disabled_carga ?>>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Aplica almacenamiento</label>
                            <select name="aplica_almacenamiento" id="aplica_almacenamiento" class="form-control" id=""
                                <?php echo $disabled_carga ?>>
                                <option value="1">Sí</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Número de día de almacenaje</label>
                            <input type="text" class="form-control" name="numero_dia_almacenaje"
                                id="numero_dia_almacenaje" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Días de almacenaje libre</label>
                            <input type="text" class="form-control" placeholder="Días de almacenaje"
                                id="dia_almacenaje_libre" name="dia_almacenaje_libre" <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Cubicaje</label>
                            <input type="text" class="form-control" name="cubicaje" id="cubicaje"
                                <?php echo $disabled_carga ?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Observaciones</label>
                            <textarea name="observaciones_carga" id="observaciones_carga" class="form-control"
                                <?php echo $disabled_carga ?>></textarea>

                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Adjuntar una o varias fotos</label>
                            <div class="input-group">

                                <div class="custom-file">

                                    <input type="file" name="evidencias[]" class="form-control" id="inputGroupFile04"
                                        multiple>
                                    <label class="" for="inputGroupFile04"></label>
                                </div>

                                <div class="input-group-append">
                                    <button class="btn btn-success" onclick="guardar_form_3()" type="button">Guardar
                                        datos y
                                        evidencias <i class="fa fa-upload"></i></button>
                                </div>
                            </div>
                        </div>

                    </div>

                </form>
            </section>
            <h3>Entrega mercancia</h3>
            <section>

                <form id="form_4">
                    <input type="hidden" name="id_servicio_form4" class="form-control" id="id_servicio_form4">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <label for="">¿Despacho parcial o total?</label>
                                    <select name="despacho_parcial_total" class="form-control"
                                        id="despacho_parcial_total" <?php echo $disabled_carga ?>>
                                        <option value="0" selected>Despacho parcial</option>
                                        <option value="1">Despacho total</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Fecha y hora de despacho</label>
                                    <input type="datetime-local" class="form-control " name="fecha_hora_despacho"
                                        id="fecha_hora_despacho" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Transportadora</label>
                                    <input type="text" class="form-control" id="transportadora" name="transportadora"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Nombre conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="nombre_conductor_t"
                                        id="nombre_conductor_t" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Identificación del conductor (transportadora)</label>
                                    <input type="text" class="form-control" name="doc_conductor_t" id="doc_conductor_t"
                                        <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Placa del vehículo (transportadora)</label>
                                    <input type="text" class="form-control" name="placa_v_conductor_t"
                                        id="placa_v_conductor_t" <?php echo $disabled_carga ?>>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <label for="">Cantidad pallets despachado</label>
                                    <input type="text" class="form-control" id="cantidad_pallets_despacho"
                                        name="cantidad_pallets_despacho" <?php echo $disabled_carga ?>>

                                </div>
                                <div class="col-12 col-sm-6">
                                    <hr>
                                    <a href="javascript:void(0)" onclick="guardar_form_4()"
                                        style="width: 60%;margin-top: -7px;"
                                        class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte?> ">
                                        <div class="ht-40 justify-content-between">
                                            <span class="pd-x-15">Guardar datos</span>
                                            <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                        </div>
                                    </a>

                                </div>


                            </div>
                        </div>
                        <div class="col-12 col-sm-6">

                            <div id="despacho_parcial_totals"></div>
                        </div>
                    </div>

                </form>
            </section>

            <h3>Devolución vacio</h3>
            <section>
                <form id="form_5">
                    <input type="hidden" name="id_servicio_form5" class="form-control" id="id_servicio_form5"
                        <?php echo $disabled_transporte?>>
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <label for="">Lugar devolucion del vacío</label>
                            <input type="text" class="form-control" name="lugar_devolucion_v" id="lugar_devolucion_v"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Fecha de cita devolución del vacio</label>
                            <input type="date" class="form-control " name="fecha_cita_devolucion_v"
                                id="fecha_cita_devolucion_v" <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Nombre conductor (devolución del vacío)</label>
                            <input type="text" class="form-control" id="nombre_condcutor_v" name="nombre_condcutor_v"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Identificación del conductor (devolución del vacío)</label>
                            <input type="text" class="form-control" id="doc_condcutor_v" name="doc_condcutor_v"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <label for="">Placa del vehículo (devolución del vacío)</label>
                            <input type="text" class="form-control" id="palca_condcutor_v" name="placa_condcutor_v"
                                <?php echo $disabled_transporte?>>
                        </div>
                        <div class="col-12 col-sm-6">
                            <hr>
                            <a href="javascript:void(0)" onclick="guardar_form_5()" style="width: 60%;margin-top: -7px;"
                                class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte?> ">
                                <div class="ht-40 justify-content-between">
                                    <span class="pd-x-15">Guardar datos</span>
                                    <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                </div>
                            </a>

                        </div>

                    </div>
                </form>
            </section>

            <h3>Cierre del servicio</h3>
            <section>
                <div id="resumen_servicio"></div>
            </section>-->
        </div>
    </div>
</div>