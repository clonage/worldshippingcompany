<?php 

include '../../database/database.php';

$id_tipo_carga = $_GET["id_tipo_carga"];

$consultar_tipos_cargas = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga' ");
$consultar_tipos_cargas->execute();
$consultar_tipos_cargas = $consultar_tipos_cargas->fetchAll(PDO::FETCH_ASSOC);
foreach($consultar_tipos_cargas as $tipo_carga){
    
}
?>

<form id="editar_form_tipo_carga">

    <input type="hidden" name="id_tipo_carga" value="<?php echo $id_tipo_carga?>">
    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Nombre carga</label>
        <input type="text" class="form-control" name="nombre_carga" value="<?php echo $tipo_carga["nombre_carga"]?>"
            id="recipient-name-2">
    </div>

    <div class="form-group">
        <label for="recipient-name-2" class="form-control-label">Estado carga</label>
        <select name="estado" class="form-control" id="estado">

            <option value="<?php echo $tipo_carga["estado"]?>">
                <?php 
            if($tipo_carga["estado"] == 0){
                echo "Desactivado";

            }else{
                echo "Activado";
            }
            ?>

            </option>
            <option value="0">Desactivar</option>
            <option value="1">Activar</option>

        </select>


    </div>
</form>

<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary" onclick="actualizar_tipos_cargas()">Actualizar</button>
</div>