<?php
include '../../database/database.php';
session_start();

$id_servicio = $_GET["id_servicio"];
$opcion = $_GET["opcion"];

$consultar_despachos = $conn->prepare("SELECT * FROM despachos WHERE id_servicio = '$id_servicio'");
$consultar_despachos->execute();
$consultar_despachos = $consultar_despachos->fetchAll(PDO::FETCH_ASSOC);
?>

<style>
/* The Modal (background) */
.modal_evidencia {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 902;
    /* Sit on top */
    padding-top: 100px;
    /* Location of the box */
    left: 0;
    top: 0;
    width: 100%;
    /* Full width */
    height: 100vh;
    /* Full height */
    /*  overflow: auto;*/
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Modal Content */
.modal_evidencia-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    height: 100vh;
    border-radius: 20px 20px 0px 0px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {
        top: 300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}

@keyframes animatetop {
    from {
        top: 300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}


/* The Close Button */
.close_evidencia {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close_evidencia:hover,
.close_evidencia:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

#cerrar {
    margin-top: -20px;
    margin-bottom: 15px;
    border-bottom: solid 1px #0000001f;
    pointer-events: auto;
    /* border-radius: 20px; */
    cursor: pointer;
}

#cerrar:hover {
    background: #00000036;
    border-radius: 0px 0px 20px 20px;

    color: white;
}
</style>


<div id="accordion">
    <?php
    foreach ($consultar_despachos as $despachos) {
        $suma_de_pallets_despachados += $despachos["cantidad_pallets_despacho"];
    ?>

    <div class="card mb-2">
        <div class="card-header">
            <a class="text-dark collapsed" data-toggle="collapse" href="#accordion<?php echo $despachos["id"] ?>"
                aria-expanded="false" data-original-title="" title="" data-init="true">
                <?php echo $despachos["nombre_transportadora"] . ' / ' . $despachos["fecha_hora_despacho"] . ' / # despachados: ' . $despachos["cantidad_pallets_despacho"] ?>
            </a>
        </div>
        <div id="accordion<?php echo $despachos["id"] ?>" class="collapse" data-parent="#accordion" style="">
            <div class="card-body">
                <div class="col-12">
                    <h6 class="tx-dark tx-13 tx-semibold">Detalles</h6>
                    <ul class="list-unstyled">
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Fecha y hora de despacho:</b>
                                <?php echo $despachos["fecha_hora_despacho"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Transportadora: </b>
                                <?php echo $despachos["nombre_transportadora"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Nombre condcutor:
                                </b><?php echo $despachos["nombres_conductor"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Identificación conductor:
                                </b><?php echo $despachos["numero_identificacion"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Placa vehículo:
                                </b><?php echo $despachos["placa_vehiculo"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Cantidad despachados:
                                </b><?php echo $despachos["cantidad_pallets_despacho"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Peso aproximado:
                                </b><?php echo $despachos["peso_aprox"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Destino:
                                </b><?php echo $despachos["destino"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="menu-item pl-0 tx-13 tx-normal" href="#">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Observación:
                                </b><?php echo $despachos["observacion"] ?>
                            </a>
                        </li>
                        <li>
                            <a class="btn btn-primary btn-sm" href="#" data-toggle="modal"
                                data-target="#ver_evidencias_despachos"
                                onclick="ver_evidencias(<?php echo $despachos['id'] ?>)">
                                <i class="icon-arrow-right-circle pl-1 pr-2"></i><b>Ver evidencias
                                </b></a> &nbsp; <a href="../../actions/actions_admin/descargar_pdf_despachos.php"
                                class="btn btn-success btn-sm">Pdf
                                despacho</a> <br><br>
                            <?php if ($opcion != 1) { ?>
                            <a href="javascript:void(0)" class="btn btn-danger btn-sm"
                                id="btn_eliminar_despacho_<?= $despachos['id'] ?>"
                                onclick="eliminar_despacho(<?php echo $despachos['id'] ?>,<?php echo $id_servicio ?>)">Eliminar
                                despacho</a>

                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<div id="eliminando_despacho"></div>
<input type="hidden" id="suma_pallets_despachados" value="<?php echo  $suma_de_pallets_despachados; ?>">

<!-- The editar servicios -->
<div id="ver_evidencia_despachos" class="modal_evidencia">
    <div onclick="cerrar_modal_evidencia()" style="padding: 31px; pointer-events: auto; cursor: pointer;
    margin-top: -109px;"></div>
    <!-- Modal content -->
    <div class="modal_evidencia-content">
        <div onclick="cerrar_modal_evidencia()" id="cerrar">
            <center><i class="fa fa-window-minimize"></i><br>
                <h6 style="font-size:8px">Cerrar</h6>
            </center>
            <!-- <span class="close_evidencia" onclick="cerrar_modal_evidencia()">&times;</span>-->
        </div>


        <div id="evidencia_despacio_fotos"> </div>
    </div>
</div>

<script>
function cerrar_modal_evidencia() {
    var modal_evidencia = document.getElementById("ver_evidencia_despachos");
    modal_evidencia.style.display = "none";
}


function ver_evidencias(id_evidencia_despacho) {

    var modal_evidencia = document.getElementById("ver_evidencia_despachos");
    modal_evidencia.style.display = "block";

    var url = "../../actions/actions_admin/ver_evidencias_despachos.php?id_despachos=" + id_evidencia_despacho;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#evidencia_despacio_fotos").html("Cargando...");
        },
        success: function(data) {
            $("#evidencia_despacio_fotos").html(data);

        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function eliminar_despacho(id_despacho, id_servicio) {
    var opcion = confirm("¿Estás seguro de realizar esta acción?");

    if (opcion == true) {
        var url =
            "../actions/actions_admin/eliminar_despachos.php?id_despacho=" +
            id_despacho + '&id_servicio=' + id_servicio;

        $.ajax({
            cache: false,
            async: false,
            url: url,
            beforeSend: function() {
                $("#eliminando_despacho").html("Cargando...");
            },
            success: function(data) {
                $("#eliminando_despacho").html(data);
            },
            error: function() {
                alert("Error, por favor intentalo más tarde.");
            },
        });
    } else {
        fadeOut();
    }
}
</script>