<?php

session_start();

include '../../database/database.php';
require "../../vendor/autoload.php";
date_default_timezone_set('America/Bogota');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

$rango_fecha = $_GET["rango_fecha"];
$id_servicio = $_GET["id_servicio"];

$fecha_cortada = explode(" ", $rango_fecha, 3);
$fecha_restada = new DateTime($fecha_cortada[0]);
$fecha_incio = $fecha_restada->format('Y-m-d');

$fecha_sumada = new DateTime($fecha_cortada[2]);
$fecha_fin = $fecha_sumada->format('Y-m-d');


if ($id_servicio == null) {
    $error = "error";
    $mensaje = "Por favor seleccione un servicio.";
    echo "<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
    }, 1000);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";
} else if ($rango_fecha == null) {
    $error = "error";
    $mensaje = "Por favor seleccione un rango de fechas";
    echo "<script> 

ejecutar_boton();
function ejecutar_boton() {
   if('$error' == 'success'){
    toastr.success('$mensaje','Hola!')
    setTimeout(function() {
    }, 1000);
    
   }else{
    toastr.error('$mensaje','Hola!')
   }
}

</script>";
} else {

    /* if ($id_servicio != 0) {
        $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio' AND fecha_recepcion_doc 
    BETWEEN '$fecha_incio' AND '$fecha_fin' ");
    } else {
        $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE id = '$id_servicio'");
    }*/
    $consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas WHERE tipo_servicio = '$id_servicio' AND fecha_recepcion_doc 
    BETWEEN '$fecha_incio' AND '$fecha_fin' ");
    $consultar_servicios->execute();
    $consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

    foreach ($consultar_servicios as $servicios) {
        $id_servicio_registrado = $servicios["id"];
        $id_cliente = $servicios["id_cliente"];
        $id_sub_cliente = $servicios["id_sub_cliente"];
        $id_tipo_servicios = $servicios["tipo_servicio"];
        $id_tipo_carga = $servicios["tipo_carga"];

        $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
        $consultar_cliente->execute();
        $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_cliente as $cliente) {
        }
        $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
        $consultar_sub_cliente->execute();
        $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_sub_cliente as $sub_cliente) {
        }

        $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
        $consultar_tipo_servicio->execute();
        $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_tipo_servicio as $tipo_servicio) {
        }
        $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
        $consultar_tipo_carga->execute();
        $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
        foreach ($consultar_tipo_carga as $tipo_carga) {
        }
    }



    $fecha_generado = date("Y-m-s H:i");
    $nombre_archivo = 'MI-TIR-FO' . $id_servicio_registrado . ' CONTROL DE VEHICULOS EN RUTA ' . $fecha_generado;
    $spreadsheet = new SpreadSheet();
    $spreadsheet->getProperties()->setCreator("WORLDSHIPPINGCOMPANYSAS")->setTitle($nombre_archivo);

    $spreadsheet->setActiveSheetIndex(0);
    $hojaActiva = $spreadsheet->getActiveSheet();

    $spreadsheet->getDefaultStyle()->getFont()->setName("Calibri");
    $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
    $spreadsheet->getActiveSheet()->getSheetView()->setZoomScale(90);
    $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(70);
    $spreadsheet->getActiveSheet()->getRowDimension('3')->setRowHeight(5);
    //$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(5);

    $spreadsheet->getActiveSheet()->getRowDimension('41')->setRowHeight(41);
    ////altura automatica celdas de observacion
    $spreadsheet->getActiveSheet()->getRowDimension('4,1000')->setRowHeight(-1);

    foreach (range('A', 'Z') as $char) {
        $spreadsheet->getActiveSheet()->getStyle($char)->getAlignment()->setWrapText(true);
    }
    foreach (range('A', 'Z') as $char) {
        foreach (range('A', 'Z') as $char2) {
            $spreadsheet->getActiveSheet()->getStyle($char . $char2)->getAlignment()->setWrapText(true);
        }
    }
    $spreadsheet->getActiveSheet()->getStyle('g')->getAlignment()->setWrapText(true);
    ////////////////
    $hojaActiva->getColumnDimension('A')->setWidth(5);
    $hojaActiva->getColumnDimension('B')->setWidth(25);
    $hojaActiva->getColumnDimension('C')->setWidth(25);
    $hojaActiva->getColumnDimension('D')->setWidth(25);
    $hojaActiva->getColumnDimension('E')->setWidth(25);
    $hojaActiva->getColumnDimension('F')->setWidth(25);

    //bordes

    $styleArray1 = array(
        'borders' => array(
            'allBorders' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),

    );
    $styleArray2 = array(
        'borders' => array(
            'outline' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),
        'font' => array(
            'bold' => true
        )

    );
    $styleArray3 = array(
        'borders' => array(
            'allBorders' => array(
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),
        'font' => array(
            'bold' => true
        )

    );
    $style_titles = array(
        'font' => array(
            'bold' => true
        )
    );
    $hojaActiva->getStyle('A1:F1')->applyFromArray($styleArray2);
    $hojaActiva->getStyle('A2:F2')->applyFromArray($styleArray2);

    $hojaActiva->getStyle('A4:z4')->applyFromArray($styleArray3);
    $hojaActiva->getStyle('AA4:ZZ4')->applyFromArray($styleArray3);
    // $hojaActiva->getStyle('A8:F39')->applyFromArray($styleArray1);


    $spreadsheet->getActiveSheet()->getStyle('A1:F1')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF');


    ///////

    $hojaActiva->mergeCells('D1:f1');
    $hojaActiva->mergeCells('A2:f2');
    $hojaActiva->mergeCells('A3:f3');
    //$hojaActiva->mergeCells('G4:Z4');

    ///// logo titulo

    $hojaActiva->setCellValue('D1', 'CONTROL DE VEHICULOS EN RUTA')->getStyle('D1')->getAlignment()->setHorizontal('center');
    $hojaActiva->setCellValue('D1', 'CONTROL DE VEHICULOS EN RUTA')->getStyle('D1')->getAlignment()->setVertical('center');

    $logo = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
    $logo->setName('Logo');
    $logo->setDescription('Logo');
    $logo->setPath('../../assets/images/logo.png'); // put your path and image here
    $logo->setCoordinates('B1');
    $logo->setOffsetX(80);
    $logo->setOffsetY(8);
    $logo->setRotation(0);
    $logo->setWidthAndHeight(70, 100);
    /*$logo->getShadow()->setVisible(true);
    $logo->getShadow()->setDirection(45);*/
    $logo->setWorksheet($spreadsheet->getActiveSheet());

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////// encabezado 
    $hojaActiva->setCellValue('A2', 'VERSIÓN: 2           CODIGO: MI-TR-FO' . $id_servicio_registrado . '           FECHA: ' . $fecha_generado . '           PAGINA 1 DE 1')->getStyle('A2')->getAlignment()->setHorizontal('center');


    //////// encabezado de tabla de los servicios
    if ($id_servicio == 1) { // itr importacion
        $encabezado = [
            "Id servicio", "Cliente", "Sub cliente", "Reserva", "Tipo servicio",
            "Tipo de contenedor", "Número contenedor", "Tamaño contenedor", "Linea naviera",
            "Puerto retiro", "Fecha retiro puerto del vacío", "Lugar de entrega del vacío", "Fecha de entrega del vacío",
            "Fecha vencimiento bodegaje puerto", "Número de placa del vehículo", "Nombre del conductor del vehículo",
            "Identificación del conductor", "Ruta", "Fecha y hora de retiro del producto (origen)", "Fecha y hora de inicio descargue",
            "Fecha y hora de terminación descargue", "Cantidad bultos", "Cantidad pallets total", "Aplica almacenamiento",
            "Número de día de almacenaje", "Días de almacenaje libre", "Cubicaje", "Observaciones descargue"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
        ///// servicios 1
        $numeroDeFila = 5;
        foreach ($consultar_servicios as $servicios) {
            $id_servicio_registrado = $servicios["id"];
            $id_cliente = $servicios["id_cliente"];
            $id_sub_cliente = $servicios["id_sub_cliente"];
            $id_tipo_servicios = $servicios["tipo_servicio"];
            $id_tipo_carga = $servicios["tipo_carga"];
            if ($servicios["aplica_almacenaje"] == 0) {
                $almacenaje = "No";
            } else {
                $almacenaje = "Si";
            }
            $consultar_cliente = $conn->prepare("SELECT * FROM clientes WHERE id = '$id_cliente'");
            $consultar_cliente->execute();
            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_cliente as $cliente) {
            }
            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente'");
            $consultar_sub_cliente->execute();
            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_sub_cliente as $sub_cliente) {
            }

            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicios'");
            $consultar_tipo_servicio->execute();
            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_servicio as $tipo_servicio) {
            }
            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
            $consultar_tipo_carga->execute();
            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
            foreach ($consultar_tipo_carga as $tipo_carga) {
            }
            // inicio serivicico
            $hojaActiva->setCellValueByColumnAndRow(1, $numeroDeFila, $servicios["id"]);
            $hojaActiva->setCellValueByColumnAndRow(2, $numeroDeFila, $cliente["razon_social"]);
            $hojaActiva->setCellValueByColumnAndRow(3, $numeroDeFila, $sub_cliente["nombre_sub_cliente"]);
            $hojaActiva->setCellValueByColumnAndRow(4, $numeroDeFila, $servicios["dons"]);
            $hojaActiva->setCellValueByColumnAndRow(5, $numeroDeFila, $tipo_servicio["nombre_servicio"]);
            $hojaActiva->setCellValueByColumnAndRow(6, $numeroDeFila, $tipo_carga["nombre_carga"]);
            $hojaActiva->setCellValueByColumnAndRow(7, $numeroDeFila, $servicios["contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(8, $numeroDeFila, $servicios["tamaño_contenedor"]);
            $hojaActiva->setCellValueByColumnAndRow(9, $numeroDeFila, $servicios["linea_naviera"]);
            $hojaActiva->setCellValueByColumnAndRow(10, $numeroDeFila, $servicios["puerto_origen"]);
            $hojaActiva->setCellValueByColumnAndRow(11, $numeroDeFila, $servicios["fecha_hora_r_p"]);
            $hojaActiva->setCellValueByColumnAndRow(12, $numeroDeFila, $servicios["lugar_entrega"]);
            $hojaActiva->setCellValueByColumnAndRow(13, $numeroDeFila, $servicios["fecha_lugar_entrega_v"]);
            $hojaActiva->setCellValueByColumnAndRow(14, $numeroDeFila, $servicios["fecha_vencimiento_b_p_zf"]);
            //transporte
            $hojaActiva->setCellValueByColumnAndRow(15, $numeroDeFila, $servicios["numero_placa_vehiculo"]);
            $hojaActiva->setCellValueByColumnAndRow(16, $numeroDeFila, $servicios["nombre_conductor"]);
            $hojaActiva->setCellValueByColumnAndRow(17, $numeroDeFila, $servicios["identificacion_conductor"]);
            $hojaActiva->setCellValueByColumnAndRow(18, $numeroDeFila, $servicios["ruta"]);
            $hojaActiva->setCellValueByColumnAndRow(19, $numeroDeFila, $servicios["fecha_r_p_p_o"]);
            //Descargue mercancia}
            $hojaActiva->setCellValueByColumnAndRow(20, $numeroDeFila, $servicios["fecha_hora_incio_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(21, $numeroDeFila, $servicios["fecha_hora_terminacion_descargue"]);
            $hojaActiva->setCellValueByColumnAndRow(22, $numeroDeFila, $servicios["cantidad_bultos"]);
            $hojaActiva->setCellValueByColumnAndRow(23, $numeroDeFila, $servicios["cantidad_pallets"]);
            $hojaActiva->setCellValueByColumnAndRow(24, $numeroDeFila, $almacenaje);
            $hojaActiva->setCellValueByColumnAndRow(25, $numeroDeFila, $servicios["numero_dia_almacenaje"]);
            $hojaActiva->setCellValueByColumnAndRow(26, $numeroDeFila, $servicios["dia_almacenaje_libre"]);
            $hojaActiva->setCellValueByColumnAndRow(27, $numeroDeFila, $servicios["cubicaje"]);
            $hojaActiva->setCellValueByColumnAndRow(28, $numeroDeFila, $servicios["observaciones_carga"]);


            $numeroDeFila++;
        }
    } else if ($id_servicio == 2) {
        $encabezado = [
            "Id servicio", "Cliente", "Sub cliente", "Reserva", "Tipo servicio",
            "Tipo de contenedor", "Número contenedor", "Tamaño contenedor", "Linea naviera",
            "Puerto retiro", "Fecha retiro puerto del vacío", "Fecha de entrega del vacío", "Lugar de entrega del vacío",
            "Fecha vencimiento traslado zf"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
    } else if ($id_servicio == 3) {
        $encabezado = [
            "Id servicio", "Cliente", "Sub cliente", "Reserva", "Tipo servicio",
            "Tipo de contenedor", "Número contenedor", "Tamaño contenedor",
            "Puerto retiro del vacío", "Fecha retiro puerto del vacío",   "Lugar de entrega del vacío", "Fecha de entrega del vacío"
        ];
        $hojaActiva->fromArray($encabezado, null, 'A4');
    }


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombre_archivo . '.xlsx"');
    header('Cache-Control: max-age=0');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
}