<?php
session_start();
include '../../database/database.php';

$id_cargo = $_SESSION["cargo"];

if ($id_cargo == 1) {
    $disabled_carga = '';
} else if ($id_cargo == 2) {
    $disabled_transporte = '';
}
$consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE estado = 1 ORDER BY id DESC");
$consultar_cliente->execute();
$consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE estado = 1");
$consultar_tipo_servicio->execute();
$consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE estado = 1 ORDER BY id DESC");
$consultar_tipo_carga->execute();
$consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);

$consultar_puerto_origen = $conn->prepare("SELECT * FROM puertos WHERE estado = 1 ORDER BY id DESC");
$consultar_puerto_origen->execute();
$consultar_puerto_origen = $consultar_puerto_origen->fetchAll(PDO::FETCH_ASSOC);



?>
<link href="../../assets/plugins/select2.css" rel="stylesheet" />
<script src="../../assets/plugins//select2.js"></script>
<link type="text/css" rel="stylesheet" href="http://worldshippingcompany.com.co/assets/plugins/steps/jquery.steps.css">
<script src="http://worldshippingcompany.com.co/assets/plugins/steps/jquery.steps.js"></script>

<center>
    <div id="selector_servicios">
        <label for="select_servicio">Seleccionar un servicio</label>
        <select class="form-control" oninput="seleccionar_servicio()" name="select_servicio" id="select_servicio">
            <option value="">Seleccione una opción</option>
            <?php foreach ($consultar_tipo_servicio as $tipo_servicio) { ?>
            <option value="<?php echo $tipo_servicio['id'] ?>">
                <?php echo $tipo_servicio['nombre_servicio'] ?>
            </option>
            <?php } ?>
        </select>
    </div>
</center>
<div id="respuesta_form_servicio_inicio"></div>
<div id="mostrar_formulario" style="display:none" data-scrollbar-shown="true" data-scrollable="true"
    style=" overflow: hidden; overflow-y: auto;">

    <center>
        <h6 id="tipo_s"></h6>
        <hr>
    </center>
    <form id="form_1" style="padding: 20px;  height: 80vh !important; overflow-x: hidden;overflow-y: auto;">
        <div class="row">
            <!-- <div class="col-12 col-sm-6">
                <label for="">Fecha recepción documento</label>

                <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc" class="form-control"
                    placeholder="Seleccionar una fecha" <?php echo $disabled_transporte ?>>
            </div>-->
            <div class="col-12 col-sm-6">
                <label for="">Cliente</label>
                <select class="form-control" style="width:100% !important;" onchange="consultar_subcliente()"
                    name="id_cliente" id="cliente" <?php echo $disabled_transporte ?>>
                    <option value="" selected>Seleccione una opción</option>
                    <?php
                    foreach ($consultar_cliente as $cliente) {
                    ?>
                    <option value="<?php echo $cliente["id"] ?>"><?php echo $cliente["razon_social"] ?>
                    </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12 col-sm-6">
                <label for="">Subcliente</label>
                <div id="sub_clientes">
                    <select class="form-control" style="width:100% !important;" name="id_sub_cliente" id="sub_cliente"
                        <?php echo $disabled_transporte ?>>
                        <option value="" selected>Seleccione un cliente</option>
                    </select>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <label for="">Do/Ns</label>
                <input type="tel" id="dons" name="dons" class="form-control" <?php echo $disabled_transporte ?>>
            </div>

            <div class="col-12 col-sm-6">
                <label for="">Tipo de servicio</label>
                <div id="opciones_servicios"></div>
            </div>
            <div class="col-12 col-sm-6" id="tipo_carga">
                <label for="">Tipo de carga</label>
                <select class="form-control" name="tipo_carga" id="tipo_carga" style="width:100% !important;"
                    <?php echo $disabled_transporte ?>>
                    <?php foreach ($consultar_tipo_carga as $tipo_carga) { ?>
                    <option value="<?php echo $tipo_carga['id'] ?>" selected>
                        <?php echo $tipo_carga['nombre_carga'] ?>
                    </option>
                    <?php } ?>
                </select>

            </div>
            <div class="col-12 col-sm-6" id="cantidad_carga_recibidos">
                <label for="">Cantidad recibidos</label>
                <input type="text" class="form-control" id="cantidad_carga_recibidos" name="cantidad_carga_recibidos"
                    <?php echo $disabled_carga ?>>

            </div>
            <!-- <div class="col-12 col-sm-6">
                            <label for="">Seleccionar imporetiro o expoingreso</label>
                            <select name="impoexpo" style="width:100% !important;" class="form-control" id="impoexpo"
                                <?php echo $disabled_transporte ?>>
                                <option value="" selected>Seleccione una opción</option>
                                <option value="IMPORETIRO">IMPORETIRO</option>
                                <option value="EXPOINGRESO">EXPOINGRESO</option>
                            </select>

                        </div>-->
            <div class="col-12 col-sm-6" id="tipo_contenedor">
                <label for="">Tipo de contenedor</label>
                <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="HC">HC</option>
                    <option value="DRY">DRY</option>
                    <option value="OPEN TOP">OPEN TOP</option>
                    <option value="REEFER HC">REEFER HC</option>
                    <option value="REERFER">REERFER</option>
                    <option value="FALT RACK">FALT RACK</option>
                    <option value="TANQUE">TANQUE</option>

                </select>
            </div>
            <div class="col-12 col-sm-6" id="contenedors">
                <label for="">Número de contenedor</label>
                <input type="text" name="contenedor" id="contenedor" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="tamaño_contenedor">
                <label for="">Tamaño contenedor</label>
                <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                    <?php echo $disabled_transporte ?>>
                    <option value="">Seleccione una opción</option>
                    <option value="20">20</option>
                    <option value="40">40</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="linea_naviera">
                <label for="">Linea naviera</label>
                <input type="text" name="linea_naviera" id="linea_naviera" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>
            <!-- <div class="col-12 col-sm-6">
                            <label for="">Medio recepcion documentos</label>
                            <input type="text" name="recepcion_doc" id="recepcion_doc" class="form-control"
                                <?php echo $disabled_transporte ?>>

                        </div>-->
            <div class="col-12 col-sm-6">
                <label for="">Puerto de retiro del vacío</label>

                <select class="form-control" style="width:100% !important;" onchange="puerto_otro()"
                    name="puerto_origen" id="puerto_origen" <?php echo $disabled_transporte ?>>
                    <option value="" selected>Seleccione una opción</option>
                    <?php
                    foreach ($consultar_puerto_origen as $puertos) {
                    ?>
                    <option value="<?php echo $puertos["nombre"] ?>"><?php echo $puertos["nombre"] ?>
                    </option>
                    <?php } ?>
                    <option value="otro">Otro</option>
                </select>
                <p id="puerto_origen_otro"></p>

            </div>
            <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                <label for="">Fecha retiro puerto</label>
                <input type="datetime-local" class="form-control " name="fecha_hora_r_p" id="fecha_hora_r_p"
                    <?php echo $disabled_transporte ?>>
            </div>


            <!-- <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                <label for="">Fecha de entrega del vacío</label>
                <input type="date" class="form-control " name="fecha_lugar_entrega_v" id="fecha_devolucion_v"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="lugar_descargue">
                <label for="">Lugar de entrega del vacío</label>
                <input type="text" name="lugar_entrega" id="lugar_descargue" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>-->


            <div id="fechas_bodegaje_traslado_zf" class="col-12 col-sm-6"></div>

            <div class="col-12 col-sm-6" id="lugar_entrega_zona_f_1" style="display:none">
                <label for="">Lugar de entrega zona franca</label>
                <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>

            <div class="col-12 col-sm-6" id="fecha_entrega_zona_f_1" style="display:none">
                <label for="">Fecha de entrega en zona franca</label>
                <input type="date" class="form-control " name="fecha_entrega_zona_f" id="fecha_entrega_zona_f"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="patio_retiro" style="display:none">
                <label for="">Patio retiro del vacío</label>
                <input type="text" name="patio_retiro_vacio" id="patio_retiro_vacio" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="fecha_patio_retiro" style="display:none">
                <label for="">Fecha retiro patio del vacío</label>
                <input type="date" class="form-control " name="fecha_patio_retiro_vacio" id="fecha_patio_retiro_vacio"
                    <?php echo $disabled_transporte ?>>
            </div>


            <div class="col-12 col-sm-6" id="linea_naviera">
                <label for="">Observación</label>
                <input type="text" name="observacion1" id="observacion1" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>
            <div class="col-12 col-sm-6">
                <hr>
                <a href="javascript:void(0)" onclick="guardar_form_1()" style="width: 60%;margin-top: -7px;"
                    class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                    <div class="ht-40 justify-content-between">
                        <span class="pd-x-15">Guardar datos</span>
                        <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                    </div>
                </a>

            </div>
        </div>
    </form>

</div>

<div id="mostrar_formulario_no_itrs" style="display:none">

    <center>
        <h6 id="tipo_s_1"></h6>
        <hr>
    </center>

    <form id="form_1" style="padding: 20px;  height: 80vh !important; overflow-x: hidden;overflow-y: auto;">
        <div class="row">
            <div class="col-12 col-sm-6">
                <label for="">Cliente</label>
                <select class="form-control" style="width:100% !important;" onchange="consultar_subcliente()"
                    name="id_cliente" id="cliente" <?php echo $disabled_transporte ?>>
                    <option value="" selected>Seleccione una opción</option>
                    <?php
                    foreach ($consultar_cliente as $cliente) {
                    ?>
                    <option value="<?php echo $cliente["id"] ?>"><?php echo $cliente["razon_social"] ?>
                    </option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-12 col-sm-6">
                <label for="">Subcliente</label>
                <div id="sub_clientes">
                    <select class="form-control" style="width:100% !important;" name="id_sub_cliente" id="sub_cliente"
                        <?php echo $disabled_transporte ?>>
                        <option value="" selected>Seleccione un cliente</option>
                    </select>
                </div>
            </div>

            <div class="col-12 col-sm-6">
                <label for="">Do/Ns</label>
                <input type="tel" id="dons" name="dons" class="form-control" <?php echo $disabled_transporte ?>>
            </div>

            <div class="col-12 col-sm-6">
                <label for="">Tipo de servicio</label>

                <div id="opciones_servicios_1"></div>

            </div>
            <div class="col-12 col-sm-6" id="tipo_carga" style="display:none">
                <label for="">Tipo de carga</label>
                <select class="form-control" name="tipo_carga" id="tipo_carga" style="width:100% !important;"
                    <?php echo $disabled_transporte ?>>
                    <?php foreach ($consultar_tipo_carga as $tipo_carga) { ?>
                    <option value="<?php echo $tipo_carga['id'] ?>" selected>
                        <?php echo $tipo_carga['nombre_carga'] ?>
                    </option>
                    <?php } ?>
                </select>

            </div>
            <div class="col-12 col-sm-6" id="cantidad_pallets_despachos" style="display:none">
                <label for="">Cantidad pallets</label>
                <input type="text" class="form-control" id="cantidad_pallets_despacho" name="cantidad_pallets"
                    <?php echo $disabled_carga ?>>

            </div>
            <div class="col-12 col-sm-6" id="linea_naviera" style="display:none">
                <label for="">Linea naviera</label>
                <input type="text" name="linea_naviera" id="linea_naviera" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>
            <div class="col-12 col-sm-6" id="lugar_descargue" style="display:none">
                <label for="">Lugar de entrega del vacío</label>
                <input type="text" name="lugar_entrega" id="lugar_descargue" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>
            <div id="fechas_bodegaje_traslado_zf" class="col-12 col-sm-6" style="display:none"></div>

            <div class="col-12 col-sm-6" id="fecha_devolucion_v" style="display:none">
                <label for="">Fecha de entrega del vacío</label>
                <input type="date" class="form-control " name="fecha_lugar_entrega_v" id="fecha_devolucion_v"
                    <?php echo $disabled_transporte ?>>
            </div>

            <div class="col-12 col-sm-6" id="tipo_contenedor">
                <label for="">Tipo de contenedor</label>
                <select name="tipo_contenedor" id="tipo_contenedor" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="HC">HC</option>
                    <option value="DRY">DRY</option>
                    <option value="OPEN TOP">OPEN TOP</option>
                    <option value="REEFER HC">REEFER HC</option>
                    <option value="REERFER">REERFER</option>
                    <option value="FALT RACK">FALT RACK</option>
                    <option value="TANQUE">TANQUE</option>

                </select>
            </div>
            <div class="col-12 col-sm-6" id="numero_contendors">
                <label for="">Número de contenedor</label>
                <input type="text" name="contenedor" id="contenedor" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="tamaño_contenedor_1" style="display:none">
                <label for="">Tamaño contenedor</label>
                <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                    <?php echo $disabled_transporte ?>>
                    <option value="">Seleccione una opción</option>
                    <option value="20">20</option>
                    <option value="40">40</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="select_almacenaje" style="display:none">
                <label for="">¿Tendrá almacenaje el servicio?</label>
                <select id="select_almacenar" style="width:100%" class="form-control" oninput="almacenaje()">
                    <option value="0">Selecciones una opción</option>
                    <option value="1">Si</option>
                    <option value="0">No</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="puerto_origens" style="display:none">
                <label for="">Puerto de retiro del vacío</label>
                <input type="text" name="puerto_origen" id="puerto_origen" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>
            <div class="col-12 col-sm-6" id="fecha_retiro_puerto" style="display:none">
                <label for="">Fecha retiro puerto del vacío</label>
                <input type="datetime-local" class="form-control " name="fecha_hora_r_p" id="fecha_hora_r_p"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="peso_retiros" style="display:none">
                <label for="">Peso retirado</label>
                <input type="text" name="peso_retiro" id="peso_retiro" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>

            <div class="col-12 col-sm-6" id="lugar_entrega_zona_f_1" style="display:none">
                <label for="">Lugar de entrega zona franca</label>
                <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f" class="form-control"
                    <?php echo $disabled_transporte ?>>

            </div>

            <div class="col-12 col-sm-6" id="fecha_entrega_zona_f_1" style="display:none">
                <label for="">Fecha de entrega en zona franca</label>
                <input type="date" class="form-control " name="fecha_entrega_zona_f" id="fecha_entrega_zona_f"
                    <?php echo $disabled_transporte ?>>
            </div>


            <div class="col-12 col-sm-6" id="patio_retiro" style="display:none">
                <label for="">Patio retiro del vacío</label>
                <input type="text" name="patio_retiro_vacio" id="patio_retiro_vacio" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="fecha_patio_retiro" style="display:none">
                <label for="">Fecha retiro patio del vacío</label>
                <input type="date" class="form-control " name="fecha_patio_retiro_vacio" id="fecha_patio_retiro_vacio"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="puerto_entrega_vacios" style="display:none">
                <label for="">Puerto entrega del vacío</label>
                <input type="text" name="puerto_entrega_vacio" id="puerto_entrega_vacio" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="fecha_entrega_vacios" style="display:none">
                <label for="">Fecha de entrega del del vacío</label>
                <input type="date" class="form-control " name="fecha_puerto_entrega_vacio"
                    id="fecha_puerto_entrega_vacio" <?php echo $disabled_transporte ?>>
            </div>

            <div class="col-12 col-sm-6" id="lugar_entrega_1" style="display:none">
                <label for="">Lugar de entrega del vacío</label>
                <input type="text" name="lugar_entrega" id="lugar_entrega" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="fecha_lugar_entrega_v_1" style="display:none">
                <label for="">Fecha de entrega del vacío</label>
                <input type="date" class="form-control " name="fecha_lugar_entrega_v" id="fecha_lugar_entrega_v"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="tipo_vehiculo" style="display:none">
                <label for="">Seleccionar vehículo acarreo</label>
                <select name="vehiculo_acarreo" id="vehiculo_acarreo" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="Furgón">Furgón</option>
                    <option value="Patineta">Patineta</option>
                    <option value="Mula">Mula</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="lugar_ingresos" style="display:none">
                <label for="">Lugar ingreso</label>
                <input type="text" name="lugar_ingreso" id="lugar_ingreso" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="nombre_bodegas" style="display:none">
                <label for="">Nombre bodega</label>
                <input type="text" name="nombre_bodega" id="nombre_bodega" class="form-control">
            </div>
            <div class="col-12 col-sm-6" id="fecha_vaciados" style="display:none">
                <label for="">Fecha de vaciado</label>
                <input type="date" name="fecha_vaciado" id="fecha_vaciado" class="form-control">
            </div>
            <div class="col-12 col-sm-6" id="dia_almacenaje_libres" style="display:none">
                <label for="">Días de almacenaje libre</label>
                <input type="text" class="form-control" placeholder="Días de almacenaje" <?php echo $disabled_carga ?>
                    value="<?php echo $servicios["dia_almacenaje_libre"] ?>" id="dia_almacenaje_libre"
                    name="dia_almacenaje_libre">
                <h6 style="font-size:10px"><b>Dejar el campo vacío si no tiene días de almacenaje libre*</b>
                </h6>
            </div>
            <div class="col-12 col-sm-6" id="tipo_despachos" style="display:none">
                <label for="">Tipo de despacho</label>
                <select name="tipo_despacho" id="tipo_despacho" onchange="tipos_despachos()" class="form-control"
                    <?php echo $disabled_carga ?>>
                    <option value="" selected>Selecione una opción</option>
                    <option value="1">Metros cuadrados almacenados</option>
                    <option value="2">Toneladas almacenadas</option>
                    <option value="3">Número de pallets</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="cantidad_despachos" style="display:none">
                <label for="" id="opcion_label"></label>
                <input type="text" name="cantidad_despacho" id="cantidad_despacho" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>

            <div class="col-12 col-sm-6" id="lugar_operacions" style="display:none">
                <label for="">Lugar operación</label>
                <input type="text" name="lugar_operacion" id="lugar_operacion" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="fecha_operacions" style="display:none">
                <label for="">Fecha de operación</label>
                <input type="date" name="fecha_operacion" id="fecha_operacion" class="form-control">
            </div>
            <div class="col-12 col-sm-6" id="horas_operacions" style="display:none">
                <label for="">Horas de operación</label>
                <input type="text" name="hora_operacion" id="hora_operacion" class="form-control">
            </div>

            <div class="col-12 col-sm-6" id="lugar_entrega_estibas" style="display:none">
                <label for="">Lugar entrega</label>
                <input type="text" name="lugar_entrega_estiba" id="lugar_entrega_estiba" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="fecha_entrega_estibas" style="display:none">
                <label for="">Fecha de entrega</label>
                <input type="date" name="fecha_entrega_estiba" id="fecha_entrega_estiba" class="form-control">
            </div>

            <div class="col-12 col-sm-6" id="cantidad_estibas" style="display:none">
                <label for="">Cantidad entrega</label>
                <input type="text" name="cantidad_estiba_entregada" id="cantidad_estiba_entregada" class="form-control"
                    <?php echo $disabled_transporte ?>>
            </div>
            <div class="col-12 col-sm-6" id="patelizos" style="display:none">
                <label for="">¿Se patelizó?</label>
                <select name="patelizo" id="patelizo" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="Sí">Sí</option>
                    <option value="No">No</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="lleno_contenedors" style="display:none">
                <label for="">¿Se llenó el contenedor?</label>
                <select name="lleno_contenedor" id="lleno_contenedor" class="form-control">
                    <option value="">Seleccione una opción</option>
                    <option value="Sí">Sí</option>
                    <option value="No">No</option>
                </select>
            </div>
            <div class="col-12 col-sm-6" id="hora_montacargas" style="display:none">
                <label for="">Horas de montacargas</label>
                <input type="text" name="hora_montacarga" id="hora_montacarga" class="form-control">
            </div>

            <div class="col-12 col-sm-6">
                <hr>
                <a href="javascript:void(0)" onclick="guardar_form_1()" style="width: 60%;margin-top: -7px;"
                    class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte ?> ">
                    <div class="ht-40 justify-content-between">
                        <span class="pd-x-15">Guardar datos</span>
                        <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                    </div>
                </a>

            </div>
        </div>
    </form>
</div>



<script>
function puerto_otro() {

    var puerto = document.getElementById("puerto_origen").value;

    if (puerto === "otro") {
        var elementoClonado = $("#" + "puerto_origen").clone();
        elementoClonado.attr('id', 'nuevo id');
        elementoClonado.attr('name', 'nuevo nombre');

        document.getElementById("puerto_origen_otro").innerHTML =
            '<input type="text" name="puerto_origen" id="puerto_origen" placeholder="Escribir puerto..."  class="form-control">';
    } else {
        document.getElementById("puerto_origen_otro").innerHTML = '';
    }

}

function tipos_despachos() {
    var select = document.getElementById("tipo_despacho").value;
    if (select == 1) {
        $("#cantidad_despachos").show();
        $("#opcion_label").html("Metros cuadros almacenados");
    } else if (select == 2) {
        $("#cantidad_despachos").show();
        $("#opcion_label").html("Tolenadas almacenados");
    } else if (select == 3) {
        $("#cantidad_despachos").show();
        $("#opcion_label").html("Número de pallets");
    } else if (select == "") {
        $("#cantidad_despachos").hide();
    }
}

function almacenaje() {
    var select = document.getElementById("select_almacenar").value;
    var id_servicio = document.getElementById("select_servicio").value;
    if (select == 0) {
        if (id_servicio == 3) {
            $("#lugar_entrega_1").show();
            $("#fecha_lugar_entrega_v_1").show();
        } else if (id_servicio == 4) {

            $("#patio_retiro").show();
            $("#fecha_patio_retiro").show();
            $("#puerto_entrega_vacios").show();
            $("#fecha_entrega_vacios").show();
        }
    } else if (select == 1) {
        if (id_servicio == 4) {


            $("#patio_retiro").show();
            $("#fecha_patio_retiro").show();
            $("#puerto_entrega_vacios").hide();
            $("#fecha_entrega_vacios").hide();

        } else {
            $("#lugar_entrega_1").hide();
            $("#fecha_lugar_entrega_v_1").hide();
            $("#patio_retiro").hide();
            $("#fecha_patio_retiro").hide();
        }

    }
}

function seleccionar_servicio() {
    var id = document.getElementById("select_servicio").value;
    var nombre_servicio = $("#select_servicio option:selected").text();
    $("#tipo_s").html("Servicio: " + nombre_servicio);
    $("#opciones_servicios").html(
        '<select class="form-control" onchange="fechas_bodegaje_traslado_zf()" style="width:100% !important;" name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>> <option value="' +
        id + '">' + nombre_servicio + '</option> </select>');
    fechas_bodegaje_traslado_zf();
    $("#tipo_s_1").html("Servicio: " + nombre_servicio);
    $("#opciones_servicios_1").html(
        '<select class="form-control" oninput="fechas_bodegaje_traslado_zf()" style="width:100% !important;" name="tipo_servicio" id="tipo_servicios" <?php echo $disabled_transporte ?>> <option value="' +
        id + '">' + nombre_servicio + '</option> </select>');


    if (id == 1) {
        //    alert(id);
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").remove();
        $("#mostrar_formulario").show();

    } else if (id == 2) {
        $("#mostrar_formulario").show();
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").remove();
    } else if (id == 3) {
        $("#mostrar_formulario").remove();
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#select_almacenaje").show();
        $("#puerto_origens").show();
        $("#fecha_retiro_puerto").show();

    } else if (id == 4) {
        $("#patio_retiro").show();
        $("#fecha_patio_retiro").show();
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#patio_retiro").show();
        $("#fecha_patio_retiro").show();
        $("#puerto_entrega_vacios").show();
        $("#fecha_entrega_vacios").show();
        $("#select_almacenaje").show();
    } else if (id == 5) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();
        $("#puerto_origens").show();
        $("#fecha_retiro_puerto").show();
        $("#tamaño_contenedor_1").show();
        $("#lugar_entrega_zona_f_1").show();
        $("#fecha_entrega_zona_f_1").show();

    } else if (id == 6) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#numero_contendors").hide();
        $("#cantidad_pallets_despachos").show();
        $("#lugar_entrega_1").show();
        $("#puerto_origens").show();
        $("#fecha_retiro_puerto").show();
    } else if (id == 7) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#puerto_origens").show();
        $("#fecha_retiro_puerto").show();
        $("#peso_retiros").show();
        $("#tipo_vehiculo").show();
        $("#lugar_ingresos").show();
        $("#lugar_ingresos").show();

        $("#numero_contendors").hide();

    } else if (id == 8) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();
        $("#puerto_origens").show();
        $("#fecha_retiro_puerto").show();
        $("#tamaño_contenedor_1").show();
        $("#lugar_entrega_zona_f_1").show();
        $("#fecha_entrega_zona_f_1").show();
    } else if (id == 8) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();


    } else if (id == 9) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#nombre_bodegas").show();
        $("#fecha_vaciados").show();
        $("#dia_almacenaje_libres").show();
        $("#tipo_despachos").show();
    } else if (id == 10) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#lugar_operacions").show();
        $("#fecha_operacions").show();
        $("#horas_operacions").show();
        //$("#tipo_despachos").show();
    } else if (id == 11) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#lugar_operacions").show();
        $("#fecha_operacions").show();
        $("#horas_operacions").show();
        //$("#tipo_despachos").show();
    } else if (id == 12) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#lugar_entrega_estibas").show();
        $("#fecha_entrega_estibas").show();
        $("#cantidad_estibas").show();
        $("#patelizos").show();
        $("#lleno_contenedors").show();
        $("#hora_montacargas").show();
        //$("#tipo_despachos").show();
    } else if (id == 13) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#lugar_entrega_estibas").show();
        $("#fecha_entrega_estibas").show();
        $("#cantidad_estibas").show();
        $("#patelizos").show();
        $("#lleno_contenedors").show();
        $("#hora_montacargas").show();

        //$("#tipo_despachos").show();
    } else if (id == 14) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#lugar_entrega_estibas").show();
        $("#fecha_entrega_estibas").show();
        $("#cantidad_estibas").show();
        $("#lleno_contenedors").show();
        $("#hora_montacargas").show();

        //$("#tipo_despachos").show();
    } else if (id == 15) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        $("#lugar_entrega_estibas").show();
        $("#fecha_entrega_estibas").show();
        $("#cantidad_estibas").show();
        $("#lleno_contenedors").show();
        $("#hora_montacargas").show();

        //$("#tipo_despachos").show();
    } else if (id == 16 || id == 17) {
        $("#selector_servicios").hide();
        $("#mostrar_formulario_no_itrs").show();
        $("#mostrar_formulario").remove();

        // $("#cargue_descargues").show();
        $("#lugar_operacions").show();
        $("#horas_operacions").show();

        //$("#tipo_despachos").show();
    }
}
let timeout;

function boton_atras() {
    formulario_añadir_servicio();
}
</script>
<script>
/*
$('#tipo_servicioss').selectpicker({
    liveSearch: true
});*/



function fechas_bodegaje_traslado_zf() {

    var tipo_servicio = document.getElementById("tipo_servicios").value;
    // alert(tipo_servicio)
    var url = "../../actions/actions_admin/consultar_fechas_bodegaje_p_traslado_zf.php?tipo_servicio=" + tipo_servicio;

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#fechas_bodegaje_traslado_zf").html("Cargando...");
        },
        success: function(data) {
            $("#fechas_bodegaje_traslado_zf").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });

}

$(document).ready(function() {
    $('#impoexpo').select2({
        width: 'resolve',
        dropdownParent: "#myModal"

    });
    $('#tipo_servicios').select2({
        width: 'resolve',
        dropdownParent: "#myModal"

    });
    $('#sub_cliente').select2({
        width: 'resolve',
        dropdownParent: "#myModal"

    });
    $('#cliente').select2({
        width: 'resolve',
        dropdownParent: "#myModal"

    });
    /* $('#tipo_carga').select2({
         width: 'resolve',
         dropdownParent: "#myModal"

     });*/

});
$('#basic33').selectpicker({
    liveSearch: true,
    maxOptions: 1
});
</script>

<script src="http://worldshippingcompany.com.co/assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>