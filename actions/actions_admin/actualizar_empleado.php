<?php

include '../../database/database.php';

$id_empleado = $_POST["id_empleado"];
$nombres = $_POST["nombres"];
$apellidos = $_POST["apellidos"];
$tipo_id = $_POST["tipo_id"];
$numero_identificacion = $_POST["numero_identificacion"];
$email = $_POST["email"];
$telefono = $_POST["telefono"];
$contraseña = $_POST["contraseña"];
$contraseña1 = $_POST["contraseña1"];
$cargo = $_POST["cargo"];
$estado = $_POST["estado"];
$foto_firma = $_FILES["foto_firma"]["name"];


if ($nombres == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el nombre del empleado.";
} else if ($apellidos == null) {
   $error = "error";
   $mensaje = "Por favor ingrese los apellidos del empleado.";
} else if ($tipo_id == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el tipo de documento.";
} else if ($numero_identificacion == null) {
   $error = "error";
   $mensaje = "Por favor ingrese un número de identificación.";
} else if ($email == null) {
   $error = "error";
   $mensaje = "Por favor ingrese email del empleado.";
} else if ($telefono == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el número de teléfono del empleado .";
} else if ($cargo == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el cargo del empleado.";
} else if ($estado == null) {
   $error = "error";
   $mensaje = "Por favor ingrese el cargo del empleado.";
} else {

   if ($contraseña == null) {

      $actualizar_empleado = $conn->prepare("UPDATE empleados SET 
    nombres = '$nombres',apellidos = '$apellidos',tipo_identificacion = '$tipo_id',numero_identificacion = '$numero_identificacion',email = '$email',telefono = '$telefono',cargo = '$cargo',estado = '$estado' WHERE id = '$id_empleado' ");

      $actualizar_empleado->execute();

      if ($actualizar_empleado === null) {
         $error = "error";
         $mensaje = "Hubo un error, inténtelo más tarde.";
      } else {
         $error = "success";
         $mensaje = "Empleado actualizado exitosamente.";
         if ($foto_firma == null) {
         } else {
            $directorio = "../../foto_firma_empleados/$id_empleado";
            if (!file_exists($directorio)) {
               mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
            }

            $dir = opendir($directorio);
            $filename = $foto_firma;
            $source = $_FILES["foto_firma"]["tmp_name"];
            $target_path = $directorio . '/' . $filename;
            move_uploaded_file($source, $target_path);

            if ($filename != null) {
               $guardar_foto = $conn->prepare("UPDATE empleados SET firma_digital = '$foto_firma'  where id ='$id_empleado'");
               $guardar_foto->execute();
            }
         }
      }
   } else {

      if ($contraseña != $contraseña1) {
         $error = "error";
         $mensaje = "Las contraseñas no coinciden.";
      } else {
         $actualizar_empleado = $conn->prepare("UPDATE empleados SET 
      nombres = '$nombres',apellidos = '$apellidos',tipo_identificacion = '$tipo_id',numero_identificacion = '$numero_identificacion',contraseña = '$contraseña', email = '$email',telefono = '$telefono',cargo = '$cargo',estado = '$estado' WHERE id = '$id_empleado' ");
         $actualizar_empleado->execute();

         if ($actualizar_empleado === null) {
            $error = "error";
            $mensaje = "Hubo un error, inténtelo más tarde.";
         } else {
            $error = "success";
            $mensaje = "Empleado actualizado exitosamente.";


            if ($foto_firma == null) {
            } else {
               $directorio = "../../foto_firma_empleados/$id_empleado";
               if (!file_exists($directorio)) {
                  mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
               }

               $dir = opendir($directorio);
               $filename = $foto_firma;
               $source = $_FILES["foto_firma"]["tmp_name"];
               $target_path = $directorio . '/' . $filename;
               move_uploaded_file($source, $target_path);

               if ($filename != null) {
                  $guardar_foto = $conn->prepare("UPDATE empleado SET firma_digital = '$foto_firma'  where id= '$id_empleado'");
                  $guardar_foto->execute();
               }
            }
         }
      }
   }
}



echo "<script> 

 ejecutar_boton();
 function ejecutar_boton() {
    if('$error' == 'success'){
     toastr.success('$mensaje','Hola!')
     setTimeout(function() {
 
     window.location.href = 'empleados';
     
     }, 1700);
    }else{
     toastr.error('$mensaje','Hola!')
    }
 }
 
 </script>";