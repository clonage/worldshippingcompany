<?php

include '../../database/database.php';

$id_cliente = $_GET["id_cliente"];

$consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id_cliente = '$id_cliente' AND estado = 1");
$consultar_sub_cliente->execute();
$consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);

$contador = count($consultar_sub_cliente);

?>



<select class="form-control" name="id_sub_cliente" id="sub_cliente" <?php echo $disabled_transporte?>>
    <option value="" selected>Seleccione una opción</option>
    <?php 
    if($contador > 0){
    foreach($consultar_sub_cliente as $sub_cliente){
    ?>
    <option value="<?php echo $sub_cliente["id"]?>"><?php echo $sub_cliente["nombre_sub_cliente"]?>
    </option>
    <?php }
    }else{
       echo ' <option value="">No hay registros de subclientes.
    </option>';
    }
    ?>
</select>