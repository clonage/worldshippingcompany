<?php

include '../../database/database.php';
session_start();

$id_servicio = $_GET["id_servicio"];


?>


<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

<head>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="" />
    <!-- Page Title -->
    <title>World Shipping Company</title>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/flag-icon/flag-icon.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/ionicons/css/ionicons.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/footable/footable.core.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/toastr/toastr.min.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/chartist/chartist.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/apex-chart/apexcharts.css">
    <link type="text/css" rel="stylesheet" href="../../assets/css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/css/style.min.css" />
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/datepicker/css/datepicker.min.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/bootstrap-select/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/steps/jquery.steps.css">


    <!-- Favicon -->
    <link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

</head>


<body>
    <!--================================-->
    <!-- Page Container Start -->
    <!--================================-->

    <div id="pdf" style="margin:100px; color: #006fae">

        <div class="row">
            <div class="col-12 col-sm-6">
                <img src="../../../../assets/images/logo.png" alt="" style="width:200px; margin-top: -37px;">
            </div>
            <div class="col-12 col-sm-6" style="text-align: right">
                <h5><b>WORLD SHIPPING COMPANY S.A.S</b></h5>
                <h6><b>TRANSPORTADORES DE CARGA</b></h6>
                <h6><b>NIT.900011819-1 RÉGIMEN COMÚN</b></h6>

                <h6>Bosque callejón Cano, Transversal 40 N° 21A-10</h6>
                <h6>Tel: 662 1378 - 662 1349 - Cartagena de Indias - Colombia</h6>
            </div>
        </div>

        <br><br>

        <div class="row" style="border: 1px solid #006fae; border-radius: 10px">
            <div class="col-12" style="margin:10px;">
                <h5><b>CARTA DE PORTE N° 08580 PARA DESPACHO MERCANCÍA</b></h5>
            </div>
            <div class="col-12">
                <div class="row"
                    style="background-color: #3781ad33; border: 1px solid black;  border-radius: 10px; margin: 10px;padding: 10px;border-color: #3781ad00;">
                    <div class="col-4"><b style="font-size: 15px;">DO.</b></div>
                    <div class="col-4"><b style="font-size: 15px;">PEDIDO</b></div>
                    <div class="col-4"><b style="font-size: 15px;">FECHA</b></div>
                </div>
            </div>
            <br>
            <div class="col-12" style="margin-left: 12px; margin-top: 10px; color: #006fae">
                <div style="display:flex">
                    <p><b>MANIFIESTO N°:</b></p>
                    <p>________________________________</p>
                </div>
                <p><b>MANIFIESTO N°:</b> ____________________________________________________</p>
                <p><b>CONOCIMIENTO DE EMBARQUE:</b> _________________________________________</p>
                <p><b>TRANSPORTADOR:</b> ____________________________________________________</p>
                <p><b>IMPORTADOR:</b> _______________________________________________________</p>
                <p><b>DESTINO:</b> __________________________________________________________</p>
                <p><b>DIRECCIÓN:</b> ________________________________________________________</p>
                <p><b>BULTOS:</b> _______________________________________________________</p>
                <p><b>CONTENIDO:</b> _______________________________________________________</p>
                <p><b>IMPORTADOR:</b> _______________________________________________________</p>
                <p><b>PESO APROX:</b> _______________________________________________________</p>
                <p><b>CONDUCTOR:</b> _______________________________________________________</p>
                <p><b>C.C.:</b> _______________________________________________________</p>
                <p><b>CEL:</b> _______________________________________________________</p>
                <p><b>PLACA:</b> _______________________________________________________</p>
            </div>
            <div class="col-12">
                <div class="row"
                    style="background-color: #3781ad33; border: 1px solid black;  border-radius: 10px; margin: 10px;padding: 10px;border-color: #3781ad00;">
                    <p><b>OBSERVACIONES:</b>
                        ___________________________________________________<br>_________<br>__________<br>_______</p>
                </div>
            </div>

            <div class="col-12" style="margin-left: 12px; margin-top: 10px;">
                <h6><b>CONDICIONES ESPECIALES:</b> La empresa transportadora se compromete a transportar el cargamento
                    detallado anteriormente bajo la responsabilidad mancomunada y solidaria del conductor y dueno del
                    camión y la enteegará en su destino de acuerdo con las instrucciones contenidas en la presenta Carta
                    de Porte, cuyo fiel entendimiento da por medio de su firma y selli al pie </h6>
            </div>

            <div class="row" style="margin-left: 12px; margin-top: 10px; width: 98%;  margin-bottom: 32px;">
                <div class="col-6">
                    <br><br>
                    <p style="width:100%; border: 0.5px solid  #006fae; border-bottom: 0px solid  #006fae"></p>
                    <h6>Firma y sello del Consignatario</h6>
                </div>
                <div class="col-6" style="text-align: right">
                    <br>
                    <br>
                    <p style="width:100%; border:  0.5px solid  #006fae; border-bottom: 0px solid  #006fae"></p>
                    <h6>Firma y sello del Consignatario</h6>
                </div>
            </div>

        </div>


    </div>

    <script src=" ../../assets/plugins/jquery/jquery.min.js">
    </script>
    <script src="../../assets/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="../../assets/plugins/popper/popper.js"></script>
    <script src="../../assets/plugins/feather-icon/feather.min.js"></script>
    <script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/plugins/pace/pace.min.js"></script>
    <script src="../../assets/plugins/toastr/toastr.min.js"></script>
    <script src="../../assets/plugins/countup/counterup.min.js"></script>
    <script src="../../assets/plugins/waypoints/waypoints.min.js"></script>
    <script src="../../assets/plugins/chartjs/chartjs.js"></script>
    <script src="../../assets/plugins/apex-chart/apexcharts.min.js"></script>
    <script src="../../assets/plugins/apex-chart/irregular-data-series.js"></script>
    <script src="../../assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js"></script>
    <script src="../../assets/js/dashboard/sales-dashboard-init.js"></script>
    <script src="../../assets/js/jquery.slimscroll.min.js"></script>
    <script src="../../assets/js/highlight.min.js"></script>
    <script src="../../assets/js/app.js"></script>
    <script src="../../assets/js/custom.js"></script>

    <script src="../../assets/plugins/footable/footable.all.min.js"></script>
    <script src="../../assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="../../assets/plugins/steps/jquery.steps.js"></script>
    <script src="../../assets/plugins/pace/pace.min.js"></script>

    <script src="../../assets/plugins/parsleyjs/parsley.js"></script>

    <script src="js/js-admin/main.js"></script>
    <script src="js/js-operador/main.js"></script>




</body>

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

</html>