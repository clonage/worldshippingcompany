<?php

include '../../database/database.php';

$id_conductor = $_GET["id_conductor"];

$consultar_inspeccion_vehiculo = $conn->prepare("SELECT * FROM control_inspeccion_vehiculo WHERE identificacion_conductor = '$id_conductor' ORDER BY id DESC ");
$consultar_inspeccion_vehiculo->execute();
$consultar_inspeccion_vehiculo = $consultar_inspeccion_vehiculo->fetchAll(PDO::FETCH_ASSOC);
session_start();
$identificacion_empleado = $_SESSION["numero_identificacion"];
$nombre_empleado = $_SESSION["nombre_admin"];

if ($identificacion_empleado == null) {
    $is_admin_identificacion = 0;
} else {
    $is_admin_identificacion = $_SESSION["numero_identificacion"];
}

?>
<table id="foo-filtering" class="table table-bordered table-hover toggle-circle table-responsive-sm"
    data-page-size="10">

    <thead>
        <tr>
            <th data-toggle="true" title="Id inspección">Id</th>
            <th data-toggle="true">Nombre conductor</th>
            <th data-hide="phone, tablet">Número de identificación</th>
            <th data-toggle="true">Placa vehículo</th>
            <th data-hide="phone, tablet">Coordinador/aux</th>
            <th data-hide="phone, tablet">Fecha/hora</th>
            <th data-toggle="true">Estado</th>
            <th data-toggle="true">Acción</th>
        </tr>
    </thead>
    <tbody>

        <?php
        foreach ($consultar_inspeccion_vehiculo as $inspeccion) {
        ?>
        <tr>
            <td><?php echo $inspeccion["id"] ?></td>
            <td id="responsive"><?php echo $inspeccion["conductor"] ?></td>
            <td><?php echo $inspeccion["identificacion_conductor"] ?></td>
            <td><?php echo $inspeccion["placa_vehiculo"] ?></td>
            <td id="responsive"><?php echo $inspeccion["nombre_empleado"] ?></td>
            <td id="responsive"><?php echo $inspeccion["fecha_creacion"] ?></td>
            <td>
                <?php
                    if ($inspeccion['estado'] == 0) {
                        echo '<span class="badge badge-outlined badge-danger">Sin revisar</span>';
                    } else {
                        echo '<span class="badge badge-outlined badge-success">Revisado</span>';
                    } ?>
            </td>
            <td>
                <!-- <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Ver detalles de inspección completa"
                    onclick="editar_inspeccion(<?php echo $inspeccion['id'] ?>)">
                    <button type="button" data-toggle="modal" data-target="#ver_inspeccion"
                        class="btn btn-outline-primary btn-sm btn-icon mg-r-5">
                        <i class="fa fa-eye"></i>
                    </button>
                </span>-->
                <span style="margin: 0px; padding:0px" data-toggle="tooltip" data-trigger="hover" data-placement="top"
                    title="" data-original-title="Descargar excel">
                    <a href="../actions/actions_admin/generar_excel_inspeccion.php?id_inspeccion=<?php echo $inspeccion["id"] ?>"
                        class="btn btn-outline-success  btn-sm btn-icon mg-r-5">

                        <i class="fa fa-file-excel-o"></i>
                    </a>
                </span>

            </td>

        </tr>
        <?php
        }
        ?>


    </tbody>
    <tfoot>
        <tr>
            <td colspan="5">
                <div class="ft-right">
                    <ul class="pagination"></ul>
                </div>
            </td>
        </tr>
    </tfoot>
</table>




<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>