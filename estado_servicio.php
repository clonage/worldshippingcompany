<?php 

include 'database/database.php';
session_start();

 $id_servicio = base64_decode($_GET["service"]);

$id_cargo = 2; //$_SESSION["cargo"];

if($id_cargo == 1){
$disabled_carga = 'disabled';
}else if($id_cargo == 2){
$disabled_transporte = 'disabled';
}

$consultar_servicios = $conn->prepare("SELECT * FROM servicios_control_rutas  WHERE id = '$id_servicio'");
$consultar_servicios->execute();
$consultar_servicios = $consultar_servicios->fetchAll(PDO::FETCH_ASSOC);

foreach($consultar_servicios as $servicios){
  $id_tipo_servicio = $servicios["tipo_servicio"];
  $estado_servicio = $servicios["estado"];
}

$consultar_clientes = $conn->prepare("SELECT * FROM clientes  WHERE estado = 1 ORDER BY id DESC");
$consultar_clientes->execute();
$consultar_clientes = $consultar_clientes->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_servicios = $conn->prepare("SELECT * FROM tipo_servicios WHERE estado = 1");
$consultar_tipo_servicios->execute();
$consultar_tipo_servicios = $consultar_tipo_servicios->fetchAll(PDO::FETCH_ASSOC);

$consultar_tipo_cargas = $conn->prepare("SELECT * FROM tipo_cargas WHERE estado = 1 ORDER BY id DESC");
$consultar_tipo_cargas->execute();
$consultar_tipo_cargas = $consultar_tipo_cargas->fetchAll(PDO::FETCH_ASSOC);

$consultar_conductores = $conn->prepare("SELECT * FROM conductores WHERE estado = 1 ORDER BY id DESC");
$consultar_conductores->execute();
$consultar_conductores = $consultar_conductores->fetchAll(PDO::FETCH_ASSOC);
?>


<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

<head>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="" />
    <!-- Page Title -->
    <title>World Shipping Company</title>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/flag-icon/flag-icon.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/ionicons/css/ionicons.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/footable/footable.core.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/toastr/toastr.min.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/chartist/chartist.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/apex-chart/apexcharts.css">
    <link type="text/css" rel="stylesheet" href="assets/css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/css/style.min.css" />
    <link type="text/css" rel="stylesheet" href="assets/plugins/datepicker/css/datepicker.min.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-select/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="assets/plugins/steps/jquery.steps.css">


    <!-- Favicon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

    <style>
    #responsive {
        overflow: hidden;
        white-space: initial;
    }
    </style>
</head>


<body>
    <!--================================-->
    <!-- Page Container Start -->
    <!--================================-->

    <style>
    #estado {
        position: fixed;
        right: 0;
        z-index: 100;
        background: white;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    .activity {
        margin: 9px -5px -18px 14px !important;
    }

    .activity i,
    .activity img {
        width: 31px !important;
        height: 31px !important;
        text-align: center;
        line-height: 31px !important;
        border-radius: 100%;
        position: absolute;
        left: -22px;
        color: #4d79f6;
        background-color: #f3f6f7;
        font-size: 20px;
        margin-top: -10px;
        -webkit-box-shadow: 0 0 0 0.5px #f3f6f7;
        box-shadow: 0 0 0 0.5px #f3f6f7;
    }
    </style>

    <div class="pd-t-4 pd-b-0 pd-x-20" id="estado">
        <div class="tx-10 tx-uppercase">
            <div class="activity">
                <?php if($estado_servicio == 0){?>
                <i class="icon-check bg-soft-primary"></i>
                <div class="time-item">
                    <div class="item-info ">
                        <div class="d-flex justify-content-between align-items-center">
                            <h6 class="tx-dark tx-13 mb-0">Recien creado</h6>
                        </div>
                    </div>
                </div>
                <?php }else if($estado_servicio == 1){?>
                <i class="icon-check bg-soft-warning"></i>
                <div class="time-item">
                    <div class="item-info ">
                        <div class="d-flex justify-content-between align-items-center">
                            <h6 class="tx-dark tx-13 mb-0">En proceso</h6>
                        </div>
                    </div>
                </div>
                <?php } else if($estado_servicio == 2){?>
                <i class="icon-check bg-soft-success"></i>
                <div class="time-item">
                    <div class="item-info ">
                        <div class="d-flex justify-content-between align-items-center">
                            <h6 class="tx-dark tx-13 mb-0">Completado</h6>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>

    <div class="page-container">



        <?php 
            if($id_tipo_servicio == 1 || $id_tipo_servicio == 2){
            ?>
        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
            </div>
            <div data-scrollbar-shown="true" data-scrollable="true" style=" overflow: hidden; overflow-y: auto;">
                <div class="card-body collapse show" id="collapse2">
                    <div id="wizard2_editar_servicios" class="wizard wizard-style-1 clearfix">
                        <hr>
                        <h5>Creación servicio</h5>
                        <hr>
                        <section>
                            <form id="form_editar_1">

                                <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">

                                <div class="row">

                                    <div class=" col-12 col-sm-6">
                                        <label for="">Cliente</label>
                                        <select class="form-control" onchange="consultar_subcliente()" name="id_cliente"
                                            id="id_cliente" <?php echo $disabled_transporte ?>>
                                            <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                <?php
                                    $id_cliente = $servicios["id_cliente"];
                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                            $consultar_cliente->execute();
                                            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                            foreach($consultar_cliente as $cliente){
                                                echo $cliente["razon_social"];
                                            }
                                            ?>
                                            </option>
                                            <?php 
                                foreach($consultar_clientes as $clientes){
                                ?>
                                            <option value="<?php echo $clientes["id"]?>">
                                                <?php echo $clientes["razon_social"]?>
                                            </option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Subcliente</label>

                                        <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                            disabled>
                                            <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                <?php
                                     $id_sub_cliente = $servicios["id_sub_cliente"];
                                     $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                     $consultar_sub_cliente->execute();
                                     $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                     foreach($consultar_sub_cliente as $sub_cliente){
                                         echo $sub_cliente["nombre_sub_cliente"];
                                     }
                                    ?>

                                            </option>
                                        </select>

                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Do/Ns</label>
                                        <input type="text" id="dons" name="dons" value="<?php echo $servicios["dons"]?>"
                                            class="form-control" disabled>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Tipo de servicio</label>
                                        <select class="form-control"
                                            oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                            name="tipo_servicio" id="tipo_servicios" disabled>
                                            <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                <?php 
                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                    $consultar_tipo_servicio->execute();
                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                    foreach($consultar_tipo_servicio as $tipo_servicio){
                                             echo $tipo_servicio["nombre_servicio"];
                                    }

                                ?>
                                            </option>
                                            <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                            <option value="<?php echo $tipo_servicios['id']?>">
                                                <?php echo $tipo_servicios['nombre_servicio']?>
                                            </option>
                                            <?php }?>
                                        </select>

                                    </div>
                                    <div class="col-12 col-sm-6" id="cantidad_pallets_despachos">
                                        <label for="">Cantidad pallets despachado</label>
                                        <input type="text" class="form-control" id="cantidad_pallets_despacho"
                                            value="<?=  $servicios["cantidad_pallets"] ?>" name="cantidad_pallets"
                                            <?php echo $disabled_carga ?>>

                                    </div>

                                    <div class="col-12 col-sm-6">
                                        <label for="">Tipo de carga</label>
                                        <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                            <option value="<?php echo $servicios["tipo_carga"]?>">
                                                <?php 
                                    $id_tipo_carga = $servicios["tipo_carga"];
                                $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                $consultar_tipo_carga->execute();
                                $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                foreach($consultar_tipo_carga as $tipo_carga){
                                    echo $tipo_carga["nombre_carga"];
                                }
                                ?>
                                            </option>
                                            <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                            <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                <?php echo $tipo_cargas['nombre_carga']?>
                                            </option>
                                            <?php }?>
                                        </select>

                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Número de contenedor</label>
                                        <input type="text" name="contenedor"
                                            value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                            class="form-control" disabled>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Tamaño contenedor</label>
                                        <input type="text" name="tamaño_contenedor"
                                            value="<?php echo $servicios["tamaño_contenedor"]?>" id="tamaño_contenedor"
                                            class="form-control" disabled>

                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Linea naviera</label>
                                        <input type="text" name="linea_naviera"
                                            value="<?php echo $servicios["linea_naviera"]?>" id="linea_naviera"
                                            class="form-control" disabled>

                                    </div>

                                    <div class="col-12 col-sm-6">
                                        <label for="">Puerto de retiro del vacío</label>
                                        <input type="text" value="<?php echo $servicios["puerto_origen"]?>"
                                            name="puerto_origen" id="puerto_origen" class="form-control" disabled>

                                    </div>
                                    <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                                        <label for="">Fecha retiro puerto del vacío</label>
                                        <input type="datetime-local" class="form-control " name="fecha_hora_r_p"
                                            id="fecha_hora_r_p"
                                            value="<?php $servicios['fecha_hora_r_p'] = preg_replace("/\s/",'T',$servicios['fecha_hora_r_p']); echo $servicios['fecha_hora_r_p']?>"
                                            disabled>
                                    </div>

                                    <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                                        <label for="">Fecha de entrega del vacío</label>
                                        <input type="date" class="form-control " name="fecha_lugar_entrega_v"
                                            value="<?= $servicios["fecha_lugar_entrega_v"] ?>" id="fecha_devolucion_v"
                                            disabled>
                                    </div>
                                    <div class="col-12 col-sm-6" id="lugar_descargue">
                                        <label for="">Lugar de entrega del vacío</label>
                                        <input type="text" name="lugar_entrega" id="lugar_descargue"
                                            class="form-control" value="<?= $servicios["lugar_entrega"] ?>" disabled>

                                    </div>
                                    <div id="fechas_bodegaje_traslado_zf_edit" class="col-12 col-sm-6"></div>

                                </div>
                            </form>
                            <!-- form-group -->
                        </section>
                        <hr>
                        <h5>Transporte</h5>
                        <hr>
                        <section>
                            <form id="form_2">
                                <input type="hidden" name="id_servicio_form2" class="form-control"
                                    value="<?php echo $id_servicio?>">

                                <div class="col-12 col-sm-12">
                                    <label for="">Número de placa del vehículo</label>
                                    <input type="text" class="form-control" id="placa_vehiculo_c"
                                        value="<?php echo $servicios["numero_placa_vehiculo"]?>"
                                        name="numero_placa_vehiculo" disabled>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <label for="">Nombre del conductor del vehículo</label>
                                    <input type="text" class="form-control" name="nombre_conductor"
                                        value="<?php echo $servicios["nombre_conductor"]?>" id="nombre_conductor"
                                        disabled>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <label for="">Identificación del conductor</label>
                                    <input type="text" class="form-control" name="identificacion_conductor"
                                        value="<?php echo $servicios["identificacion_conductor"]?>"
                                        id="identificacion_conductor" disabled>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <label for="">Ruta</label>
                                    <input type="text" class="form-control" name="ruta" id="ruta"
                                        value="<?php echo $servicios["ruta"]?>" disabled>
                                </div>

                                <div class="col-12 col-sm-12">
                                    <label for="">Fecha y hora de retiro del producto (origen)</label>
                                    <input type="datetime-local"
                                        value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_r_p_p_o']));?>"
                                        name="fecha_r_p_p_o" id="fecha_r_p_p_o" class="form-control "
                                        placeholder="Seleccionar una fecha" disabled>
                                </div>
                                <!--  <div class="col-12 col-sm-6">
                                <hr>
                                <a href="javascript:void(0)" onclick="guardar_form_2()"
                                    style="width: 40%;margin-top: -7px;"
                                    class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte?> ">
                                    <div class="ht-40 justify-content-between">
                                        <span class="pd-x-15">Actualizar datos</span>
                                        <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                    </div>
                                </a>

                            </div>-->
                            </form>
                            <!-- form-group -->

                        </section>
                        <hr>
                        <h5>Descargue mercancia</h5>
                        <hr>
                        <section>
                            <form id="form_3">
                                <div class="row">
                                    <input type="hidden" name="id_servicio_form3" class="form-control"
                                        value="<?php echo $id_servicio?>">
                                    <div class="col-12 col-sm-6">
                                        <label for="">Fecha y hora de inicio descargue</label>
                                        <input type="datetime-local" class="form-control" id="fecha_hora_descargue"
                                            name="fecha_hora_descargue" value="<?php 
                                if($servicios['fecha_hora_incio_descargue'] == null){

                                }else{
                                   echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_incio_descargue']));  
                                }
                               
                                
                                ?>" disabled>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Fecha y hora de terminación descargue</label>
                                        <input type="datetime-local" class="form-control" id="fecha_hora_t_descargue"
                                            name="fecha_hora_t_descargue" disabled value="<?php 
                                   if($servicios['fecha_hora_terminacion_descargue'] == null){
                                   }else{
                                echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_terminacion_descargue']));
                                   }
                                ?>">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Cantidad bultos</label>
                                        <input type="text" class="form-control" disabled
                                            value="<?php echo $servicios["cantidad_bultos"] ?>" id="cantidad_bultos"
                                            name="cantidad_bultos">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Cantidad pallets total</label>
                                        <input type="text" class="form-control" id="cantidad_bpallets" disabled
                                            value="<?php echo $servicios["cantidad_pallets"] ?>"
                                            name="cantidad_bpallets">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Aplica almacenamiento</label>
                                        <select name="aplica_almacenamiento" id="aplica_almacenamiento" disabled
                                            class="form-control" id="">
                                            <option value="<?php echo $servicios["aplica_almacenaje"]?>" selected>
                                                <?php 
                                if($servicios["aplica_almacenaje"] == 0){
                                    echo "No";
                                }else{
                                    echo "Si";
                                }
                                ?>

                                            </option>
                                            <option value="1">Sí</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Número de día de almacenaje</label>
                                        <input type="text" class="form-control" disabled
                                            value="<?php echo $servicios["numero_dia_almacenaje"]?>"
                                            name="numero_dia_almacenaje" id="numero_dia_almacenaje">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Días de almacenaje libre</label>
                                        <input type="text" class="form-control" placeholder="Días de almacenaje"
                                            disabled value="<?php echo $servicios["dia_almacenaje_libre"]?>"
                                            id="dia_almacenaje_libre" name="dia_almacenaje_libre">
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Cubicaje</label>
                                        <input type="text" value="<?php echo $servicios["cubicaje"]?>"
                                            class="form-control" name="cubicaje" id="cubicaje" disabled>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <label for="">Observaciones</label>
                                        <textarea name="observaciones_carga" id="observaciones_carga"
                                            class="form-control" <?php echo $disabled_carga ?>
                                            value="<?php echo $servicios["observaciones_carga"]?>"><?php echo $servicios["observaciones_carga"]?></textarea>

                                    </div>
                                    <!--<div class="col-12 col-sm-12">
                                    <label for="">Adjuntar una o varias fotos</label>
                                    <div class="input-group">

                                        <div class="custom-file">

                                            <input type="file" name="evidencias[]" class="form-control"
                                                id="inputGroupFile04" multiple>
                                            <label class="" for="inputGroupFile04"></label>
                                        </div>
                                        <br>
                                        <div class="input-group-append">
                                            <button class="btn btn-success" onclick="guardar_form_3()"
                                                type="button">Guardar
                                                datos y
                                                evidencias <i class="fa fa-upload"></i></button>
                                        </div>
                                    </div>
                                    <a href="admin/evidencias_servicio?id_servicios=<?php echo $id_servicio?>"
                                        target="_blank">Ver
                                        evidencias agregadas</a>
                                </div>-->

                                </div>
                            </form>
                        </section>
                        <hr>
                        <h5>Entrega mercancia</h5>
                        <hr>
                        <section>

                            <form id="form_4">
                                <input type="hidden" name="id_servicio_form4" id="id_servicio_form4"
                                    class="form-control" value="<?php echo $id_servicio?>">
                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div id="total_pallets"></div>

                                        <div id="despacho_parcial_totals_servicios"></div>
                                    </div>
                                </div>

                            </form>
                        </section>
                        <hr>
                        <h5>Devolución vacio</h5>
                        <hr>
                        <section>
                            <form id="form_5">
                                <input type="hidden" name="id_servicio_form5" id="id_servicio_form5"
                                    class="form-control" value="<?php echo $id_servicio?>">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <label for="">Lugar devolucion del vacío</label>
                                        <input type="text" class="form-control" name="lugar_devolucion_vacio"
                                            id="lugar_devolucion_vacio"
                                            value="<?php echo $servicios["lugar_devolucion_vacio"]?>" disabled>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label for="">Fecha de cita devolución del vacío</label>
                                        <input type="date" class="form-control " name="fecha_cita_devolucion_v"
                                            onchange="comparar_fechas_devolucion()" id="fecha_cita_devolucion_v" value="<?php 
                                  if($servicios['fecha_cita_devolucion_vacio'] == null){
                                }else{
                                echo  date('Y-m-d', strtotime($servicios['fecha_cita_devolucion_vacio']));
                                 }
                                ?>" disabled>
                                    </div>
                                    <div class="col-12 col-sm-4">
                                        <label for="">Nombre conductor (devolución del vacío)</label>
                                        <select name="nombre_condcutor_v" oninput="seleccionar_conductor()"
                                            id="nombre_condcutor_v" class="form-control" style="width:100% !important;"
                                            disabled>
                                            <option value=" <?php echo $servicios["id_conductor_d"]?>">
                                                <?php $id_conductor = $servicios["id_conductor_d"];
                                    $consultar_conductor = $conn->prepare("SELECT * FROM conductores WHERE id = '$id_conductor'");
                                    $consultar_conductor->execute();
                                    $consultar_conductor = $consultar_conductor->fetchAll(PDO::FETCH_ASSOC);

                                    foreach($consultar_conductor as $conductor){
                                        echo $conductor["nombres_conductor"].' '.$conductor["apellidos_conductor"];
                                    }
                                    ?></option>
                                            <?php foreach($consultar_conductores as $conductor ) { ?>
                                            <option value="<?php echo $conductor["id"]?>">
                                                <?php echo $conductor["nombres_conductor"]?></option>
                                            <?php  }?>
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-8" id="campos_conductor"></div>
                                    <br>
                                    <br>
                                    <h1 style="margin-top:50px;margin-bottom:50px;">
                                        <hr>
                                    </h1>
                                </div>
                            </form>
                        </section>
                    </div>
                </div>
            </div>
        </div>

        <?php 
    }else if($id_tipo_servicio == 3){  
    ?>

        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>
        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">

                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                    if($servicios['fecha_recepcion_doc'] == null){
                                        
                                        }else{
                                        echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                        }
                                    ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                                    $id_cliente = $servicios["id_cliente"];
                                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                    $consultar_cliente->execute();
                                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                                    foreach($consultar_cliente as $cliente){
                                                        echo $cliente["razon_social"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php 
                                                    foreach($consultar_clientes as $clientes){
                                                    ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                    $consultar_sub_cliente->execute();
                                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_sub_cliente as $sub_cliente){
                                                        echo $sub_cliente["nombre_sub_cliente"];
                                                    }
                                                    ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                    $consultar_tipo_servicio->execute();
                                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_servicio as $tipo_servicio){
                                                            echo $tipo_servicio["nombre_servicio"];
                                                    }

                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" style="display:none">
                                            <label for="">Tipo de carga</label>
                                            <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                                <option value="<?php echo $servicios["tipo_carga"]?>">
                                                    <?php 
                                                    $id_tipo_carga = $servicios["tipo_carga"];
                                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                    $consultar_tipo_carga->execute();
                                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_carga as $tipo_carga){
                                                        echo $tipo_carga["nombre_carga"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                                <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                    <?php echo $tipo_cargas['nombre_carga']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <label for="">Puerto de retiro del vacío</label>
                                            <input type="text" name="puerto_origen" id="puerto_origen"
                                                class="form-control" value="<?php echo $servicios["puerto_origen"]?>"
                                                disabled>

                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                                            <label for="">Fecha retiro puerto del vacío</label>
                                            <input type="datetime-local" class="form-control "
                                                value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p']));?>"
                                                name="fecha_hora_r_p" id="fecha_hora_r_p" disabled>
                                        </div>


                                        <?php if ($servicios["lugar_entrega"] != ""  && $servicios["fecha_lugar_entrega_v"] != "0000-00-00"){?>
                                        <div class="col-12 col-sm-6" id="lugar_descargue">
                                            <label for="">Lugar de entrega del vacío</label>
                                            <input type="text" name="lugar_entrega" id="lugar_descargue"
                                                value="<?php echo $servicios["lugar_entrega"]?>" class="form-control"
                                                disabled>

                                        </div>

                                        <div class="col-12 col-sm-6" id="fecha_devolucion_v">
                                            <label for="">Fecha de entrega del vacío</label>
                                            <input type="date" class="form-control"
                                                value="<?php echo $servicios["fecha_lugar_entrega_v"]?>"
                                                name="fecha_lugar_entrega_v" id="fecha_devolucion_v" disabled>
                                        </div>
                                        <?php  }?>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>

                                <?php if ($servicios["lugar_entrega"] == "" || $servicios["fecha_lugar_entrega_v"] == "0000-00-00"){?>

                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <form id="formulario_servicio_2">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="col-12 ">
                                                <label for="">Patio almacenaje</label>
                                                <input type="text" name="patio_almacenaje" id="patio_almacenaje"
                                                    class="form-control"
                                                    value="<?php echo $servicios["patio_almacenaje"]?>" disabled>
                                            </div>
                                            <div class="col-12 ">
                                                <label for="">Fecha de ingreso al patio de almacenaje</label>
                                                <input type="date" class="form-control"
                                                    name="fecha_ingreso_patio_almacenaje"
                                                    value="<?php echo $servicios["fecha_ingreso_patio_almacenaje"]?>"
                                                    disabled>
                                            </div>
                                            <div class="col-12 ">
                                                <label for="">Nombre conductor (transportadora)</label>
                                                <input type="text" class="form-control" name="nombre_c_p_a"
                                                    id="nombre_conductor_t"
                                                    value="<?php echo $servicios["nombre_c_p_a"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Identificación del conductor
                                                    (transportadora)</label>
                                                <input type="text" class="form-control" name="identificacion_c_p_a"
                                                    id="doc_conductor_t"
                                                    value="<?php echo $servicios["identificacion_c_p_a"]?>" disabled>
                                            </div>
                                            <div class="col-12 ">
                                                <label for="">Placa del vehículo (transportadora)</label>
                                                <input type="text" class="form-control" name="placa_v_p_a"
                                                    id="placa_v_conductor_t"
                                                    value="<?php echo $servicios["placa_v_p_a"]?>" disabled>
                                            </div>

                                            <div class="col-12 ">

                                                <a href="javascript:void(0)" onclick="actualizar_servicios(2)"
                                                    style="width: 100%; margin-top:15px;"
                                                    class="btn btn-success btn-with-icon btn-block  <?php echo $disabled_transporte?> ">
                                                    <div class="ht-40 justify-content-between">
                                                        <span class="pd-x-15">Guardar/actualizar</span>
                                                        <span class="icon wd-40"><i class="fa fa-refresh"></i></span>
                                                    </div>
                                                </a>

                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <form id="formulario_servicio_3">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="col-12">
                                                <label for="">Fecha de ingreso al patio del naviero</label>
                                                <input type="date" class="form-control"
                                                    name="fecha_ingreso_patio_naviero"
                                                    value="<?php echo $servicios["fecha_ingreso_patio_naviero"]?>">
                                            </div>
                                            <div class="col-12">
                                                <label for="">Lugar de entrega del vacío</label>
                                                <input type="text" name="lugar_entrega" id="lugar_descargue"
                                                    value="<?php echo $servicios["lugar_entrega"]?>"
                                                    class="form-control" disabled>

                                            </div>
                                            <div class="col-12">
                                                <label for="">Nombre conductor (transportadora)</label>
                                                <input type="text" class="form-control" name="nombre_c_e_v"
                                                    id="nombre_conductor_t"
                                                    value="<?php echo $servicios["nombre_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Identificación del conductor
                                                    (transportadora)</label>
                                                <input type="text" class="form-control" name="identificacion_c_e_v"
                                                    id="doc_conductor_t"
                                                    value="<?php echo $servicios["identificacion_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Placa del vehículo (transportadora)</label>
                                                <input type="text" class="form-control" name="placa_v_e_v"
                                                    id="placa_v_conductor_t"
                                                    value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                            </div>
                                        </form>
                                        <?php }else{?>
                                        <form id="formulario_servicio_2">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="col-12">
                                                <label for="">Nombre conductor (transportadora)</label>
                                                <input type="text" class="form-control" name="nombre_c_e_v"
                                                    id="nombre_conductor_t"
                                                    value="<?php echo $servicios["nombre_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Identificación del conductor
                                                    (transportadora)</label>
                                                <input type="text" class="form-control" name="identificacion_c_e_v"
                                                    id="doc_conductor_t"
                                                    value="<?php echo $servicios["identificacion_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Placa del vehículo (transportadora)</label>
                                                <input type="text" class="form-control" name="placa_v_e_v"
                                                    id="placa_v_conductor_t"
                                                    value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
        }else if($id_tipo_servicio == 4){
        ?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>

        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">

                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                    if($servicios['fecha_recepcion_doc'] == null){
                                        
                                        }else{
                                        echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                        }
                                    ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                        $id_cliente = $servicios["id_cliente"];
                                        $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                        $consultar_cliente->execute();
                                        $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                        foreach($consultar_cliente as $cliente){
                                            echo $cliente["razon_social"];
                                        }
                                        ?>
                                                </option>
                                                <?php 
                                            foreach($consultar_clientes as $clientes){
                                            ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                $id_sub_cliente = $servicios["id_sub_cliente"];
                                                $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                $consultar_sub_cliente->execute();
                                                $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_sub_cliente as $sub_cliente){
                                                    echo $sub_cliente["nombre_sub_cliente"];
                                                }
                                                ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                        $id_tipo_servicio = $servicios["tipo_servicio"];
                                                        $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                        $consultar_tipo_servicio->execute();
                                                        $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                        foreach($consultar_tipo_servicio as $tipo_servicio){
                                                                echo $tipo_servicio["nombre_servicio"];
                                                        }

                                                        ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" style="display:none">
                                            <label for="">Tipo de carga</label>
                                            <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                                <option value="<?php echo $servicios["tipo_carga"]?>">
                                                    <?php 
                                                        $id_tipo_carga = $servicios["tipo_carga"];
                                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                    $consultar_tipo_carga->execute();
                                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_carga as $tipo_carga){
                                                        echo $tipo_carga["nombre_carga"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                                <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                    <?php echo $tipo_cargas['nombre_carga']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>


                                        <div class="col-12 col-sm-6" id="patio_retiro">
                                            <label for="">Patio retiro del vacío</label>
                                            <input type="text" name="patio_retiro_vacio" id="patio_retiro_vacio"
                                                class="form-control" <?php echo $disabled_transporte?>
                                                value="<?php echo $servicios["patio_retiro_vacio"]?>" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_patio_retiro">
                                            <label for="">Fecha retiro patio del vacío</label>
                                            <input type="date" class="form-control " name="fecha_patio_retiro_vacio"
                                                id="fecha_patio_retiro_vacio"
                                                value="<?php echo $servicios["fecha_patio_retiro_vacio"]?>" disabled>
                                        </div>

                                        <?php if ($servicios["puerto_entrega_vacio"] != ""  && $servicios["fecha_puerto_entrega_vacio"] != "0000-00-00"){?>
                                        <div class="col-12 col-sm-6" id="puerto_entrega_vacios">
                                            <label for="">Puerto entrega del vacío</label>
                                            <input type="text" name="puerto_entrega_vacio" id="puerto_entrega_vacio"
                                                class="form-control" <?php echo $disabled_transporte?>
                                                value="<?php echo $servicios["puerto_entrega_vacio"]?>">
                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_entrega_vacios">
                                            <label for="">Fecha de entrega del del vacío</label>
                                            <input type="date" class="form-control " name="fecha_puerto_entrega_vacio"
                                                id="fecha_puerto_entrega_vacio" <?php echo $disabled_transporte?>
                                                value="<?php echo $servicios["fecha_puerto_entrega_vacio"]?>">
                                        </div>
                                        <?php  }?>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>

                                <?php if ($servicios["fecha_puerto_entrega_vacio"] == "0000-00-00"){?>

                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <form id="formulario_servicio_2">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="col-12 ">
                                                <label for="">Patio almacenaje</label>
                                                <input type="text" name="patio_almacenaje" id="patio_almacenaje"
                                                    class="form-control"
                                                    value="<?php echo $servicios["patio_almacenaje"]?>" disabled>
                                            </div>
                                            <div class="col-12 ">
                                                <label for="">Fecha de ingreso al patio de almacenaje</label>
                                                <input type="date" class="form-control"
                                                    name="fecha_ingreso_patio_almacenaje"
                                                    value="<?php echo $servicios["fecha_ingreso_patio_almacenaje"]?>"
                                                    disabled>
                                            </div>
                                            <div class="col-12 ">
                                                <label for="">Nombre conductor (transportadora)</label>
                                                <input type="text" class="form-control" name="nombre_c_p_a"
                                                    id="nombre_conductor_t"
                                                    value="<?php echo $servicios["nombre_c_p_a"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Identificación del conductor
                                                    (transportadora)</label>
                                                <input type="text" class="form-control" name="identificacion_c_p_a"
                                                    id="doc_conductor_t"
                                                    value="<?php echo $servicios["identificacion_c_p_a"]?>" disabled>
                                            </div>
                                            <div class="col-12 ">
                                                <label for="">Placa del vehículo (transportadora)</label>
                                                <input type="text" class="form-control" name="placa_v_p_a"
                                                    id="placa_v_conductor_t"
                                                    value="<?php echo $servicios["placa_v_p_a"]?>" disabled>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <form id="formulario_servicio_3">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="col-12">
                                                <label for="">Puerto de entrega del vacío</label>
                                                <input type="text" name="puerto_entrega_vacio" id="puerto_entrega_vacio"
                                                    class="form-control"
                                                    value="<?php echo $servicios["puerto_entrega_vacio"]?>"
                                                    <?php echo $disabled_transporte?> disabled>

                                            </div>
                                            <div class="col-12" id="fecha_hora_r_p">
                                                <label for="">Fecha de ingreso puerto</label>
                                                <input type="date" class="form-control "
                                                    value="<?php echo  $servicios['fecha_ingreso_puerto'];?>"
                                                    name="fecha_ingreso_puerto" id="fecha_ingreso_puerto"
                                                    <?php echo $disabled_transporte?> disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Nombre conductor (transportadora)</label>
                                                <input type="text" class="form-control" name="nombre_c_e_v"
                                                    id="nombre_conductor_t"
                                                    value="<?php echo $servicios["nombre_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Identificación del conductor
                                                    (transportadora)</label>
                                                <input type="text" class="form-control" name="identificacion_c_e_v"
                                                    id="doc_conductor_t"
                                                    value="<?php echo $servicios["identificacion_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Placa del vehículo (transportadora)</label>
                                                <input type="text" class="form-control" name="placa_v_e_v"
                                                    id="placa_v_conductor_t"
                                                    value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                            </div>
                                        </form>
                                        <?php }else{?>
                                        <form id="formulario_servicio_2">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="col-12">
                                                <label for="">Nombre conductor (transportadora)</label>
                                                <input type="text" class="form-control" name="nombre_c_e_v"
                                                    id="nombre_conductor_t"
                                                    value="<?php echo $servicios["nombre_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Identificación del conductor
                                                    (transportadora)</label>
                                                <input type="text" class="form-control" name="identificacion_c_e_v"
                                                    id="doc_conductor_t"
                                                    value="<?php echo $servicios["identificacion_c_e_v"]?>" disabled>
                                            </div>
                                            <div class="col-12">
                                                <label for="">Placa del vehículo (transportadora)</label>
                                                <input type="text" class="form-control" name="placa_v_e_v"
                                                    id="placa_v_conductor_t"
                                                    value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php }else if($id_tipo_servicio == 5){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>


        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">

                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                        if($servicios['fecha_recepcion_doc'] == null){
                                            
                                            }else{
                                            echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                            }
                                        ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                            $id_cliente = $servicios["id_cliente"];
                                            $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                            $consultar_cliente->execute();
                                            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                            foreach($consultar_cliente as $cliente){
                                                echo $cliente["razon_social"];
                                            }
                                            ?>
                                                </option>
                                                <?php 
                                            foreach($consultar_clientes as $clientes){
                                                    ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                    $consultar_sub_cliente->execute();
                                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_sub_cliente as $sub_cliente){
                                                        echo $sub_cliente["nombre_sub_cliente"];
                                                    }
                                                    ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                    $consultar_tipo_servicio->execute();
                                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_servicio as $tipo_servicio){
                                                            echo $tipo_servicio["nombre_servicio"];
                                                    }

                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" style="display:none">
                                            <label for="">Tipo de carga</label>
                                            <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                                <option value="<?php echo $servicios["tipo_carga"]?>">
                                                    <?php 
                                                    $id_tipo_carga = $servicios["tipo_carga"];
                                                    $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                    $consultar_tipo_carga->execute();
                                                    $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_carga as $tipo_carga){
                                                        echo $tipo_carga["nombre_carga"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                                <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                    <?php echo $tipo_cargas['nombre_carga']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="tamaño_contenedor_1">
                                            <label for="">Tamaño contenedor</label>
                                            <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                                                disabled>
                                                <option value="<?php echo $servicios["tamaño_contenedor"]?>">
                                                    <?php echo $servicios["tamaño_contenedor"]?></option>
                                                <option value="20">20</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>


                                        <div class="col-12 col-sm-6" id="puerto_origen">
                                            <label for="">Puerto de retiro del vacío</label>
                                            <input type="text" name="puerto_origen" id="puerto_origen"
                                                class="form-control" value="<?php echo $servicios["puerto_origen"]?>"
                                                disabled>

                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                                            <label for="">Fecha retiro puerto del vacío</label>
                                            <input type="datetime-local" class="form-control "
                                                value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p']));?>"
                                                name="fecha_hora_r_p" id="fecha_hora_r_p" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="lugar_entrega_zona_f_1">
                                            <label for="">Lugar de entrega zona franca</label>
                                            <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f"
                                                class="form-control"
                                                value="<?php echo $servicios["lugar_entrega_zona_f"]?>" disabled>

                                        </div>

                                        <div class="col-12 col-sm-6" id="fecha_entrega_zona_f_1">
                                            <label for="">Fecha de entrega en zona franca</label>
                                            <input type="date" class="form-control " name="fecha_entrega_zona_f"
                                                id="fecha_entrega_zona_f"
                                                value="<?php echo $servicios["fecha_entrega_zona_f"]?>" disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>
                                <form id="formulario_servicio_2">
                                    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                    <div class="col-12">
                                        <label for="">Nombre conductor (transportadora)</label>
                                        <input type="text" class="form-control" name="nombre_c_z_f" id="nombre_c_z_f"
                                            value="<?php echo $servicios["nombre_c_z_f"]?>" disabled>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Identificación del conductor (transportadora)</label>
                                        <input type="text" class="form-control" name="identificacion_c_z_f"
                                            id="doc_conductor_t" value="<?php echo $servicios["identificacion_c_z_f"]?>"
                                            disabled>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Placa del vehículo (transportadora)</label>
                                        <input type="text" class="form-control" name="placa_v_z_f"
                                            id="placa_v_conductor_t" value="<?php echo $servicios["placa_v_z_f"]?>"
                                            disabled>
                                    </div>

                                </form>
                                <hr>

                                <div class="col-12 col-sm-12">
                                    <?php if ($servicios["patio_almacenaje"] == "" || $servicios["lugar_devolucion_vacio"] == "" ){?>

                                    <div id="opcion">
                                        <label for="">¿Se generará almacenamiento del vacío?</label>
                                        <select name="almacenamiento_vacio" onchange="mostrar_opciones_form_5()"
                                            class="form-control" id="almacenamiento_vacio">
                                            <option value="3">Seleccione una opción</option>
                                            <option value="1">Sí</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <div id="mensaje_almacenamiento"></div>

                                    <?php }?>
                                    <div id="mostrar_opcion_no" style="display:none">

                                        <form id="formulario_servicio_53">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="row">
                                                <div class="col-12 ">
                                                    <label for="">Lugar devolucion del vacío </label>
                                                    <input type="text" class="form-control"
                                                        value="<?php echo $servicios["lugar_devolucion_vacio"]?>"
                                                        name="lugar_devolucion_vacio" id="lugar_devolucion_v" disabled>
                                                </div>
                                                <div class="col-12 ">
                                                    <label for="">Fecha devolución del vacío</label>
                                                    <input type="date" class="form-control " name="fecha_devolucion"
                                                        onchange="comparar_fechas_devolucion()" id="fecha_devolucion"
                                                        value="<?php 
                                                    if($servicios['fecha_devolucion'] == null){
                                                    }else{
                                                    echo  date('Y-m-d', strtotime($servicios['fecha_devolucion']));
                                                    }
                                                    ?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Nombre conductor (transportadora)</label>
                                                    <input type="text" class="form-control" name="nombre_c_e_v"
                                                        id="nombre_conductor_t"
                                                        value="<?php echo $servicios["nombre_c_e_v"]?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Identificación del conductor
                                                        (transportadora)</label>
                                                    <input type="text" class="form-control" name="identificacion_c_e_v"
                                                        id="doc_conductor_t"
                                                        value="<?php echo $servicios["identificacion_c_e_v"]?>"
                                                        disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Placa del vehículo (transportadora)</label>
                                                    <input type="text" class="form-control" name="placa_v_e_v"
                                                        id="placa_v_conductor_t"
                                                        value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">¿Se realizó descarga y carga con montacargas
                                                        de?
                                                    </label>
                                                    <select name="montacarga" id="montacarga" class="form-control">
                                                        <?= $montacarga = $servicios["montacarga"];
                                                 if($montacarga == ""){ 
                                               echo '<option value="">Seleccione una opción</option>';
                                               }else{
                                                echo '<option value="'.$servicios["montacarga"].'">'.$servicios["montacarga"].'</option>';
                                                }
                                                 ?>
                                                        <option value="7 toneladas">7 toneladas</option>
                                                        <option value="8 toneladas">8 toneladas</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                    <div id="mostrar_opcion_si" style="display:none">
                                        <form id="formulario_servicio_54">
                                            <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                            <div class="row">
                                                <div class="col-12 ">
                                                    <label for="">Patio almacenaje</label>
                                                    <input type="text" name="patio_almacenaje" id="patio_almacenaje"
                                                        class="form-control"
                                                        value="<?php echo $servicios["patio_almacenaje"]?>" disabled>
                                                </div>
                                                <div class="col-12 ">
                                                    <label for="">Fecha de ingreso al patio de
                                                        almacenaje</label>
                                                    <input type="date" class="form-control"
                                                        name="fecha_ingreso_patio_almacenaje"
                                                        value="<?php echo $servicios["fecha_ingreso_patio_almacenaje"]?>"
                                                        disabled>
                                                </div>
                                                <div class="col-12 ">
                                                    <label for="">Nombre conductor (transportadora)</label>
                                                    <input type="text" class="form-control" name="nombre_c_p_a"
                                                        id="nombre_conductor_t"
                                                        value="<?php echo $servicios["nombre_c_p_a"]?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Identificación del conductor
                                                        (transportadora)</label>
                                                    <input type="text" class="form-control" name="identificacion_c_p_a"
                                                        id="doc_conductor_t"
                                                        value="<?php echo $servicios["identificacion_c_p_a"]?>"
                                                        disabled>
                                                </div>
                                                <div class="col-12 ">
                                                    <label for="">Placa del vehículo (transportadora)</label>
                                                    <input type="text" class="form-control" name="placa_v_p_a"
                                                        id="placa_v_conductor_t"
                                                        value="<?php echo $servicios["placa_v_p_a"]?>" disabled>
                                                </div>
                                                <hr>
                                                <div class="col-12 ">
                                                    <label for="">Lugar entrega del vacío (naviero) </label>
                                                    <input type="text" class="form-control"
                                                        value="<?php echo $servicios["lugar_entrega"]?>"
                                                        name="lugar_entrega" id="lugar_entrega" disabled>
                                                </div>
                                                <div class="col-12 ">
                                                    <label for="">Fecha de ingreso patio del naviero</label>
                                                    <input type="date" class="form-control "
                                                        name="fecha_ingreso_patio_naviero"
                                                        onchange="comparar_fechas_devolucion()"
                                                        id="fecha_ingreso_patio_naviero" value="<?php 
                                                    if($servicios['fecha_ingreso_patio_naviero'] == null){
                                                    }else{
                                                    echo  date('Y-m-d', strtotime($servicios['fecha_ingreso_patio_naviero']));
                                                    }
                                                    ?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Nombre conductor (transportadora)</label>
                                                    <input type="text" class="form-control" name="nombre_c_e_v"
                                                        id="nombre_conductor_t"
                                                        value="<?php echo $servicios["nombre_c_e_v"]?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Identificación del conductor
                                                        (transportadora)</label>
                                                    <input type="text" class="form-control" name="identificacion_c_e_v"
                                                        id="doc_conductor_t"
                                                        value="<?php echo $servicios["identificacion_c_e_v"]?>"
                                                        disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Placa del vehículo (transportadora)</label>
                                                    <input type="text" class="form-control" name="placa_v_e_v"
                                                        id="placa_v_conductor_t"
                                                        value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">¿Se realizó descarga y carga con montacargas
                                                        de?
                                                    </label>
                                                    <select name="montacarga" id="montacarga" class="form-control"
                                                        disabled>
                                                        <?= $montacarga = $servicios["montacarga"];
                                          if($montacarga == ""){ 
                                               echo '<option value="">Seleccione una opción</option>';
                                               }else{
                                                echo '<option value="'.$servicios["montacarga"].'">'.$servicios["montacarga"].'</option>';
                                                }
                                                 ?>
                                                        <option value="7 toneladas">7 toneladas</option>
                                                        <option value="8 toneladas">8 toneladas</option>
                                                    </select>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php }else if($id_tipo_servicio == 6){?>

        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>
        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">

                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                    if($servicios['fecha_recepcion_doc'] == null){
                                        
                                        }else{
                                        echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                        }
                                    ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                            $id_cliente = $servicios["id_cliente"];
                                            $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                            $consultar_cliente->execute();
                                            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                            foreach($consultar_cliente as $cliente){
                                                echo $cliente["razon_social"];
                                            }
                                            ?>
                                                </option>
                                                <?php 
                                                    foreach($consultar_clientes as $clientes){
                                                    ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                    $consultar_sub_cliente->execute();
                                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_sub_cliente as $sub_cliente){
                                                        echo $sub_cliente["nombre_sub_cliente"];
                                                    }
                                                    ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                    $consultar_tipo_servicio->execute();
                                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_servicio as $tipo_servicio){
                                                            echo $tipo_servicio["nombre_servicio"];
                                                    }

                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" id="cantidad_pallets_despacho">
                                            <label for="">Cantidad pallets</label>
                                            <input type="text" class="form-control" id="cantidad_pallets"
                                                value="<?= $servicios["cantidad_pallets"]?>" name="cantidad_pallets"
                                                <?php echo $disabled_carga ?>>

                                        </div>
                                        <div class="col-12 col-sm-6" id="puerto_origen">
                                            <label for="">Puerto de retiro del vacío</label>
                                            <input type="text" name="puerto_origen" id="puerto_origen"
                                                class="form-control" value="<?php echo $servicios["puerto_origen"]?>"
                                                disabled>

                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                                            <label for="">Fecha retiro puerto del vacío</label>
                                            <input type="datetime-local" class="form-control "
                                                value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p']));?>"
                                                name="fecha_hora_r_p" id="fecha_hora_r_p" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6">
                                            <label for="">Lugar entrega</label>
                                            <input type="text" class="form-control"
                                                value="<?php echo $servicios["lugar_entrega"]?>" name="lugar_entrega"
                                                id="lugar_entrega" disabled>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>
                                <form id="formulario_servicio_2">
                                    <input type="hidden" name="id_servicio" value="<?php echo $id_servicio?>">
                                    <div class="col-12">
                                        <label for="">Nombre conductor (transportadora)</label>
                                        <input type="text" class="form-control" name="nombre_c_z_f" id="nombre_c_z_f"
                                            value="<?php echo $servicios["nombre_c_z_f"]?>" disabled>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Identificación del conductor (transportadora)</label>
                                        <input type="text" class="form-control" name="identificacion_c_z_f"
                                            id="doc_conductor_t" value="<?php echo $servicios["identificacion_c_z_f"]?>"
                                            disabled>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Placa del vehículo (transportadora)</label>
                                        <input type="text" class="form-control" name="placa_v_z_f"
                                            id="placa_v_conductor_t" value="<?php echo $servicios["placa_v_z_f"]?>"
                                            disabled>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php }else if($id_tipo_servicio == 7){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>


        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">
                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                        if($servicios['fecha_recepcion_doc'] == null){
                                            
                                            }else{
                                            echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                            }
                                        ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                        $id_cliente = $servicios["id_cliente"];
                                        $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                        $consultar_cliente->execute();
                                        $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                        foreach($consultar_cliente as $cliente){
                                            echo $cliente["razon_social"];
                                        }
                                        ?>
                                                </option>
                                                <?php 
                                        foreach($consultar_clientes as $clientes){
                                        ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                            $id_sub_cliente = $servicios["id_sub_cliente"];
                                            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                            $consultar_sub_cliente->execute();
                                            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($consultar_sub_cliente as $sub_cliente){
                                                echo $sub_cliente["nombre_sub_cliente"];
                                            }
                                            ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                $id_tipo_servicio = $servicios["tipo_servicio"];
                                                $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                $consultar_tipo_servicio->execute();
                                                $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_servicio as $tipo_servicio){
                                                        echo $tipo_servicio["nombre_servicio"];
                                                }

                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" id="puerto_origen">
                                            <label for="">Puerto de retiro del vacío</label>
                                            <input type="text" name="puerto_origen" id="puerto_origen"
                                                class="form-control" value="<?php echo $servicios["puerto_origen"]?>"
                                                disabled>

                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                                            <label for="">Fecha retiro puerto del vacío</label>
                                            <input type="datetime-local" class="form-control "
                                                value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p']));?>"
                                                name="fecha_hora_r_p" id="fecha_hora_r_p" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="peso_retiros">
                                            <label for="">Peso retirado</label>
                                            <input type="text" name="peso_retiro" id="peso_retiro" class="form-control"
                                                value="<?php echo $servicios["peso_retiro"]?>" disabled>

                                        </div>
                                        <div class="col-12 col-sm-6" id="tipo_vehiculo">
                                            <label for="">Seleccionar vehículo acarreo</label>
                                            <select name="vehiculo_acarreo" id="vehiculo_acarreo" class="form-control">
                                                <option value="<?= $servicios["vehiculo_acarreo"]?><">
                                                    <?= $servicios["vehiculo_acarreo"]?>
                                                </option>
                                                <option value="Furgón">Furgón</option>
                                                <option value="Patineta">Patineta</option>
                                                <option value="Mula">Mula</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6" id="lugar_ingresos">
                                            <label for="">Lugar ingreso</label>
                                            <input type="text" name="lugar_ingreso" id="lugar_ingreso"
                                                class="form-control" value="<?= $servicios["lugar_ingreso"]?>" disabled>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>
                                <form id="form_4">
                                    <input type="hidden" name="id_servicio_form4" id="id_servicio_form4"
                                        class="form-control" value="<?php echo $id_servicio?>">
                                    <div class="row">

                                        <div class="col-12 col-sm-12">
                                            <div id="total_pallets"></div>

                                            <div id="despacho_parcial_totals_servicios"></div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php }else if($id_tipo_servicio == 8){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>


        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">

                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                        if($servicios['fecha_recepcion_doc'] == null){
                                            
                                            }else{
                                            echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                            }
                                        ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                            $id_cliente = $servicios["id_cliente"];
                                            $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                            $consultar_cliente->execute();
                                            $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                            foreach($consultar_cliente as $cliente){
                                                echo $cliente["razon_social"];
                                            }
                                            ?>
                                                </option>
                                                <?php 
                                    foreach($consultar_clientes as $clientes){
                                    ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                $id_sub_cliente = $servicios["id_sub_cliente"];
                                $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                $consultar_sub_cliente->execute();
                                $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                foreach($consultar_sub_cliente as $sub_cliente){
                                    echo $sub_cliente["nombre_sub_cliente"];
                                }
                                ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                $id_tipo_servicio = $servicios["tipo_servicio"];
                                                $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                $consultar_tipo_servicio->execute();
                                                $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_servicio as $tipo_servicio){
                                                        echo $tipo_servicio["nombre_servicio"];
                                                }

                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" style="display:none">
                                            <label for="">Tipo de carga</label>
                                            <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                                <option value="<?php echo $servicios["tipo_carga"]?>">
                                                    <?php 
                                            $id_tipo_carga = $servicios["tipo_carga"];
                                            $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                            $consultar_tipo_carga->execute();
                                            $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($consultar_tipo_carga as $tipo_carga){
                                                echo $tipo_carga["nombre_carga"];
                                            }
                                            ?>
                                                </option>
                                                <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                                <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                    <?php echo $tipo_cargas['nombre_carga']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="tamaño_contenedor_1">
                                            <label for="">Tamaño contenedor</label>
                                            <select name="tamaño_contenedor" id="tamaño_contenedor" class="form-control"
                                                disabled>
                                                <option value="<?php echo $servicios["tamaño_contenedor"]?>">
                                                    <?php echo $servicios["tamaño_contenedor"]?></option>
                                                <option value="20">20</option>
                                                <option value="40">40</option>
                                            </select>
                                        </div>


                                        <div class="col-12 col-sm-6" id="puerto_origen">
                                            <label for="">Puerto de retiro del vacío</label>
                                            <input type="text" name="puerto_origen" id="puerto_origen"
                                                class="form-control" value="<?php echo $servicios["puerto_origen"]?>"
                                                disabled>

                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_hora_r_p">
                                            <label for="">Fecha retiro puerto del vacío</label>
                                            <input type="datetime-local" class="form-control "
                                                value="<?php echo  date('Y-m-d\TH:i:s', strtotime($servicios['fecha_hora_r_p']));?>"
                                                name="fecha_hora_r_p" id="fecha_hora_r_p" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="lugar_entrega_zona_f_1">
                                            <label for="">Lugar de entrega zona franca</label>
                                            <input type="text" name="lugar_entrega_zona_f" id="lugar_entrega_zona_f"
                                                class="form-control"
                                                value="<?php echo $servicios["lugar_entrega_zona_f"]?>" disabled>

                                        </div>

                                        <div class="col-12 col-sm-6" id="fecha_entrega_zona_f_1">
                                            <label for="">Fecha de entrega en zona franca</label>
                                            <input type="date" class="form-control " name="fecha_entrega_zona_f"
                                                id="fecha_entrega_zona_f"
                                                value="<?php echo $servicios["fecha_entrega_zona_f"]?>" disabled>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>
                                <div class="col-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-12 col-sm-12">
                                            <form id="formulario_servicio_2">
                                                <input type="hidden" name="id_servicio"
                                                    value="<?php echo $id_servicio?>">
                                                <div class="col-12">
                                                    <label for="">Nombre conductor (transportadora)</label>
                                                    <input type="text" class="form-control" name="nombre_c_z_f"
                                                        id="nombre_c_z_f"
                                                        value="<?php echo $servicios["nombre_c_z_f"]?>" disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Identificación del conductor
                                                        (transportadora)</label>
                                                    <input type="text" class="form-control" name="identificacion_c_z_f"
                                                        id="doc_conductor_t"
                                                        value="<?php echo $servicios["identificacion_c_z_f"]?>"
                                                        disabled>
                                                </div>
                                                <div class="col-12">
                                                    <label for="">Placa del vehículo (transportadora)</label>
                                                    <input type="text" class="form-control" name="placa_v_z_f"
                                                        id="placa_v_conductor_t"
                                                        value="<?php echo $servicios["placa_v_z_f"]?>" disabled>
                                                </div>

                                            </form>
                                        </div>
                                        <div class="col-12 col-sm-12">
                                            <?php if ($servicios["patio_almacenaje"] == "" || $servicios["lugar_devolucion_vacio"] == "" ){?>

                                            <div id="opcion">
                                                <label for="">¿Se generará almacenamiento del vacío?</label>
                                                <select name="almacenamiento_vacio" onchange="mostrar_opciones_form_5()"
                                                    class="form-control" id="almacenamiento_vacio">
                                                    <option value="3">Seleccione una opción</option>
                                                    <option value="1">Sí</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                            <div id="mensaje_almacenamiento"></div>

                                            <?php }?>
                                            <div id="mostrar_opcion_no" style="display:none">

                                                <form id="formulario_servicio_53">
                                                    <input type="hidden" name="id_servicio"
                                                        value="<?php echo $id_servicio?>">
                                                    <div class="row">
                                                        <div class="col-12 ">
                                                            <label for="">Lugar devolucion del vacío </label>
                                                            <input type="text" class="form-control"
                                                                value="<?php echo $servicios["lugar_devolucion_vacio"]?>"
                                                                name="lugar_devolucion_vacio" id="lugar_devolucion_v"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12 ">
                                                            <label for="">Fecha devolución del vacío</label>
                                                            <input type="date" class="form-control "
                                                                name="fecha_devolucion"
                                                                onchange="comparar_fechas_devolucion()"
                                                                id="fecha_devolucion" value="<?php 
                                                        if($servicios['fecha_devolucion'] == null){
                                                        }else{
                                                        echo  date('Y-m-d', strtotime($servicios['fecha_devolucion']));
                                                        }
                                                        ?>" disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Nombre conductor
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control" name="nombre_c_e_v"
                                                                id="nombre_conductor_t"
                                                                value="<?php echo $servicios["nombre_c_e_v"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Identificación del conductor
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control"
                                                                name="identificacion_c_e_v" id="doc_conductor_t"
                                                                value="<?php echo $servicios["identificacion_c_e_v"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Placa del vehículo
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control" name="placa_v_e_v"
                                                                id="placa_v_conductor_t"
                                                                value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">¿Se realizó descarga y carga con
                                                                montacargas
                                                                de? </label>
                                                            <select name="montacarga" id="montacarga"
                                                                class="form-control">
                                                                <?= $montacarga = $servicios["montacarga"];
                                                            if($montacarga == ""){ 
                                                                echo '<option value="">Seleccione una opción</option>';
                                                                }else{
                                                                    echo '<option value="'.$servicios["montacarga"].'">'.$servicios["montacarga"].'</option>';
                                                                    }
                                                                    ?>
                                                                <option value="7 toneladas">7 toneladas</option>
                                                                <option value="8 toneladas">8 toneladas</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>
                                            <div id="mostrar_opcion_si" style="display:none">
                                                <form id="formulario_servicio_54">
                                                    <input type="hidden" name="id_servicio"
                                                        value="<?php echo $id_servicio?>">
                                                    <div class="row">
                                                        <div class="col-12 ">
                                                            <label for="">Patio almacenaje</label>
                                                            <input type="text" name="patio_almacenaje"
                                                                id="patio_almacenaje" class="form-control"
                                                                value="<?php echo $servicios["patio_almacenaje"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12 ">
                                                            <label for="">Fecha de ingreso al patio de
                                                                almacenaje</label>
                                                            <input type="date" class="form-control"
                                                                name="fecha_ingreso_patio_almacenaje"
                                                                value="<?php echo $servicios["fecha_ingreso_patio_almacenaje"]?>">
                                                        </div>
                                                        <div class="col-12 ">
                                                            <label for="">Nombre conductor
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control" name="nombre_c_p_a"
                                                                id="nombre_conductor_t"
                                                                value="<?php echo $servicios["nombre_c_p_a"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Identificación del conductor
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control"
                                                                name="identificacion_c_p_a" id="doc_conductor_t"
                                                                value="<?php echo $servicios["identificacion_c_p_a"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12 ">
                                                            <label for="">Placa del vehículo
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control" name="placa_v_p_a"
                                                                id="placa_v_conductor_t"
                                                                value="<?php echo $servicios["placa_v_p_a"]?>" disabled>
                                                        </div>
                                                        <hr>
                                                        <div class="col-12 ">
                                                            <label for="">Lugar entrega del vacío (naviero)
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                value="<?php echo $servicios["lugar_entrega"]?>"
                                                                name="lugar_entrega" id="lugar_entrega" disabled>
                                                        </div>
                                                        <div class="col-12 ">
                                                            <label for="">Fecha de ingreso al patio del
                                                                naviero</label>
                                                            <input type="date" class="form-control "
                                                                name="fecha_ingreso_patio_naviero"
                                                                onchange="comparar_fechas_devolucion()"
                                                                id="fecha_ingreso_patio_naviero" value="<?php 
                                                            if($servicios['fecha_ingreso_patio_naviero'] == null){
                                                            }else{
                                                            echo  date('Y-m-d', strtotime($servicios['fecha_ingreso_patio_naviero']));
                                                            }
                                                            ?>" disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Nombre conductor
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control" name="nombre_c_e_v"
                                                                id="nombre_conductor_t"
                                                                value="<?php echo $servicios["nombre_c_e_v"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Identificación del conductor
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control"
                                                                name="identificacion_c_e_v" id="doc_conductor_t"
                                                                value="<?php echo $servicios["identificacion_c_e_v"]?>"
                                                                disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">Placa del vehículo
                                                                (transportadora)</label>
                                                            <input type="text" class="form-control" name="placa_v_e_v"
                                                                id="placa_v_conductor_t"
                                                                value="<?php echo $servicios["placa_v_e_v"]?>" disabled>
                                                        </div>
                                                        <div class="col-12">
                                                            <label for="">¿Se realizó descarga y carga con
                                                                montacargas
                                                                de? </label>
                                                            <select name="montacarga" id="montacarga"
                                                                class="form-control">
                                                                <?= $montacarga = $servicios["montacarga"];
                                                            if($montacarga == ""){ 
                                                                echo '<option value="">Seleccione una opción</option>';
                                                                }else{
                                                                    echo '<option value="'.$servicios["montacarga"].'">'.$servicios["montacarga"].'</option>';
                                                                    }
                                                                    ?>
                                                                <option value="7 toneladas">7 toneladas</option>
                                                                <option value="8 toneladas">8 toneladas</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php }else if($id_tipo_servicio == 9){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>

        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información general del servicio</h2>
                                <form id="form_editar_1">

                                    <input type="hidden" name="id_servicio" value="<?php  echo $id_servicio?>">

                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                            if($servicios['fecha_recepcion_doc'] == null){
                                                
                                                }else{
                                                echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                                }
                                            ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                                    $id_cliente = $servicios["id_cliente"];
                                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                    $consultar_cliente->execute();
                                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                                    foreach($consultar_cliente as $cliente){
                                                        echo $cliente["razon_social"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php 
                                                    foreach($consultar_clientes as $clientes){
                                                    ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_clientes"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                $id_sub_cliente = $servicios["id_sub_cliente"];
                                                $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                $consultar_sub_cliente->execute();
                                                $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_sub_cliente as $sub_cliente){
                                                    echo $sub_cliente["nombre_sub_cliente"];
                                                }
                                                ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                    $id_tipo_servicio = $servicios["tipo_servicio"];
                                                    $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                    $consultar_tipo_servicio->execute();
                                                    $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_tipo_servicio as $tipo_servicio){
                                                            echo $tipo_servicio["nombre_servicio"];
                                                    }

                                                    ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" style="display:none">
                                            <label for="">Tipo de carga</label>
                                            <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                                <option value="<?php echo $servicios["tipo_carga"]?>">
                                                    <?php 
                                                $id_tipo_carga = $servicios["tipo_carga"];
                                                $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                $consultar_tipo_carga->execute();
                                                $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_carga as $tipo_carga){
                                                    echo $tipo_carga["nombre_carga"];
                                                }
                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                                <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                    <?php echo $tipo_cargas['nombre_carga']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="nombre_bodegas">
                                            <label for="">Nombre bodega</label>
                                            <input type="text" name="nombre_bodega" id="nombre_bodega"
                                                class="form-control" value="<?= $servicios["nombre_bodega"]?>">
                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_vaciados">
                                            <label for="">Fecha de vaciado</label>
                                            <input type="date" name="fecha_vaciado" id="fecha_vaciado"
                                                class="form-control" value="<?= $servicios["fecha_vaciado"]?>">
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Días de almacenaje libre</label>
                                            <input type="text" class="form-control" placeholder="Días de almacenaje"
                                                disabled value="<?php echo $servicios["dia_almacenaje_libre"]?>"
                                                id="dia_almacenaje_libre" name="dia_almacenaje_libre">
                                            <h6 style="font-size:10px"><b>Dejar el campo vacío si no tiene días
                                                    de
                                                    almacenaje
                                                    libre*</b>
                                            </h6>
                                        </div>
                                        <div class="col-12 col-sm-6" id="tipo_despachos">
                                            <label for="">Tipo de despacho</label>
                                            <select name="tipo_despacho" id="tipo_despacho" onchange="tipos_despachos()"
                                                class="form-control" disabled>
                                                <option value="<?= $servicios["tipo_despacho"] ?>" selected>
                                                    <?php
                                        if($servicios["tipo_despacho"] == 1){
                                          echo 'Metros cuadros almacenados'; 
                                        }else if($servicios["tipo_despacho"] == 2){
                                          echo 'Tolenadas almacenados'; 
                                        }else if($servicios["tipo_despacho"] == 3){
                                            echo 'Número de pallets'; 
                                        } 
                                        ?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6" id="cantidad_despachos">
                                            <label for=""><?php  if($servicios["tipo_despacho"] == 1){
                                          echo 'Metros cuadros almacenados'; 
                                        }else if($servicios["tipo_despacho"] == 2){
                                          echo 'Tolenadas almacenados'; 
                                        }else if($servicios["tipo_despacho"] == 3){
                                            echo 'Número de pallets'; 
                                        } 
                                        ?></label>
                                            <input type="text" name="cantidad_despacho"
                                                value="<?= $servicios["cantidad_despacho"]?>" id="cantidad_despacho"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Horas de montacarga</label>
                                            <input type="text" class="form-control" placeholder="" disabled
                                                value="<?php echo $servicios["hora_montacarga"]?>" id="hora_montacarga"
                                                name="hora_montacarga">
                                        </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="baseline-list baseline-border baseline-primary">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Más detalles</h2>
                                <form id="form_4">
                                    <input type="hidden" name="id_servicio_form4" id="id_servicio_form4"
                                        class="form-control" value="<?php echo $id_servicio?>">
                                    <div class="row">

                                        <div class="col-12 col-sm-12">
                                            <center>
                                                <h6 style="font-size:10px"><b><?php  if($servicios["tipo_despacho"] == 1){
                                          echo 'Metros cuadros almacenados'; 
                                        }else if($servicios["tipo_despacho"] == 2){
                                          echo 'Tolenadas almacenados'; 
                                        }else if($servicios["tipo_despacho"] == 3){
                                            echo 'Número de pallets'; 
                                        } 
                                        ?></b></h6>
                                            </center>
                                            <hr>
                                            <div id="total_pallets"></div>
                                            <div id="despacho_parcial_totals_servicios"></div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }else if($id_tipo_servicio == 10 || $id_tipo_servicio == 11){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>

        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información del servicio</h2>
                                <form>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                        if($servicios['fecha_recepcion_doc'] == null){
                                            
                                            }else{
                                            echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                            }
                                        ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                                $id_cliente = $servicios["id_cliente"];
                                                $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                $consultar_cliente->execute();
                                                $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                                foreach($consultar_cliente as $cliente){
                                                    echo $cliente["razon_social"];
                                                }
                                                ?>
                                                </option>
                                                <?php 
                                                foreach($consultar_clientes as $clientes){
                                                ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                            $id_sub_cliente = $servicios["id_sub_cliente"];
                                            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                            $consultar_sub_cliente->execute();
                                            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($consultar_sub_cliente as $sub_cliente){
                                                echo $sub_cliente["nombre_sub_cliente"];
                                            }
                                            ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                $id_tipo_servicio = $servicios["tipo_servicio"];
                                                $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                $consultar_tipo_servicio->execute();
                                                $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_servicio as $tipo_servicio){
                                                        echo $tipo_servicio["nombre_servicio"];
                                                }

                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>

                                        <div class="col-12 col-sm-6" style="display:none">
                                            <label for="">Tipo de carga</label>
                                            <select class="form-control" name="tipo_carga" id="tipo_carga" disabled>
                                                <option value="<?php echo $servicios["tipo_carga"]?>">
                                                    <?php 
                                                $id_tipo_carga = $servicios["tipo_carga"];
                                                $consultar_tipo_carga = $conn->prepare("SELECT * FROM tipo_cargas WHERE id = '$id_tipo_carga'");
                                                $consultar_tipo_carga->execute();
                                                $consultar_tipo_carga = $consultar_tipo_carga->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_carga as $tipo_carga){
                                                    echo $tipo_carga["nombre_carga"];
                                                }
                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_cargas as $tipo_cargas){?>
                                                <option value="<?php echo $tipo_cargas['id']?>" selected>
                                                    <?php echo $tipo_cargas['nombre_carga']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="lugar_operacions">
                                            <label for="">Lugar operación</label>
                                            <input type="text" name="lugar_operacion" id="lugar_operacion"
                                                class="form-control" value="<?php echo $servicios["lugar_operacion"]?>"
                                                disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_operacions">
                                            <label for="">Fecha de operación</label>
                                            <input type="date" name="fecha_operacion" id="fecha_operacion"
                                                class="form-control" value="<?php echo $servicios["fecha_operacion"]?>"
                                                disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="hora_operacions">
                                            <label for="">Horas de operación</label>
                                            <input type="text" name="hora_operacion" id="hora_operacion"
                                                class="form-control" value="<?php echo $servicios["hora_operacion"]?>"
                                                disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }else if($id_tipo_servicio == 12 || $id_tipo_servicio == 13){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>
        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información del servicio</h2>
                                <form>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                            if($servicios['fecha_recepcion_doc'] == null){
                                                
                                                }else{
                                                echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                                }
                                            ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                                    $id_cliente = $servicios["id_cliente"];
                                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                    $consultar_cliente->execute();
                                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                                    foreach($consultar_cliente as $cliente){
                                                        echo $cliente["razon_social"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php 
                                                    foreach($consultar_clientes as $clientes){
                                                    ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                $id_sub_cliente = $servicios["id_sub_cliente"];
                                                $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                $consultar_sub_cliente->execute();
                                                $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_sub_cliente as $sub_cliente){
                                                    echo $sub_cliente["nombre_sub_cliente"];
                                                }
                                                ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                $id_tipo_servicio = $servicios["tipo_servicio"];
                                                $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                $consultar_tipo_servicio->execute();
                                                $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_servicio as $tipo_servicio){
                                                        echo $tipo_servicio["nombre_servicio"];
                                                }

                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="lugar_entrega_estibas">
                                            <label for="">Lugar entrega Estibas</label>
                                            <input type="text" name="lugar_entrega_estiba" id="lugar_entrega_estiba"
                                                class="form-control"
                                                value="<?php echo $servicios["lugar_entrega_estiba"]?>" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_entrega_estibas">
                                            <label for="">Fecha de entrega Estibas</label>
                                            <input type="date" name="fecha_entrega_estiba" id="fecha_entrega_estiba"
                                                class="form-control"
                                                value="<?php echo $servicios["fecha_entrega_estiba"]?>" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="cantidad_estibas">
                                            <label for="">Cantidad entrega de Estibas</label>
                                            <input type="text" name="cantidad_estiba_entregada"
                                                id="cantidad_estiba_entregada"
                                                value="<?php echo $servicios["cantidad_estiba_entregada"]?>"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="patelizos">
                                            <label for="">¿Se patelizó?</label>
                                            <select name="patelizo" id="patelizo" class="form-control" disabled>
                                                <option value="<?php echo $servicios["patelizo"]?>">
                                                    <?php echo $servicios["patelizo"]?>
                                                </option>
                                                <option value="Sí">Sí</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6" id="lleno_contenedors">
                                            <label for="">¿Se llenó el contenedor?</label>
                                            <select name="lleno_contenedor" id="lleno_contenedor" class="form-control"
                                                disabled>
                                                <option value="<?php echo $servicios["lleno_contenedor"]?>">
                                                    <?php echo $servicios["lleno_contenedor"]?></option>
                                                <option value="Sí">Sí</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Horas de montacarga</label>
                                            <input type="text" class="form-control" placeholder="" disabled
                                                value="<?php echo $servicios["hora_montacarga"]?>" id="hora_montacarga"
                                                name="hora_montacarga" disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }else if($id_tipo_servicio == 14 || $id_tipo_servicio == 15 ){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>

        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información del servicio</h2>
                                <form>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                            if($servicios['fecha_recepcion_doc'] == null){
                                                
                                                }else{
                                                echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                                }
                                            ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                                $id_cliente = $servicios["id_cliente"];
                                                $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                $consultar_cliente->execute();
                                                $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                                foreach($consultar_cliente as $cliente){
                                                    echo $cliente["razon_social"];
                                                }
                                                ?>
                                                </option>
                                                <?php 
                                                foreach($consultar_clientes as $clientes){
                                                ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                            $id_sub_cliente = $servicios["id_sub_cliente"];
                                            $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                            $consultar_sub_cliente->execute();
                                            $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($consultar_sub_cliente as $sub_cliente){
                                                echo $sub_cliente["nombre_sub_cliente"];
                                            }
                                            ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                            $id_tipo_servicio = $servicios["tipo_servicio"];
                                            $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                            $consultar_tipo_servicio->execute();
                                            $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($consultar_tipo_servicio as $tipo_servicio){
                                                    echo $tipo_servicio["nombre_servicio"];
                                            }

                                            ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="lugar_entrega_estibas">
                                            <label for="">Lugar entrega </label>
                                            <input type="text" name="lugar_entrega_estiba" id="lugar_entrega_estiba"
                                                class="form-control"
                                                value="<?php echo $servicios["lugar_entrega_estiba"]?>" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="fecha_entrega_estibas">
                                            <label for="">Fecha de entrega </label>
                                            <input type="date" name="fecha_entrega_estiba" id="fecha_entrega_estiba"
                                                class="form-control"
                                                value="<?php echo $servicios["fecha_entrega_estiba"]?>" disabled>
                                        </div>

                                        <div class="col-12 col-sm-6" id="cantidad_estibas">
                                            <label for="">Cantidad entrega</label>
                                            <input type="text" name="cantidad_estiba_entregada"
                                                id="cantidad_estiba_entregada"
                                                value="<?php echo $servicios["cantidad_estiba_entregada"]?>"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="lleno_contenedors">
                                            <label for="">¿Se llenó el contenedor?</label>
                                            <select name="lleno_contenedor" id="lleno_contenedor" class="form-control"
                                                disabled>
                                                <option value="<?php echo $servicios["lleno_contenedor"]?>">
                                                    <?php echo $servicios["lleno_contenedor"]?></option>
                                                <option value="Sí">Sí</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Horas de montacarga</label>
                                            <input type="text" class="form-control" placeholder="" disabled
                                                value="<?php echo $servicios["hora_montacarga"]?>" id="hora_montacarga"
                                                name="hora_montacarga">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php }else if($id_tipo_servicio == 16 || $id_tipo_servicio == 17 ){?>
        <center>
            <h6 id="nombre_servicio"></h6>
            <hr>
        </center>

        <div class="col-md-12 col-xl-12">
            <div class="card mg-b-20">
                <div class="card-header">
                    <h4 class="card-header-title">
                        <center>
                            <img src="http://worldshippingcompany.com.co/assets/images/logo.png" style="width:80px"
                                alt="">
                        </center>
                        <br>
                        Servicio #<?= $id_servicio ?>
                    </h4>
                    <div class="card-header-btn">
                        <a href="#" data-toggle="refresh" class="btn card-refresh"><i
                                class="ion-android-refresh"></i></a>
                    </div>
                </div>
                <div class="card-body collapse show" id="upCominEvents">
                    <div class="baseline baseline-border">
                        <div class="baseline-list">
                            <div class="baseline-info">
                                <span class="mb-0 tx-gray-500">
                                    <hr>
                                </span>
                                <h2 class="tx-16 tx-dark">Información del servicio</h2>
                                <form>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <label for="">Fecha recepción documento</label>

                                            <input type="date" id="fecha_recepcion_doc" name="fecha_recepcion_doc"
                                                class="form-control" placeholder="Seleccionar una fecha" value="<?php 
                                        if($servicios['fecha_recepcion_doc'] == null){
                                            
                                            }else{
                                            echo date('Y-m-d',strtotime($servicios["fecha_recepcion_doc"])); 
                                            }
                                        ?>" disabled>
                                        </div>
                                        <div class=" col-12 col-sm-6">
                                            <label for="">Cliente</label>
                                            <select class="form-control" onchange="consultar_subcliente()"
                                                name="id_cliente" id="id_cliente" <?php echo $disabled_transporte ?>>
                                                <option value="<?php echo $servicios["id_cliente"] ?>" selected>

                                                    <?php
                                                    $id_cliente = $servicios["id_cliente"];
                                                    $consultar_cliente = $conn->prepare("SELECT * FROM clientes  WHERE id = '$id_cliente'");
                                                    $consultar_cliente->execute();
                                                    $consultar_cliente = $consultar_cliente->fetchAll(PDO::FETCH_ASSOC); 
                                                    foreach($consultar_cliente as $cliente){
                                                        echo $cliente["razon_social"];
                                                    }
                                                    ?>
                                                </option>
                                                <?php 
                                                    foreach($consultar_clientes as $clientes){
                                                ?>
                                                <option value="<?php echo $clientes["id"]?>">
                                                    <?php echo $clientes["razon_social"]?>
                                                </option>
                                                <?php }?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Subcliente</label>

                                            <select class="form-control" name="id_sub_cliente" id="id_sub_cliente"
                                                disabled>
                                                <option value="<?php echo $servicios["id_sub_cliente"] ?>" selected>
                                                    <?php
                                                    $id_sub_cliente = $servicios["id_sub_cliente"];
                                                    $consultar_sub_cliente = $conn->prepare("SELECT * FROM sub_clientes WHERE id = '$id_sub_cliente' AND estado = 1");
                                                    $consultar_sub_cliente->execute();
                                                    $consultar_sub_cliente = $consultar_sub_cliente->fetchAll(PDO::FETCH_ASSOC);
                                                    foreach($consultar_sub_cliente as $sub_cliente){
                                                        echo $sub_cliente["nombre_sub_cliente"];
                                                    }
                                                    ?>

                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Do/Ns</label>
                                            <input type="text" id="dons" name="dons"
                                                value="<?php echo $servicios["dons"]?>" class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Tipo de servicio</label>
                                            <select class="form-control"
                                                oninput="fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio ?>)"
                                                name="tipo_servicio" id="tipo_servicios" disabled>
                                                <option value="<?php echo $servicios["tipo_servicio"]?>" selected>
                                                    <?php 
                                                $id_tipo_servicio = $servicios["tipo_servicio"];
                                                $consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE id = '$id_tipo_servicio'");
                                                $consultar_tipo_servicio->execute();
                                                $consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
                                                foreach($consultar_tipo_servicio as $tipo_servicio){
                                                        echo $tipo_servicio["nombre_servicio"];
                                                }

                                                ?>
                                                </option>
                                                <?php foreach($consultar_tipo_servicios as $tipo_servicios){?>
                                                <option value="<?php echo $tipo_servicios['id']?>">
                                                    <?php echo $tipo_servicios['nombre_servicio']?>
                                                </option>
                                                <?php }?>
                                            </select>

                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <label for="">Contenedor</label>
                                            <input type="text" name="contenedor"
                                                value="<?php echo $servicios["contenedor"]?>" id="contenedor"
                                                class="form-control" disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="lugar_operacions">
                                            <label for="">Lugar operación</label>
                                            <input type="text" name="lugar_operacion" id="lugar_operacion"
                                                class="form-control" value="<?php echo $servicios["lugar_operacion"]?>"
                                                disabled>
                                        </div>
                                        <div class="col-12 col-sm-6" id="hora_operacions">
                                            <label for="">Horas de operación</label>
                                            <input type="text" name="hora_operacion" id="hora_operacion"
                                                class="form-control" value="<?php echo $servicios["hora_operacion"]?>"
                                                disabled>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>
        <div id="respuesta"></div>



        <style>
        .modal-backdrop {
            z-index: 100 !important
        }
        </style>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content rounded-4 shadow">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Servicio #<?php echo $id_servicio?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <center>
                            <h6>Resumen facturación</h6>
                        </center>
                        <div id="resumen_servicio_editar"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary">Descargar resumen (PDF)</button>
                    </div>
                </div>
            </div>
        </div>


        <footer class="page-footer" style=" position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    
    text-align: center;">

            <p class="pd-y-10 mb-0">Copyright&copy; 2022 | All rights reserved.</p>
    </div>
    </div>
    </footer>
    <!--/ Page Footer End -->
    </div>
    <!--/ Page Content End -->
    </div>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="assets/plugins/popper/popper.js"></script>
    <script src="assets/plugins/feather-icon/feather.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/plugins/pace/pace.min.js"></script>
    <script src="assets/plugins/toastr/toastr.min.js"></script>
    <script src="assets/plugins/countup/counterup.min.js"></script>
    <script src="assets/plugins/waypoints/waypoints.min.js"></script>
    <script src="assets/plugins/chartjs/chartjs.js"></script>
    <script src="assets/plugins/apex-chart/apexcharts.min.js"></script>
    <script src="assets/plugins/apex-chart/irregular-data-series.js"></script>
    <script src="assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js"></script>
    <script src="assets/js/dashboard/sales-dashboard-init.js"></script>
    <script src="assets/js/jquery.slimscroll.min.js"></script>
    <script src="assets/js/highlight.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/custom.js"></script>

    <script src="assets/plugins/footable/footable.all.min.js"></script>
    <script src="assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="assets/plugins/steps/jquery.steps.js"></script>
    <script src="assets/plugins/pace/pace.min.js"></script>

    <script src="assets/plugins/parsleyjs/parsley.js"></script>

    <script src="js/js-admin/main.js"></script>
    <script src="js/js-operador/main.js"></script>




</body>

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

</html>
<script>
window.load = mostrar_opciones_form_5();

function mostrar_opciones_form_5() {

    var opcion = document.getElementById("almacenamiento_vacio").value;
    var opcion_seleccionada = '<?php echo $servicios["patio_almacenaje"]?>';
    var opcion_seleccionada2 = '<?= $servicios["lugar_devolucion_vacio"]?>';

    // alert(opcion_seleccionada);
    if (opcion_seleccionada != "") {
        $("#mostrar_opcion_si").show();
        $("#mostrar_opcion_no").hide();
        $("#opcion").hide();
        // alert("si");
        $("#mensaje_almacenamiento").html("Si tiene almacenamiento<hr>");
    } else if (opcion_seleccionada2 != "") {
        $("#mostrar_opcion_si").hide();
        $("#mostrar_opcion_no").show();
        $("#opcion").hide();
        //alert("no");
        $("#mensaje_almacenamiento").html("No tiene almacenamiento<hr>");
    } else if (opcion == 1) {
        $("#mostrar_opcion_si").show();
        $("#mostrar_opcion_no").hide();
    } else if (opcion == 0) {
        $("#mostrar_opcion_si").hide();
        $("#mostrar_opcion_no").show();
    } else if (opcion == 3) {
        $("#mostrar_opcion_si").hide();
        $("#mostrar_opcion_no").hide();
    }

}
</script>
<script>
window.load = despachos_servicios(<?php echo $id_servicio?>);

function despachos_servicios(id_servicio) {
    var url =
        "actions/actions_admin/acordeon_despacho_parcial_total_servicio.php?id_servicio=" +
        id_servicio + '&opcion=1';

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#despacho_parcial_totals_servicios").html("Cargando...");
        },
        success: function(data) {
            $("#despacho_parcial_totals_servicios").html(data);
            seleccionar_conductor()
            fechas_bodegaje_traslado_zf_edit(<?php echo $id_servicio?>)
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });
}

function seleccionar_conductor() {
    var id_conductor = document.getElementById("nombre_condcutor_v").value;

    var url = "../../actions/actions_admin/consultar_conductores_servicios.php?id_conductor=" + id_conductor +
        "&estado=1";

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#campos_conductor").html("Cargando...");
        },
        success: function(data) {
            $("#campos_conductor").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });

}


function fechas_bodegaje_traslado_zf_edit(id_servicio) {
    var tipo_servicio = document.getElementById("tipo_servicios").value;

    var url = "../../actions/actions_admin/consultar_fechas_bodegaje_p_traslado_zf.php?tipo_servicio=" +
        tipo_servicio +
        '&id_servicio=' + id_servicio + "&estado=1";

    $.ajax({
        cache: false,
        async: false,
        url: url,
        beforeSend: function() {
            $("#fechas_bodegaje_traslado_zf_edit").html("Cargando...");
        },
        success: function(data) {
            $("#fechas_bodegaje_traslado_zf_edit").html(data);
        },
        error: function() {
            alert("Error, por favor intentalo más tarde.");
        },
    });

    var nombre_servicio = $("#tipo_servicios option:selected").text();
    $("#nombre_servicio").html("Servicio: " + nombre_servicio);
}
</script>