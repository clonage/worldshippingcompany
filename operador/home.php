<?php

include "../header/header.php";

if(isset($_SESSION["id_operador"])){
    header("Location: index");
}


?>

<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Bienvenido <?php echo $_SESSION["nombre_operador"]?></h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="index.html"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="#">Dashboard</a>
        <span class="breadcrumb-item active"></span>
    </div>
</div>

<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Ultimos 10 servicios añadidos
            </h4>
            <div class="card-header-btn">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse2"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="javascript:void(0)" onclick="cargar_ultimos_servicios()" data-toggle="refresh"
                    class="btn card-refresh"><i class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse2">
            <div id="tabla_servicios"></div>
        </div>
    </div>
</div>



<?php include "../footer/footer.php"?>

<script>
window.load = cargar_ultimos_servicios();
</script>