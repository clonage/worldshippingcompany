<?php


include "../header/header.php";

if($_SESSION["id_operador"] == null){
    echo '
    <script>redireccionar()
    function redireccionar(){
        window.location.href = "index";
    }
    </script>';
}
?>

<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Servicios</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="index.html"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="#">Dashboard</a>
        <span class="breadcrumb-item active">servicios</span>
    </div>
</div>


<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Servicios realidados - no realizados (utlima semana)
            </h4>
            <div class="card-header-btn">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse2"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="javascript:void(0)" onclick="cargar_ultimos_servicios()" data-toggle="refresh"
                    class="btn card-refresh"><i class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse2">

            <div id="tabla_servicios"></div>






        </div>

    </div>
</div>

<!-- modales formulario -->
<style>
.modal-body {
    height: 300px;
    width: 100%;
    overflow-y: auto;
}
</style>
<div class="modal fade" id="formulario1" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div id="formulario1_r"></div>
    </div>
</div>
<div class="modal fade" id="exampleModalToggle2" aria-hidden="true" aria-labelledby="exampleModalToggleLabel2"
    tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div id="formulario2_r"></div>
    </div>
</div>
<div class="modal fade" id="exampleModalToggle3" aria-hidden="true" aria-labelledby="exampleModalToggleLabel3"
    tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div id="formulario3_r"></div>
    </div>
</div>

<div class="modal fade" id="exampleModalToggle4" aria-hidden="true" aria-labelledby="exampleModalToggleLabel4"
    tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div id="formulario4_r"></div>
    </div>
</div>

<div class="modal fade" id="exampleModalToggle5" aria-hidden="true" aria-labelledby="exampleModalToggleLabel5"
    tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div id="formulario5_r"></div>
    </div>
</div>

<div class="modal fade" id="exampleModalToggle6" aria-hidden="true" aria-labelledby="exampleModalToggleLabel5"
    tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div id="resumen_control"></div>
    </div>
</div>
<?php include "../footer/footer.php"?>

<script>
window.load = cargar_servicios();
</script>
<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>