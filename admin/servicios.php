<?php

include "../header/header.php";
include '../database/database.php';
if ($_SESSION["id_admin"] == null) {
    echo "<script>alerta(); function alerta(){window.location.href = 'index';}</script>";
}

$id_cargo = $_SESSION["cargo"];
$consultar_tipo_servicio = $conn->prepare("SELECT * FROM tipo_servicios WHERE estado = 1");
$consultar_tipo_servicio->execute();
$consultar_tipo_servicio = $consultar_tipo_servicio->fetchAll(PDO::FETCH_ASSOC);
?>

<style>
/* The Modal (background) */
.modal {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 900;
    /* Sit on top */

    top: 0;
    width: 100%;
    /* Full width */
    height: 100vh;
    /* Full height */
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    height: 100%;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* The Close Button */
.closes {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.closes:hover,
.closes:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

/* The Modal (background) */
.modals {
    display: none;
    /* Hidden by default */
    position: fixed;
    /* Stay in place */
    z-index: 901;
    /* Sit on top */

    top: 0;
    width: 100%;
    /* Full width */
    height: 100vh;
    /* Full height */
    right: 0px;
    overflow: auto;
    /* Enable scroll if needed */
    background-color: rgb(0, 0, 0);
    /* Fallback color */
    background-color: rgba(0, 0, 0, 0.4);
    /* Black w/ opacity */
}

/* Modal Content */
.modals-content {
    background-color: #fefefe;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    height: 100%;

    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {
        top: -300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}

@keyframes animatetop {
    from {
        top: -300px;
        opacity: 0
    }

    to {
        top: 0;
        opacity: 1
    }
}


/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>


<div class="pageheader pd-t-25 pd-b-35">
    <div class="pd-t-5 pd-b-5">
        <h1 class="pd-0 mg-0 tx-20">Servicios</h1>
    </div>
    <div class="breadcrumb pd-0 mg-0">
        <a class="breadcrumb-item" href="home"><i class="icon ion-ios-home-outline"></i> Inicio</a>
        <a class="breadcrumb-item" href="home">Dashboard</a>
        <span class="breadcrumb-item active">servicios</span>
    </div>
</div>

<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Servicios agregados
            </h4>
            <center data-toggle="tooltip" data-trigger="hover" data-placement="top" title=""
                onclick="formulario_añadir_servicio()" data-original-title="Crear un nuevo servicio"><button
                    type="button" class="btn btn-brand btn-linkedin" id="myBtn">
                    <i data-feather="plus-circle"></i><span>Crear un
                        servicio</span></button></center>
            <div class="card-header-btn" style="margin-left:5px;">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse3"
                    aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="#" data-toggle="refresh" onclick="tabla_servicios()" class="btn card-refresh"><i
                        class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse3">
            <div class="row">

                </button>
                <div class="mg-20 form-inline wd-100p">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <select id="foo-filter-status" class="form-control">
                                <option value="">Mostrar todos</option>
                                <option value="Recien creado">Recien creado</option>
                                <option value="En proceso">En proceso</option>
                                <option value="Completado">Completado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">

                        <div class="input-group-append">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_exportal_services"
                                class="btn btn-success">Exportar (excel) <i class="fa fa-file-excel-o"></i></a>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="form-group ft-right">
                            <input id="foo-search" type="text" placeholder="Buscar servicio..." class="form-control"
                                autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div id="tabla_servicios"></div>
            <div id="estado_servicios"></div>
        </div>
    </div>
</div>


<!-- The añadir servicios -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content">
        <div class="row">
            <div class="col-1">
                <span class="closes">&times;</span>
            </div>
            <div class="col-11" style="margin-top: 6px;">
                <a href="javascript:void(0)" onclick="boton_atras()" class="btn btn-outline-primary btn-icon mg-r-5">
                    <div><i class="fa fa-arrow-circle-left"></i> Atrás</div>
                </a>
            </div>
        </div>
        <div id="respuesta_form_servicio"></div>
        <div id="formulario_añadir_servicio"></div>

        <!--  <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar ventana</button>
           <button type="button" onclick="guardar_servicio()" class="btn btn-primary">Guardar servicio</button>
        </div>-->
    </div>

</div>

<!-- The editar servicios -->
<div id="editar_servicio" class="modals">

    <!-- Modal content -->
    <div class="modal-content">
        <span class="close" onclick="cerrar_modal_editar_form()">&times;</span>
        <div id="editar_servicioss"></div>
        <div id="respuesta_editar_form_servicio"></div>

    </div>

</div>


<!-- exportar servicios excel -->
<div class="modal show" id="modal_exportal_services" data-backdrop="static" data-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true" style="z-index:199999 !important">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel_5">Exportar servicios</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ion-ios-close-empty"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <form id="excel_servicios">
                    <div>
                        <label for="">Seleccionar servicio</label>
                        <select class="form-control" oninput="seleccionar_servicio()" name="select_servicios"
                            id="select_servicios">
                            <option value="">Seleccione una opción*</option>
                            <?php foreach ($consultar_tipo_servicio as $tipo_servicio) { ?>
                            <option value="<?php echo $tipo_servicio['id'] ?>">
                                <?php echo $tipo_servicio['nombre_servicio'] ?>
                            </option>
                            <?php } ?>
                        </select>
                    </div>
                    <br>
                    <div>
                        <label for="">Seleccionar rango de fechas*</label>
                        <input id="datePicker4" type="text" class="form-control datepicker-here" name="fechas"
                            placeholder="Buscar servicios por rango de fechas" data-range="true"
                            data-multiple-dates-separator=" - " autocomplete="off">
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div id="respuesta_generador"></div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <a href="javascript:void(0)" class="btn btn-success" onclick="generar_excel_servicios()">Generar
                    excel <i class="fa fa-file-excel-o"></i></a>
            </div>
        </div>
    </div>
</div>

<script>
// ///////////////////////////////////////Row Toggler
$("#foo-row-toggler").footable();

// Accordion
$("#foo-accordion")
    .footable()
    .on("footable_row_expanded", function(e) {
        $("#foo-accordion tbody tr.footable-detail-show")
            .not(e.row)
            .each(function() {
                $("#foo-accordion").data("footable").toggleDetail(this);
            });
    });
// Filtering
var filtering = $("#foo-filtering");
filtering.footable().on("footable_filtering", function(e) {
    var selected = $("#foo-filter-status").find(":selected").val();
    e.filter += e.filter && e.filter.length > 0 ? " " + selected : selected;
    e.clear = !e.filter;
});

// Filter status
$("#foo-filter-status").change(function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});

// Search input
$("#foo-search").on("input", function(e) {
    e.preventDefault();
    filtering.trigger("footable_filter", {
        filter: $(this).val()
    });
});
</script>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("closes")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


function cerrar_modal_editar_form() {
    var modals = document.getElementById("editar_servicio");
    modals.style.display = "none";
}

function cerrar_modal_form() {
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
}


//////////////////////// editar servicio modal
</script>

<script src="../assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="../assets/plugins/datepicker/js/datepicker.es.js"></script>

<?php include "../footer/footer.php" ?>
<script>
window.load = tabla_servicios();
/*window.load = formulario_añadir_servicio();*/
window.load = despachos();
</script>
<script src="http://worldshippingcompany.com.co/assets/plugins/datepicker/js/datepicker.min.js"></script>
<script src="http://worldshippingcompany.com.co/assets/plugins/datepicker/js/datepicker.es.js"></script>