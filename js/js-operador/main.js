function iniciar_sesion_operador() {
  var url = "../../actions/actions_operador/login_action.php";

  $.ajax({
    cache: false,
    async: false,
    url: url,
    data: $("#form_inicio_sesion").serialize(),
    beforeSend: function () {
      $("#respuesta_login").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_login").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function cargar_ultimos_servicios() {
  // alert("entro");
  var accion = "ultimo10";
  var url =
    "../../actions/actions_admin/consultar_servicios.php?accion=" + accion;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_servicios").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_servicios").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

/////////// formulario ////////

function formulario1(id_servicio) {
  // $("#formulario1_r").show();
  // alert("entro");
  var url =
    "../../actions/actions_operador/formulario1.php?id_servicio=" + id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#formulario1_r").html("Cargando...");
    },
    success: function (data) {
      $("#formulario1_r").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function formulario2(id_servicio) {
  // alert("entro");
  //$("#formulario1_r").hide();
  var url =
    "../../actions/actions_operador/formulario2.php?id_servicio=" + id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#formulario2_r").html("Cargando...");
    },
    success: function (data) {
      $("#formulario2_r").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function formulario3(id_servicio) {
  // alert("entro");
  var url =
    "../../actions/actions_operador/formulario3.php?id_servicio=" + id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#formulario3_r").html("Cargando...");
    },
    success: function (data) {
      $("#formulario3_r").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function formulario4(id_servicio) {
  // alert("entro");
  var url =
    "../../actions/actions_operador/formulario4.php?id_servicio=" + id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#formulario4_r").html("Cargando...");
    },
    success: function (data) {
      $("#formulario4_r").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function formulario5(id_servicio) {
  // alert("entro");
  var url =
    "../../actions/actions_operador/formulario5.php?id_servicio=" + id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#formulario5_r").html("Cargando...");
    },
    success: function (data) {
      $("#formulario5_r").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
function resumen_control(id_servicio) {
  // alert("entro");
  var url =
    "../../actions/actions_operador/resumen_control.php?id_servicio=" +
    id_servicio;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#resumen_control").html("Cargando...");
    },
    success: function (data) {
      $("#resumen_control").html(data);
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
/////////////////////////

function cargar_servicios() {
  // alert("entro");
  var accion = "todos";
  var url =
    "../../actions/actions_operador/consultar_servicios.php?accion=" + accion;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#tabla_servicios").html("Cargando...");
    },
    success: function (data) {
      $("#tabla_servicios").html(data);
      $("#sin_buscar_servicios").show();
      $("#con_buscador_servicios").hide();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}

function buscar_servicio() {
  var fecha = document.getElementById("datePicker4").value;
  var url =
    "../../actions/actions_operador/buscador_servicio_por_rango_fecha.php?rango_fecha=" +
    fecha;

  $.ajax({
    cache: false,
    async: false,
    url: url,
    beforeSend: function () {
      $("#respuesta_buscador").html("Cargando...");
    },
    success: function (data) {
      $("#respuesta_buscador").html(data);
      $("#sin_buscar_servicios").hide();
      $("#con_buscador_servicios").show();
    },
    error: function () {
      alert("Error, por favor intentalo más tarde.");
    },
  });
}
