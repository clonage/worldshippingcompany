<?php 
session_start();

$id_operador = $_SESSION["cargo"];

?>
<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from colorlib.net/metrical/light/page-singin.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jan 2020 21:19:37 GMT -->

<head>
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="" />
    <!-- Page Title -->
    <title>World Shipping Company</title>
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/plugins/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/plugins/flag-icon/flag-icon.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/ionicons/css/ionicons.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/footable/footable.core.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/toastr/toastr.min.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/chartist/chartist.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/apex-chart/apexcharts.css">
    <link type="text/css" rel="stylesheet" href="../assets/css/app.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/css/style.min.css" />
    <link type="text/css" rel="stylesheet" href="../assets/plugins/datepicker/css/datepicker.min.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/bootstrap-select/css/bootstrap-select.min.css">
    <link type="text/css" rel="stylesheet" href="../assets/plugins/steps/jquery.steps.css">


    <!-- Favicon -->
    <link rel="icon" href="../../assets/images/favicon.ico" type="image/x-icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn"t work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

    <style>
    #responsive {
        overflow: hidden;
        white-space: initial;
    }
    </style>
</head>


<body>
    <!--================================-->
    <!-- Page Container Start -->
    <!--================================-->

    <div class="page-container">
        <!--================================-->
        <!-- Page Sidebar Start -->
        <!--================================-->
        <?php if($_SESSION["id_admin"] != null){ ?>
        <div class="page-sidebar">
            <div class="logo">
                <a class="logo-img" href="../admin">
                    <img class="desktop-logo" src="../assets/images/logo_admin.png" alt="">
                    <img class="small-logo" src="../assets/images/small-logo.png" alt="">
                </a>
                <i class="ion-ios-close-empty" id="sidebar-toggle-button-close"></i>
            </div>
            <!--================================-->
            <!-- Sidebar Menu Start -->
            <!--================================-->
            <div class="page-sidebar-inner">
                <div class="page-sidebar-menu">
                    <ul class="accordion-menu">
                        <li class="open active">
                            <a href="#"><i data-feather="home"></i>
                                <span>Dashboard</span><i class="accordion-icon fa fa-angle-left"></i></a>
                            <ul class="sub-menu" style="display: block;">
                                <!-- Active Page -->

                                <li class="active"><a href="../admin">Inicio</a></li>

                                <?php if($id_operador == ''){ ?>
                                <li><a href="clientes">Clientes</a></li>
                                <li><a href="empleados">Empleados</a></li>
                                <li><a href="conductores">Conductores</a></li>
                                <?php } ?>
                                <li><a href="servicios">Servicios</a></li>
                                <li><a href="chech_list_vehiculo">Inspección vehículos</a></li>


                            </ul>
                        </li>
                        <?php if($id_operador == ''){ ?> <li>
                            <a href="#"><i data-feather="user"></i>
                                <span>Gestionar datos</span><i class="accordion-icon fa fa-angle-left"></i></a>
                            <ul class="sub-menu">
                                <li><a href="tipos_servicios">Tipos de servicios</a></li>
                                <li><a href="tipo_carga">Tipos de cargas</a></li>
                                <li><a href="rutas">Rutas</a></li>

                            </ul>
                        </li> <?php } ?>
                        <li>
                            <a href="#"><i data-feather="user"></i>
                                <span>Cuenta</span><i class="accordion-icon fa fa-angle-left"></i></a>
                            <ul class="sub-menu">
                                <li><a href="mi_perfil">Mi perfil</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-content">

            <div class="page-header">
                <div class="search-form">
                    <form action="#" method="GET">
                        <div class="input-group">
                            <input class="form-control search-input" name="search" placeholder="Type something..."
                                type="text" />
                            <span class="input-group-btn">
                                <span id="close-search"><i class="ion-ios-close-empty"></i></span>
                            </span>
                        </div>
                    </form>
                </div>
                <nav class="navbar navbar-expand-lg">
                    <ul class="list-inline list-unstyled mg-r-20">
                        <!-- Mobile Toggle and Logo -->
                        <li class="list-inline-item align-text-top"><a class="hidden-md hidden-lg" href="#"
                                id="sidebar-toggle-button"><i class="ion-navicon tx-20"></i></a></li>
                        <!-- PC Toggle and Logo -->
                        <li class="list-inline-item align-text-top"><a class="hidden-xs hidden-sm" href="#"
                                id="collapsed-sidebar-toggle-button"><i class="ion-navicon tx-20"></i></a></li>
                    </ul>
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav mr-auto">
                            <!-- Features -->
                            <li class="dropdown mega-dropdown mg-t-5">

                            </li>
                            <!-- Technology -->
                            <li class="dropdown mega-dropdown mg-t-5">

                            </li>
                            <!-- Ecommerce -->
                            <li class="dropdown mega-dropdown mg-t-5">

                            </li>

                            <li class="dropdown mega-dropdown mg-t-5 mx-auto">
                                <a href="#"><i data-feather="home"></i>
                            </li>


                        </ul>
                    </div>
                    <div class="header-right pull-right">
                        <ul class="list-inline justify-content-end">
                            <li class="list-inline-item dropdown">
                                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="select-profile">Hola, <?php echo $_SESSION['nombre_admin']?>!</span>
                                    <img src="../assets/images/avatar/avatar1.png"
                                        class="img-fluid wd-35 ht-35 rounded-circle" alt="">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-profile shadow-2">
                                    <div class="user-profile-area">
                                        <div class="user-profile-heading">
                                            <div class="profile-thumbnail">
                                                <img src="../assets/images/avatar/avatar1.png"
                                                    class="img-fluid wd-35 ht-35 rounded-circle" alt="">
                                            </div>
                                            <div class="profile-text">
                                                <h6><?php echo $_SESSION['nombre_admin']?></h6>
                                                <span><?php echo $_SESSION['correo']?></span>
                                            </div>
                                        </div>
                                        <a href="#" class="dropdown-item"><i class="icon-user" aria-hidden="true"></i>
                                            Mi perfil</a>
                                        <a href="#" class="dropdown-item"><i class="icon-heart" aria-hidden="true"></i>
                                            Soporte</a>
                                        <a href="salir" class="dropdown-item"><i class="icon-power"
                                                aria-hidden="true"></i>
                                            Cerrar sesión</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <?php }else if($_SESSION["id_operador"] != null){?>
            <div class="page-sidebar">
                <div class="logo">
                    <a class="logo-img" href="../operador">
                        <img class="desktop-logo" src="../assets/images/logo_admin.png" alt="">
                        <img class="small-logo" src="../assets/images/small-logo.png" alt="">
                    </a>
                    <i class="ion-ios-close-empty" id="sidebar-toggle-button-close"></i>
                </div>
                <!--================================-->
                <!-- Sidebar Menu Start -->
                <!--================================-->
                <div class="page-sidebar-inner">
                    <div class="page-sidebar-menu">
                        <ul class="accordion-menu">
                            <li class="open active">
                                <a href="#"><i data-feather="home"></i>
                                    <span>Dashboard</span><i class="accordion-icon fa fa-angle-left"></i></a>
                                <ul class="sub-menu" style="display: block;">
                                    <!-- Active Page -->

                                    <li class="active"><a href="../operador/home">Inicio</a></li>
                                    <li><a href="servicios">Servicios</a></li>

                                </ul>
                            </li>
                            <li>
                                <a href="#"><i data-feather="user"></i>
                                    <span>Cuenta</span><i class="accordion-icon fa fa-angle-left"></i></a>
                                <ul class="sub-menu">
                                    <li><a href="mi_perfil">Mi perfil</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="page-content">

                <div class="page-header">
                    <div class="search-form">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input class="form-control search-input" name="search" placeholder="Type something..."
                                    type="text" />
                                <span class="input-group-btn">
                                    <span id="close-search"><i class="ion-ios-close-empty"></i></span>
                                </span>
                            </div>
                        </form>
                    </div>
                    <nav class="navbar navbar-expand-lg">
                        <ul class="list-inline list-unstyled mg-r-20">
                            <!-- Mobile Toggle and Logo -->
                            <li class="list-inline-item align-text-top"><a class="hidden-md hidden-lg" href="#"
                                    id="sidebar-toggle-button"><i class="ion-navicon tx-20"></i></a></li>
                            <!-- PC Toggle and Logo -->
                            <li class="list-inline-item align-text-top"><a class="hidden-xs hidden-sm" href="#"
                                    id="collapsed-sidebar-toggle-button"><i class="ion-navicon tx-20"></i></a></li>
                        </ul>
                        <div class="collapse navbar-collapse">
                            <ul class="navbar-nav mr-auto">
                                <!-- Features -->
                                <li class="dropdown mega-dropdown mg-t-5">

                                </li>
                                <!-- Technology -->
                                <li class="dropdown mega-dropdown mg-t-5">

                                </li>
                                <!-- Ecommerce -->
                                <li class="dropdown mega-dropdown mg-t-5">

                                </li>

                                <li class="dropdown mega-dropdown mg-t-5 mx-auto">
                                    <a href="#"><i data-feather="home"></i>
                                </li>


                            </ul>
                        </div>
                        <div class="header-right pull-right">
                            <ul class="list-inline justify-content-end">
                                <li class="list-inline-item dropdown">
                                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="select-profile">Hola,
                                            <?php echo $_SESSION['nombre_operador']?>!</span>
                                        <img src="../assets/images/avatar/avatar1.png"
                                            class="img-fluid wd-35 ht-35 rounded-circle" alt="">
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-profile shadow-2">
                                        <div class="user-profile-area">
                                            <div class="user-profile-heading">
                                                <div class="profile-thumbnail">
                                                    <img src="../assets/images/avatar/avatar1.png"
                                                        class="img-fluid wd-35 ht-35 rounded-circle" alt="">
                                                </div>
                                                <div class="profile-text">
                                                    <h6><?php echo $_SESSION['nombre_operador']?></h6>
                                                    <span><?php echo $_SESSION['cedula']?></span>
                                                </div>
                                            </div>
                                            <a href="#" class="dropdown-item"><i class="icon-user"
                                                    aria-hidden="true"></i>
                                                Mi perfil</a>
                                            <a href="#" class="dropdown-item"><i class="icon-heart"
                                                    aria-hidden="true"></i>
                                                Soporte</a>
                                            <a href="salir" class="dropdown-item"><i class="icon-power"
                                                    aria-hidden="true"></i>
                                                Cerrar sesión</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <?php }else{
            }  
            ?>
                <div class="page-inner" style="height: 100vh;">
                    <!-- Main Wrapper -->
                    <div id="main-wrapper">