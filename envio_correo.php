<?php 
require 'vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
$dirPrincipal = 'vendor/phpmailer';
require "$dirPrincipal/phpmailer/src/Exception.php";
require "$dirPrincipal/phpmailer/src/PHPMailer.php";
require "$dirPrincipal/phpmailer/src/SMTP.php";

 //enviar mail de confirmacion
 $mailTo   = 'gersonpedrozosalcedo@gmail.com';
 $mailBody = '
 <html>

     <head>
         <meta charset="UTF-8" />
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
         <style>
         .card {
             box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
             max-width: 100%;
             margin-top: 20px;
             margin-left: 10px;
             margin-right: 10px;
             text-align: center;
             font-family: arial;
             background: #ffffff;
         }

         .title {
             margin: 20px;
             color: grey;
             font-size: 14x;
             text-align: center;
         }

         .button {
             border: none;
             border-radius: 20px;
             outline: 0;
             display: inline-block;
             padding: 8px;
             color: white;
             background-color: #28a49c;
             text-align: center;
             cursor: pointer;
             width: 80%;
             margin: 8px;
             font-size: 14px;
         }
         .button:hover,
         a:hover {
             opacity: 0.7;
         }
         .flex-container {
             display: flex;

             margin-left: auto;
             margin-right: auto;
             width: 53%;
         }
         @media (min-width: 600px) {
             .flex-container {
                 display: flex;

                 margin-left: auto;
                 margin-right: auto;
                 width: 20%;
             }
         }
         .flex-container>div {

             margin: 10px;

             color: white;
         }
         .flex-container2 {
             display: flex;

         }
         .flex-container2>div {

             margin: 10px;

             color: white;
         }
         </style>
     </head>

     <body style="background:#4d4d4d;">

         <div style="background:#1c1c1c; border-radius: 0px 0px 20px 20px; ">
             <center> <img src="https://midailycare.com/assets/images/logo_blanco.png" style="width:35%"></center>
         </div>

         <div class="card" style=" padding:10px; border-radius:20px;">
             <center> <img src="https://midailycare.com/assets/images/image-5.png" style="width:100px"></center>
          
             <p class="title">Se su registro a sido exitoso.</p>
          
            
         </div>
         
         <div class="flex-container">
             <div>
             <center><img src="https://farmakanna.com/portal/assets/images/image-1.png" alt="Facebook" title="Facebook"
                         width="32"
                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                 </center>
         </div>
         <div>
                 <center><img src="https://farmakanna.com/portal/assets/images/image-2.png" alt="Facebook" title="Facebook"
                         width="32"
                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                 </center>
             </div>
         <div>
                 <center><img src="https://farmakanna.com/portal/assets/images/image-3.png" alt="Facebook" title="Facebook"
                         width="32"
                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                 </center>
             </div>
             <div>
                 <center><img src="https://farmakanna.com/portal/assets/images/image-6.png" alt="Facebook" title="Facebook"
                         width="32"
                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                 </center>
             </div>
         </div>
         <hr>
         <center style="color:white"> Copyright © MiDailyCare 2022 </center>
     </body>
 </html>
 ';
 $mailAltBody = "Gracias por registrarte en Mi Daily Care";
 $mailSubject = 'World shipping';
 //These must be at the top of your script, not inside a function


 //Create an instance; passing `true` enables exceptions
 $mail = new PHPMailer(true);

 $mailHost = "a2plcpnl0093.prod.iad2.secureserver.net";
 $mailUsername   = 'notificaciones@worldshippingcompany.com.co';                     //SMTP username
 $mailPassword   = 'Worldshipping@';
 $mailFrom = 'notificaciones@worldshippingcompany.com.co';
 
 //Server settings
 $mail->SMTPDebug = 0;//SMTP::DEBUG_SERVER;                      //Enable verbose debug output
 $mail->isSMTP();  
 $mail->CharSet = 'UTF-8';                         //Send using SMTP
 $mail->Host       = $mailHost;                     //Set the SMTP server to send through
 $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
 $mail->Username   = $mailUsername;                     //SMTP username
 $mail->Password   = $mailPassword;  
 $mail->SMTPSecure = 'ssl';                              //SMTP password
             //Enable implicit TLS encryption
 $mail->Port = 465;                                    //TCP port to connect to; use 587 if you have set 

 //Recipients
 $mail->setFrom($mailFrom, 'World Shipping');
 $mail->addAddress($mailTo);     //Add a recipient

 //Content
 $mail->isHTML(true);                                  //Set email format to HTML
 
 $mail->Subject = $mailSubject;
 $mail->Body    = $mailBody;
 
 //$mail->send();

?>